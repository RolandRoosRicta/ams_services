package nl.ricta.gateway.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.gateway.discovery.DiscoveryClientRouteDefinitionLocator;
import org.springframework.cloud.gateway.discovery.DiscoveryLocatorProperties;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;

@SpringBootApplication
@Configuration
@EnableDiscoveryClient
@EnableEurekaClient
public class GatewayServiceApplication 
{
    public static void main(String[] args) 
    {
       SpringApplication.run(GatewayServiceApplication.class, args);
    }
    @Bean
    public DiscoveryClientRouteDefinitionLocator discoveryClientRouteLocator(
            DiscoveryClient discoveryClient,
            DiscoveryLocatorProperties properties) 
    {
            return new DiscoveryClientRouteDefinitionLocator(discoveryClient, properties);
    }
    @Bean
    RouterFunction staticResourceLocator(){
            return RouterFunctions.resources("/ams-admin/**", new ClassPathResource("ui/"));
    }
    
//    @Bean
//RouterFunction staticResourceLocator(){
//	return RouterFunctions.resources("/portal/**", new FileSystemResource("D:\\website\\spring-cloud-static-resource/"));
//}
//    @Bean
//    public RouteLocator routes(RouteLocatorBuilder builder) {
//            return builder.routes()
//                .route("ams-admin", t -> t.path("/api")
//                    .uri("lb://ams-admin"))
// 
//            .build();
//        }
//    @Bean
//    public RouteLocator myRoutes(RouteLocatorBuilder builder) 
//    {
//        return builder.routes()
//        .route(p -> p
//            .path("/get")
//            .filters(f -> f.addRequestHeader("Hello", "World"))
//            .uri("http://httpbin.org:80"))
//        .build();
//        //return builder.routes().build();
//    }

}
