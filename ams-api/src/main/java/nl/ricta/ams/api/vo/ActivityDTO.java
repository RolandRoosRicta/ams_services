package nl.ricta.ams.api.vo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import lombok.NoArgsConstructor;
import nl.ricta.ams.admin.model.interfaces.Activity;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author roosr
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ActivityDTO implements Activity
{
    Long id;
    String title;
    String description;
    String requestorName;
    String dateOfCreation;
    String dateOfExpiration;
    //See ActivityState for typical values
    String status;
    //See activityType for typical values
    String type;
    String prio;

     
}
