/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.ricta.ams.api.vo;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author rroos
 * Data Transfer Object for user properties
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO //implements Accommodation, DTO
{
// public UserDTO()
// {
//     
// }

    public Long id;

    public String username;

  
    public String firstname;

 
    public String company;

    public Boolean initialized = Boolean.FALSE;

    public String email;

    public Boolean emailVerified = Boolean.FALSE;


    public String emailVerifiedUrl = UUID.randomUUID().toString();

    public String lastname;



    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", firstname='" + firstname + '\'' +
                ", email='" + email + '\'' +
                ", lastname='" + lastname + '\'' +
                '}';
    }
}
