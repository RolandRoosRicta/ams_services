package nl.ricta.ams.api.vo;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import lombok.NoArgsConstructor;
import nl.ricta.ams.admin.model.interfaces.ActivityHistory;

/**
 *
 * @author roosr
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ActivityHistoryDTO //implements ActivityHistory
{
    Long id;
    Long activityId;
    //?String changed;
    String title;
    String description;
    String requestorName;
    String type;
    String prio;
    String status;
    String creationDate;
    String expirationDate;
}
