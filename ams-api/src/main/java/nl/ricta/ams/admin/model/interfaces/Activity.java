package nl.ricta.ams.admin.model.interfaces;
import nl.ricta.ams.api.vo.*;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author roosr
 */

public interface Activity 
{
    /**
     * @return the id
     */
    public Long getId(); 

    /**
     * @return the title
     */
    public String getTitle(); 

    /**
     * @return the description
     */
    public String getDescription(); 

    /**
     * @return the requestorName
     */
    public String getRequestorName() ;

    /**
     * @return the dateOfCreation
     */
    public String getDateOfCreation() ;

    /**
     * @return the dateOfExpiration
     */
    public String getDateOfExpiration();

    /**
     * @return the status
     */
    public String getStatus();
    /**
     * @return the type
     */
    public String getType() ;

    /**
     * @return the prio
     */
    public String getPrio() ;

}
