/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.ricta.ams.admin.services.interfaces;

import java.util.List;
import nl.ricta.ams.api.vo.ActivityDTO;

/**
 *
 * @author roosr
 */
public interface ActivityService 
{
    public List<ActivityDTO> findAll();
    public long updateActivity(ActivityDTO activity);
    public ActivityDTO findOneByID(long Id);
}
