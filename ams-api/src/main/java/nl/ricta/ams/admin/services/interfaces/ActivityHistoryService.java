/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.ricta.ams.admin.services.interfaces;

import java.util.List;
import nl.ricta.ams.api.vo.ActivityDTO;
import nl.ricta.ams.api.vo.ActivityHistoryDTO;

/**
 *
 * @author roosr
 */
public interface ActivityHistoryService 
{
    public List<ActivityHistoryDTO> findAll();
    public void AddActivityVersion(ActivityDTO activity);
}
