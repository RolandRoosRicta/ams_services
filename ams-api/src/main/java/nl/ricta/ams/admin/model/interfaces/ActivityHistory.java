package nl.ricta.ams.admin.model.interfaces;

/**
 *
 * @author roosr
 */

public interface ActivityHistory 
{

    /**
     * @return the id
     */
    public Long getId(); 

    /**
     * @return the activityId
     */
    public Long getActivityId() ;

    /**
     * @return the changed
     */
   //? public String getChanged();

    /**
     * @return the title
     */
    public String getTitle() ;

    /**
     * @return the description
     */
    public String getDescription();

    /**
     * @return the requestorName
     */
    public String getRequestorName();

    /**
     * @return the type
     */
    public String getType() ;

    /**
     * @return the prio
     */
    public String getPrio();

    /**
     * @return the status
     */
    public String getStatus() ;

    /**
     * @return the creationDate
     */
    public String getCreationDate() ;

    /**
     * @return the expirationDate
     */
    public String getExpirationDate();
}
