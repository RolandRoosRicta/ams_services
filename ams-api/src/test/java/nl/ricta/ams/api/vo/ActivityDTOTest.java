/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.ricta.ams.api.vo;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.junit.Test;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author roosr
 */
@RunWith(SpringRunner.class)
//@RunWith(SpringJUnit4ClassRunner.class)
public class ActivityDTOTest 
{
    
    public ActivityDTOTest() 
    {
    }
    
    @Test
    public void builder_ActivityDto_Should_Build() 
    {
        ActivityDTO dto =  ActivityDTO
                         .builder()
                         .id(1L)
                         .title("title")
                         .dateOfCreation("1-1-2019")
                         .dateOfExpiration("1-1-2019")
                         .description("desc")
                         .prio("prio")
                         .status(ActivityState.Closed.toString())
                         .requestorName("rr")
                         .build();
        Assert.assertNotNull(dto);
    }

    
}
