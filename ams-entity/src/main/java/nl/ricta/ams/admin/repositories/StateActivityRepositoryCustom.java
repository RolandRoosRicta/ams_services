package nl.ricta.ams.admin.repositories;

import java.util.List;
import javax.persistence.EntityManager;
import nl.ricta.ams.api.entities.ProcessStateActivityEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 * @author r.roos
 */
//@RepositoryRestResource(collectionResourceRel = "stateactivity-entity", path = "processstateactivityentity")
public interface StateActivityRepositoryCustom
{
    
    //@PostFilter("filterObject.owner != null and filterObject.owner.username == authentication.name")
    //@Query("SELECT proxy FROM Proxy proxy JOIN proxy.serviceApis serviceApi WHERE :serviceApiId = serviceApi.id")
    
    //@Query("select ProcessStateActivityEntity from ProcessStateActivityEntity sa Right OUTER JOIN ActivityEntity act WHERE :activity_id = act.id")
    //public List<ProcessStateActivityEntity> findAllActivitiesWithState();//@Param(value = "username") String username , @Param(value = "from") Date from, @Param(value = "until") Date until);

  
     List<ProcessStateActivityEntity> getActsWithJsonContaining(String likeParam);
     //Iterable<ProcessStateActivityEntity> findAll();
}
