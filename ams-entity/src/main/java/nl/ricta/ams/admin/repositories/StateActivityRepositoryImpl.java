package nl.ricta.ams.admin.repositories;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import nl.ricta.ams.api.entities.ProcessStateActivityEntity;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

/**
 *
 * @author r.roos
 */
//@RepositoryRestResource(collectionResourceRel = "stateactivity-entity", path = "processstateactivityentity")

@Repository
public class StateActivityRepositoryImpl implements StateActivityRepositoryCustom//extends PagingAndSortingRepository<ProcessStateActivityEntity, Long> 
{
    
    //@PostFilter("filterObject.owner != null and filterObject.owner.username == authentication.name")
    //@Query("SELECT proxy FROM Proxy proxy JOIN proxy.serviceApis serviceApi WHERE :serviceApiId = serviceApi.id")
    
    //@Query("select ProcessStateActivityEntity from ProcessStateActivityEntity sa Right OUTER JOIN ActivityEntity act WHERE :activity_id = act.id")
    //public List<ProcessStateActivityEntity> findAllActivitiesWithState();//@Param(value = "username") String username , @Param(value = "from") Date from, @Param(value = "until") Date until);

     //@Query("Select c from ProcessStateActivityEntity c where c.activityState like :place")
//     public List<ProcessStateActivityEntity> findByPlaceContaining(@Param("place")String place)
//     {
//         
//        return null;
//     }
    @PersistenceContext
    EntityManager entityManager;
    
    @Override
    public List<ProcessStateActivityEntity> getActsWithJsonContaining(String likeParam) 
    {
        //where CAST(activity_state as TEXT) like '%"qty": 24%'
        Query query = entityManager.createNativeQuery("SELECT actState.* FROM stateactivities as actState " +
                
                "where CAST(activity_state as TEXT) like ?", ProcessStateActivityEntity.class);
        query.setParameter(1, likeParam);
        return query.getResultList();
    }
}
