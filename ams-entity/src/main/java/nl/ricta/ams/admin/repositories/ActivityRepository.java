package nl.ricta.ams.admin.repositories;

import nl.ricta.ams.api.entities.ActivityEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

/**
 *
 * @author r.roos
 */
@RepositoryRestResource(collectionResourceRel = "stateactivity-entity", path = "stateactivities")
public interface ActivityRepository extends PagingAndSortingRepository<ActivityEntity, Long> 
{

}
