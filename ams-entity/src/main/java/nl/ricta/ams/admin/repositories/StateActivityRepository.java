package nl.ricta.ams.admin.repositories;

import nl.ricta.ams.api.entities.ProcessStateActivityEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author r.roos
 */
//@RepositoryRestResource(collectionResourceRel = "stateactivity-entity", path = "processstateactivityentity")
@Repository
public interface StateActivityRepository extends PagingAndSortingRepository<ProcessStateActivityEntity, Long> , StateActivityRepositoryCustom
{
    
    
}
