package nl.ricta.ams.admin.repositories;

import nl.ricta.ams.admin.model.interfaces.ActivityHistory;
import nl.ricta.ams.api.entities.ActivityHistoryEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 * @author r.roos
 */
@RepositoryRestResource(collectionResourceRel = "activity-history-entity", path = "activityhistory")
public interface ActivityHistoryRepository extends PagingAndSortingRepository<ActivityHistoryEntity, Long> 
{
    
        //@PostFilter("filterObject.owner != null and filterObject.owner.username == authentication.name")
    //@Override
    //public Page<ActivityHistoryEntity> findAll(Pageable pgbl);

  //  @Query("SELECT serviceApi FROM ApiKey apiKey INNER JOIN apiKey.serviceApi serviceApi WHERE apiKey.keyValue = :apiKeyValue")
   // public ServiceApi findByApiKeyValue(@Param("apiKeyValue") String apiKeyValue);
}
