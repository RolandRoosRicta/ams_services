/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.ricta.ams.api.entities;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

/**
 *
 * @author r.roos
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "stateactivities")
public class ProcessStateActivityEntity
{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

   // @Column(unique = true, nullable = false)
   // protected Long activityId;
    
    @Column
    protected boolean mailSent;
    
//    @OneToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "activity_id", insertable = false, updatable = false)
//    ActivityEntity activityOfState; 
   
    //@Type(type = "jsonb")
    //@Column(columnDefinition = "json")
    @Column
    protected String activityState;
}
