/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.ricta.ams.api.entities;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import nl.ricta.ams.admin.model.interfaces.Activity;

/**
 *
 * @author r.roos
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "activities")
public class ActivityEntity implements Activity
{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(nullable = false, unique = false)
    protected String requestorName;

    @Column(nullable = false, unique = false)
    protected String title;
     
    @Column(nullable = false, unique = false)
    protected String description;
    
    @Column(nullable = true)
    protected String type;

    @Column(nullable = false)
    protected String prio;
    
    @Column(nullable = false)
    protected String status;

    @Column(nullable = false)
    protected Date creationDate;
    
    @Column(nullable = true)
    protected Date expirationDate;
  
/**
     * @return the dateOfCreation
     */
    public String getDateOfCreation()
    {
        return creationDate != null ? creationDate.toString() : null;
    }

    /**
     * @return the dateOfExpiration
     */
    public String getDateOfExpiration()
    {
        return expirationDate != null ? expirationDate.toString() : null;
    }

  
//
//    public String getLastname() {
//        return lastname;
//    }
//
//    public void setLastname(String familyname) {
//        this.lastname = familyname;
//    }
//
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public String getUsername() {
//        return username;
//    }
//
//    public void setUsername(String username) {
//        this.username = username;
//    }
//
//    @Override
//    public String toString() {
//        return "User{" +
//                "id=" + id +
//                ", username='" + username + '\'' +
//                ", firstname='" + firstname + '\'' +
//                ", email='" + email + '\'' +
//                ", lastname='" + lastname + '\'' +
//                '}';
//    }
}
