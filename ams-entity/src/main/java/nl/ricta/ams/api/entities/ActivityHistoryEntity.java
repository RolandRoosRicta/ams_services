/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.ricta.ams.api.entities;

import java.util.Date;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import nl.ricta.ams.admin.model.interfaces.ActivityHistory;


/**
 *
 * @author r.roos
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "activitieshistory")
public class ActivityHistoryEntity //implements ActivityHistory
{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;
    
    protected Long activityId;

    @Basic(optional = false)
    @Column(name="changed", columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Date changed;
    
    @Column(nullable = false)
    protected String title;
     
    @Column(nullable = false)
    protected String description;
     
    @Column(nullable = true)
    protected String requestorName;

    @Column(nullable = true)
    protected String type;

    @Column(nullable = false)
    protected String prio;
    
    @Column(nullable = false)
    protected String status;

    @Column(nullable = true)
    protected Date creationDate;
    
    @Column(nullable = true)
    protected Date expirationDate;
  
     /**
     * @return the expirationDate
     */
    public String getExpirationDate()
    {
        return expirationDate.toString();
    }
    
      /**
     * @return the expirationDate
     */
    public String getCreationDate()
    {
        return creationDate.toString();
    }

//    @Override
//    public String toString() {
//        return "User{" +
//                "id=" + id +
//                ", username='" + username + '\'' +
//                ", firstname='" + firstname + '\'' +
//                ", email='" + email + '\'' +
//                ", lastname='" + lastname + '\'' +
//                '}';
//    }
}
