package nl.ricta.ams.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@Configuration
@EnableJpaRepositories("nl.ricta.ams*")
@ComponentScan(basePackages = {"nl.ricta.ams*", "nl.ricta.ams.*"})
@EntityScan(basePackages = {"nl.ricta.ams*", "nl.ricta.ams.*"})
public class ApplicationTest 
{
    public static void main(String[] args) 
    {
	SpringApplication.run(ApplicationTest.class, args);
    }
}
