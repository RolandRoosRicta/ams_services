package nl.ricta.ams.admin.repositories;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Date;
import java.util.List;
import nl.ricta.ams.api.entities.ActivityEntity;
import nl.ricta.ams.api.entities.ProcessStateActivityEntity;
import org.apache.commons.collections.IteratorUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

/**
 *
 * @author rr
 */
@RunWith(SpringRunner.class)
@DataJpaTest(showSql = true)

//@ActiveProfiles("test") // Like this
//@AutoConfigureTestDatabase(replace = NONE)
public class StateActivityRepositoryTest {

    @Autowired
    StateActivityRepository stateActivityRepository;

    @Test
    public void testActRepo() 
    {
        Assert.isTrue(stateActivityRepository != null);
        stateActivityRepository.findAll();
    }
    
    @Autowired
    ActivityRepository activityRepository;
   
    @Value("${ams.admin.test}")
    private  String configTest;
     
    @Test
    public void testConfigSettings() 
    {
        Assert.isTrue("jantje".equals(configTest));
    }
    
    @Test
    public void queryActivietiesWithState() 
    {

        ActivityEntity act  = ActivityEntity.builder()
               .creationDate(new Date(2019, 1,1))
               .description("desc")
               .expirationDate(new Date(2019, 1,1))
               .id(1L)
               .prio("prio")
               .requestorName("rr")
               .type("type")
               .status("open")
               .title("title")
               .build();
        activityRepository.save(act);
         
        ProcessStateActivityEntity stateAct = 
                ProcessStateActivityEntity.builder()
                        //.activityOfState(act)
                        //.state(new String[]{"jo:1", "po:2"})
                         .activityState("'jo:1', 'po:2'")
                        .build();
        
        stateActivityRepository.save(stateAct);
        
       
          
          List<ProcessStateActivityEntity> procesStateActs = IteratorUtils.toList(stateActivityRepository.findAll().iterator());
         Assert.isTrue(procesStateActs.size() == 1L);
         
         // List<ProcessStateActivityEntity> actsWithState = IteratorUtils.toList(stateActivityRepository.findAllActivitiesWithState().iterator());
          //ActivitiesWithState();
//        ApiKey apiKey = new ApiKey();
//        apiKey.setKeyValue("anything");
//        apiKeyRepository.save(apiKey);
//
//        ServiceApi service = new ServiceApi();
//        service.setName("something");
//        service.setUriIdentifier("service");
//        service.setLocalUrl("https://www.google.com");
//        serviceApiRepository.save(service);
//
//        User user = new User();
//        user.setUsername("something");
//        user.setFirstname("first");
//        user.setLastname("Last");
//        user.setCompany("company");
//        user.setEmail("test@gmail.com");
//        user.setInitialized(Boolean.FALSE);
//        userRepository.save(user);
//
//        apiKey.setServiceApi(service);
//        apiKey.setUser(user);
//
//        Proxy proxy = new Proxy();
//        proxy.setName("something");
//
//        Metric metric = new Metric();
//        metric.setCount(1);
//        metric.setApiKey(apiKey);
//        //metric.setProxy(proxy);
//
//        metricRepository.save(metric);

    }

}
