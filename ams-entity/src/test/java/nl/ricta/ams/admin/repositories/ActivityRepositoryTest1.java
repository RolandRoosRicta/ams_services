package nl.ricta.ams.admin.repositories;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Date;
import java.util.Optional;
//import java.util.List;
import nl.ricta.ams.api.entities.ActivityEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

/**
 *
 * @author rr
 */
//@EnableAutoConfiguration
//@SpringBootConfiguration
//@SpringBootTest
//@AutoConfigureTestDatabase
//@Configuration

//@ActiveProfiles("test") // Like this
//@AutoConfigureTestDatabase(replace = NONE)
@RunWith(SpringRunner.class)
@DataJpaTest(showSql = true)
public class ActivityRepositoryTest1 {



    @Autowired
    ActivityRepository activityRepository;
   

     @Test
    public void findNotOneByIdTest() 
    {
         Optional<ActivityEntity> actToFind = activityRepository.findById(1L);
         Assert.isTrue(actToFind == null);
    }

    
    @Test
    public void findOneByIdTest() 
    {
         ActivityEntity act  = ActivityEntity.builder()
                .creationDate(new Date(2019, 1,1))
                .description("desc")
                .expirationDate(new Date(2019, 1,1))
                .id(1L)
                .prio("prio")
                .requestorName("rr")
                .type("type")
                .status("open")
                .title("title")
                .build();
          activityRepository.save(act);
          
         Optional<ActivityEntity> actToFind = activityRepository.findById(1L);
         Assert.isTrue(actToFind != null);
    }
    
    @Test
    public void findOneByIdWithoutIdValueTest() 
    {
        //Create one first and save it
        ActivityEntity act  = ActivityEntity.builder()
                .creationDate(new Date(2019, 1,1))
                .description("desc")
                .expirationDate(new Date(2019, 1,1))
                //.id(1L)
                .prio("prio")
                .requestorName("rr")
                .type("type")
                .status("open")
                .title("title")
                .build();
         Assert.isTrue(act.getId() == null);
         activityRepository.save(act);
         Assert.isTrue(act.getId() == 1L);
         
         //query back
         Optional<ActivityEntity> actToFind = activityRepository.findById(1L);
         Assert.isTrue(actToFind != null);


    }
 
  
}
