package nl.ricta.ams.admin.security;

//import nl.corizon.ygg.admin.model.repository.ProxyRepository;
//import nl.corizon.ygg.api.Proxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.util.Assert;

import java.util.Collections;

/**
 * Authentication provider that checks gateway auth tokens, which have been
 * added to the security context by @{@link GatewayAuthFilter}, for their
 * validity.
 * <p>
 * In case the key is valid, the gateway is considered as authenticated and the
 * role YGG_GATEWAY is added to the security context.
 *
 * @author gr-hovest
 */
public class GatewayAuthProvider implements AuthenticationProvider {

    private static final Logger log = LoggerFactory.getLogger(GatewayAuthProvider.class);

    private static final String YGG_GATEWAY_ROLE_NAME = "ROLE_YGG_GATEWAY";

  //  @Autowired
   // private ProxyRepository proxyRepository;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException 
    {

        if (authentication instanceof GatewayAuthToken) 
        {

            GatewayAuthToken authToken = (GatewayAuthToken) authentication;
            //Collections.singleton(new SimpleGrantedAuthority(YGG_GATEWAY_ROLE_NAME));
            return authentication;
        }
        return authentication;
          /*  if (isAuthTokenValid(authToken)) {

                // add ROLE_YGG_GATEWAY as granted authority
                GatewayAuthToken yggGatewayAuthToken = new GatewayAuthToken(
                        (String) authToken.getCredentials(), authToken.getUser(), authToken.getPassword(),
                        Collections.singleton(new SimpleGrantedAuthority(YGG_GATEWAY_ROLE_NAME))
                );
                yggGatewayAuthToken.setAuthenticated(true);
                return yggGatewayAuthToken;
            } else {
                throw new BadCredentialsException(
                        String.format("Gateway's auth token '%s' is not valid", authToken));
            }
        } else {
            // This provider is not able to decide about authentication
            return null;
        }*/
    }

    private boolean isAuthTokenValid(GatewayAuthToken authToken) throws AuthenticationException {

        try {
            Assert.notNull(authToken.getCredentials(), "the gateway's api key value is NULL");
            Assert.isInstanceOf(String.class, authToken.getCredentials());
            String apiKeyValue = (String) authToken.getCredentials();
            Assert.hasText(apiKeyValue, "the gateway's api key value does not contain any text");

            log.debug("checking gateway's APIKEY value {}", apiKeyValue);
            return true;
            // search proxy with given api key value, which is equivalent to the proxy's UUID
            /*Proxy proxy = proxyRepository.findOne(apiKeyValue);

            if (proxy != null) {
                log.debug("Proxy was found by gateway api key verifier: " + proxy);
                if (proxy.getCredentialName().equals(authToken.getUser()) && proxy.getConfigServerPassword().equals(authToken.getPassword())) {
                    return true;
                } else {
                    log.debug("Gateway's Credential doesnt match for apiKey" + apiKeyValue);
                    return false;
                }

            } else {
                log.debug("Gateway's API key value is invalid: " + apiKeyValue);
                return false;
            }*/
        } catch (IllegalArgumentException e) {
            log.debug("API key parameter missing or invalid: " + e.getMessage());
            throw new BadCredentialsException("API key parameter missing or invalid: " + e.getMessage());
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return GatewayAuthToken.class.isAssignableFrom(authentication);
    }
}
