package nl.ricta.ams.admin.security;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Represents an API key that was transmitted by an YGG gateway to access the
 * YGG admin api.
 *
 * @author gr-hovest
 */
public class GatewayAuthToken extends AbstractAuthenticationToken {

    private final String gatewayApiKey;
    private final String user;

    private final String password;

    public GatewayAuthToken(String gatewayApiKey, String user, String password, Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        this.gatewayApiKey = gatewayApiKey;
        this.user = user;
        this.password = password;

    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }
    

    @Override
    public Object getCredentials() {
        return gatewayApiKey;
    }

    @Override
    public Object getPrincipal() {
        return gatewayApiKey;
    }

}
