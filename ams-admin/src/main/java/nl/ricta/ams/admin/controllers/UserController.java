package nl.ricta.ams.admin.controllers;

//import nl.corizon.ygg.api.ApiKey;
//import io.swagger.annotations.ApiOperation;
//import io.swagger.annotations.ApiParam;

import java.util.Optional;
import nl.ricta.ams.api.vo.UserDTO;
//import nl.ricta.ams.api.dto.UserDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.ModelAttribute;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author gr-hovest(at)atb-bremen.de
 */
//@Controller
//@RequestMapping(value = "/api")

@RestController
@RequestMapping(value = "/api/users",
        produces = {"application/json"})
public class UserController {

    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    
//    @ModelAttribute("user")
//    public ApiKey getUserObject() {
//        return new ApiKey();
//    }
     @ModelAttribute("user") 
    public UserDTO getUserObjectDTO() {
        return new UserDTO();
    }
    
//      @ModelAttribute("test")
//    public Test getTestDTO() {
//        return new Test();
//    }

    
    @RequestMapping("/register")
    //public String registerForm(UserDTO user, BindingResult bindingResult, Model model)
    public String registerForm(String user,BindingResult bindingResult, Model model)
    {

        if (user == null) {
            user = "boe";//getUserObject();
        }

        model.addAttribute("user", user);

        return "register";
    }

//    @Value("${ygg.mail.sender.user}")
//    private String mail_sender_user;
//
//    @Value("${ygg.mail.sender.password}")
//    private String mail_sender_password;
//
//    @Value("${ygg.mail.verification.server.url}")
//    private String mail_verification_server_url;

    @PostMapping(value = "/register")
    //@ApiOperation(value = "rEISTER USER")
    //public String registerUser(UserDTO user, BindingResult bindingResult, Model model) //throws MessagingException
    public String registerUser(
            String userName,
           // @ApiParam(value = "User role information", required = true)
            UserDTO userDto,
           // @ApiParam(value = "BINDING RESULT", required = false)
            BindingResult bindingResult, 
            Model model)
    {

//        userValidator.validate(user, bindingResult);
//
//        if (bindingResult.hasErrors()) {
//
//            return "register";
//        }

//        user.setInitialized(Boolean.FALSE);
//
//        userRepository.save(user);
//
//        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
//        mailSender.setHost("smtp.gmail.com");
//        mailSender.setPort(587);
//        mailSender.setProtocol("smtp");
//
//        mailSender.setUsername(mail_sender_user);
//        mailSender.setPassword(mail_sender_password);
//
//        Properties props = mailSender.getJavaMailProperties();
//        props.put("mail.transport.protocol", "smtp");
//        props.put("mail.smtp.auth", "true");
//        props.put("mail.smtp.starttls.enable", "true");
//        props.put("mail.debug", "true");
//
//        MimeMessage message = mailSender.createMimeMessage();
//        MimeMessageHelper helper = new MimeMessageHelper(message, true);
//        helper.setTo(user.getEmail());
//        helper.setFrom("registration@coatrack.eu");
//        helper.setSubject("Verification of your email address");
//        helper.setText("Dear Sir or Madam </p></p></p> In order to verify your email address, please open the following link: </p> <p><a \n"
//                + "href=\"" + mail_verification_server_url + "/users/" + user.getId() + "/verify/" + user.getEmailVerifiedUrl() + "\">Verify, please </a></p>\n"
//                + "\n"
//                + "<p>Best regards</p>\n"
//                + "\n"
//                + "<p>Coatrack Team</p>", true);
//        mailSender.send(message);

        return "redirect:/admin";
    }

    @PostMapping(value = "/test")
    //@ApiOperation(value = "Create user role")
    public boolean Test(
           // @ApiParam(value = "User role information", required = true)
            UserDTO user)
    {
        return true;
    }
//    @GetMapping(value = "users/{id}/verify/{emailVerificationCode}")
//    public ModelAndView userEmailVeritification(@PathVariable("id") Long id, @PathVariable("emailVerificationCode") String emailVerificationCode) {
//
//        User user = userRepository.findOne(id);
//
//        if (user.getEmailVerifiedUrl().equals(emailVerificationCode)) {
//            user.setEmailVerified(Boolean.TRUE);
//        }
//        userRepository.save(user);
//
//        ModelAndView mav = new ModelAndView();
//
//        mav.setViewName("verified");
//        return mav;
//    }
}
