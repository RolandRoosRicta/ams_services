package nl.ricta.ams.admin;

import com.netflix.discovery.EurekaClient;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import org.modelmapper.ModelMapper;
import org.modelmapper.internal.util.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.extras.java8time.dialect.Java8TimeDialect;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templateresolver.ITemplateResolver;

//@RefreshScope
@SpringBootApplication
@Configuration
//@ConfigurationProperties
@EnableScheduling
@EnableJpaRepositories("nl.ricta.ams*")
@ComponentScan(basePackages = {"nl.ricta.ams*", "nl.ricta.ams.*","nl.ricta.featureflag.*","nl.ricta.featureflag*","nl.ricta.ams.admin.services.*","nl.ricta.ams.admin.services*","nl.ricta.ams.admin.services.interfaces*"})
@EntityScan(basePackages = {"nl.ricta.ams*", "nl.ricta.ams.*"})
@ImportResource({"classpath*:applicationContext.xml"})
//@EnableDiscoveryClient
@EnableEurekaClient
public class AmsAdminApplication //implements GreetingController
{
     private static final Logger log = LoggerFactory.getLogger(AmsAdminApplication.class);

    @Autowired
    @Lazy
    private EurekaClient eurekaClient;
 
    @Value("${spring.application.name}")
    private String appName;
    
    public static void main(String[] args) 
    {
		
       SpringApplication.run(AmsAdminApplication.class, args);
        //new SpringApplicationBuilder(AccommodationApplication.class).web(WebApplicationType.SERVLET).run(args);
    }
 
    //Either by this bean, or in the applicationContext.xml, the ModelMapper type must be added to the container
//     @Bean
//    public ModelMapper modelMapper() {
//        return new ModelMapper();
//    }
    
    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) 
    {
        
        return args -> 
        {
            log.debug("Let's inspect the beans provided by Spring Boot:");

            String[] beanNames = ctx.getBeanDefinitionNames();
            Arrays.sort(beanNames);
            List<String> listBeans =  Arrays.asList(beanNames);
            Assert.isTrue(listBeans.contains("modelMapper"), "Either supply a Java Bean, or a Configuration for Type ModelMapper in the Xml");
            for (String beanName : beanNames) 
            {
               //  log.debug(beanName);
            }
        };
    }
    
  
    
    @Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.gmail.com");
        mailSender.setPort(587);

        mailSender.setUsername("roos.ict.architectures@gmail.com");
        mailSender.setPassword("Roos5000");

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");

        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
    
        return mailSender;
    }

    
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    private TemplateEngine templateEngine(ITemplateResolver templateResolver) {
        SpringTemplateEngine engine = new SpringTemplateEngine();
        engine.addDialect(new Java8TimeDialect());
        engine.setTemplateResolver(templateResolver);
        return engine;
    }

    @Bean
    public Java8TimeDialect java8TimeDialect() {
        return new Java8TimeDialect();
    }

}
