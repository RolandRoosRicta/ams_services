package nl.ricta.ams.admin;

//import nl.corizon.ygg.admin.security.GatewayAuthFilter;
//import nl.corizon.ygg.admin.security.GatewayAuthProvider;
//import  nl.ricta.ams.admin.security;
import nl.ricta.ams.admin.security.GatewayAuthFilter;
import nl.ricta.ams.admin.security.GatewayAuthProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


/**
 * WebSecurityConfigurerAdapter for the API that is accessed by the gateways.
 * <p>
 * This needs to be separated from the WebSecurityConfig because the authentication
 * is not done via form login but via an API key.
 * <p>
 * The @Order annotation is used to assure that this config is considered first.
 *
 * @author gr-hovest(at)atb-bremen.de
 */
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Order(1)
public class ApiSecurityConfig extends WebSecurityConfigurerAdapter implements WebMvcConfigurer {

    //private static final String GATEWAY_API_RESOURCE_MATCHER = "/api/**";
    private static final String GATEWAY_API_RESOURCE_MATCHER = "/none/**";
    private static final String GATEWAY_ROLE_NAME = "YGG_GATEWAY";

    //@Bean
    //GatewayAuthProvider gatewayAuthProvider() {
    //    return new GatewayAuthProvider();
    //}

//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) {
//        auth.authenticationProvider(new GatewayAuthProvider());
//                //gatewayAuthProvider());
//    }
private static final String[] AUTH_WHITELIST = {
        "/swagger-resources/**",
        "/swagger-ui.html",
        "/v2/api-docs",
        "/webjars/**"
};
    protected void configure(HttpSecurity http) throws Exception {

        http
                .antMatcher(GATEWAY_API_RESOURCE_MATCHER)
                .csrf().disable()
                .addFilterAfter(new GatewayAuthFilter(), BasicAuthenticationFilter.class)
                .authorizeRequests()
                .anyRequest().hasRole(GATEWAY_ROLE_NAME);
                
               // .antMatchers("/admin/**", "/images/**","/templates/**").permitAll();
         //http.authorizeRequests()
         //   .antMatchers("/admin/**", "/images/**","/templates/**").permitAll();
         
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }
    
    //TODO SWAGGER make work
//    @Override
//    public void configure(WebSecurity web) throws Exception {
//        web.ignoring().antMatchers(AUTH_WHITELIST);
//    }
//    
//    @Override 
//        public void addResourceHandlers(ResourceHandlerRegistry registry) {
//            registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
//            registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
//        }
        
}

