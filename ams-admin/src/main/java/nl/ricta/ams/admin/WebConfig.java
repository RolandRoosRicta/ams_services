package nl.ricta.ams.admin;

//import nl.corizon.ygg.admin.interceptor.AdminInterceptor;
import nl.ricta.ams.admin.interceptor.AdminInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@ComponentScan(basePackages = {"nl.ricta.ams.admin.controllers"})
//@EnableWebMvc
public class WebConfig extends WebMvcConfigurerAdapter {

    @Autowired
    AdminInterceptor interceptor;
    @Value("${ams.admin.test}")
    private  String configTest;
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        // Register admin interceptor with multiple path patterns
        registry.addInterceptor(interceptor).addPathPatterns(new String[]{"/admin", "/admin/*", "/admin/**/*", "/admin/**/**/*"});
    }
//
//     @Bean
//    public ViewResolver getViewResolver() {
//        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
//       // resolver.setPrefix("templates/");
//        //resolver.setSuffix(".html");
//        return resolver;
//    }
//
//    @Override
//    public void configureDefaultServletHandling(
//            DefaultServletHandlerConfigurer configurer) {
//        configurer.enable();
//    }
    
    /**
     *
     *
     * Based on
     * solution:https://blog.georgovassilis.com/2015/10/29/spring-mvc-rest-controller-says-406-when-emails-are-part-url-path/
     *
     * @param configurer
     */
    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        // Simple strategy: only path extension is taken into account
        configurer.favorPathExtension(true).
                favorParameter(false).
                parameterName("mediaType").
                ignoreAcceptHeader(false).
                useJaf(false).
                defaultContentType(MediaType.APPLICATION_JSON).
                mediaType("xml", MediaType.APPLICATION_XML).
                mediaType("json", MediaType.APPLICATION_JSON);
        //use e.g. curl -H "Content-Type: application/json" -d {} http://localhost:9020/actuator/refresh
         //       mediaType("json", MediaType.APPLICATION_FORM_URLENCODED);
    }
    
//     @Override
//    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
//        configurer.favorPathExtension(true).
//                favorParameter(false).
//                ignoreAcceptHeader(true).
//                useJaf(false).
//                defaultContentType(MediaType.APPLICATION_JSON);
//    }
}
