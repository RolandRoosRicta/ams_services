package nl.ricta.ams.admin;

//import nl.corizon.ygg.admin.model.repository.*;
//import nl.ricta.ams.api.entities;
//import nl.ricta.ams.api.vo;
//import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import org.springframework.http.HttpMethod;

/**
 * @author r.roos
 */
@Component
public class AmsDatabaseInitializer {

   // private static final Logger log = LoggerFactory.getLogger(AmsDatabaseInitializer.class);
//
//    @Autowired
//    ProxyRepository proxyRepository;
//
//    @Autowired
//    ApiKeyRepository apiKeyRepository;
//
//    @Autowired
//    ServiceApiRepository serviceApiRepository;
//
//    @Autowired
//    MetricRepository metricRepository;
//
//    @Autowired
//    UserRepository userRepository;
//
//    @Autowired
//    EntryPointRepository entryPointRepository;

      
    @Value("${ams.admin.database.insertSampleDataOnStartup}")
    private boolean sampleDataShouldBeInserted;

    
    // api provider username can be configured as test user's github login name
    // use europool as default value, e.g. for unit testing
    //@Value("${ygg.admin.database.sampleDataUsername:europool}")
    //private String sampleDataApiProviderUsername;

    // api consumer username can be configured as test user's github login name
    // use perezdf as default value, e.g. for unit testing
    //@Value("${ygg.admin.database.sampleDataUsernameApiConsumer:perezdf2}")
    //private String sampleDataApiConsumerUsername;

    @PostConstruct
    public void createPresentationData() {

        if (sampleDataShouldBeInserted) {

            //log.info("Inserting sample data into the database");

            //log.debug("sample data api provider username is {}", sampleDataApiProviderUsername);
//            User apiProviderUser = createNewUser("Europool", sampleDataApiProviderUsername, "Arthur", "Admin");
//
//            User edekaUser = createNewUser("Edeka", "edeka", "James", "Bond");
//            User frankFarmerUser = createNewUser("Mc Donald", sampleDataApiConsumerUsername, "Frank", "Farmer");
//            User richardRetailerUser = createNewUser("Apple", "Richard Retailer", "Richard", "Retailer");
//            User waltWholesalerUser = createNewUser("Jaguar", "Walt Wholesaler", "Walt", "Wholesaler");
//
//            ServiceApi serviceApiCTS = createNewServiceAPI(
//                    apiProviderUser,
//                    "Crate Tracking Service",
//                    "crate-tracking",
//                    "http://192.168.55.14:8086/cts",
//                    ServiceAccessPermissionPolicy.PERMISSION_NECESSARY,
//                    ServiceAccessPaymentPolicy.WELL_DEFINED_PRICE,
//                    new BigDecimal(2.0),
//                    new BigDecimal(15.9),
//                    "Service for EPS customers to get information about the location and the latest depot scan of their crates. To be passed as parameter 'crateID' is the GRAI.");
//
//            ServiceApi serviceApiOrderDE = createNewServiceAPI(
//                    apiProviderUser,
//                    "Crate Ordering Service DE",
//                    "order-crates-de",
//                    "http://192.168.47.3:8087/orders",
//                    ServiceAccessPermissionPolicy.PERMISSION_NECESSARY,
//                    ServiceAccessPaymentPolicy.FOR_FREE,
//                    null,
//                    null,
//                    "EPS customers in Germany can use this service to order crates.");
//
//            ServiceApi serviceApiOrderNL = createNewServiceAPI(
//                    apiProviderUser,
//                    "Crate Ordering Service NL",
//                    "order-crates-nl",
//                    "http://192.168.55.19:8087/orders",
//                    ServiceAccessPermissionPolicy.PERMISSION_NECESSARY,
//                    ServiceAccessPaymentPolicy.FOR_FREE,
//                    null,
//                    null,
//                    "EPS customers in the Netherlands can use this service to order crates.");
//
//            Set<ServiceApi> collection = new HashSet<>();
//            collection.add(serviceApiCTS);
//            collection.add(serviceApiOrderNL);
//
//            Proxy proxyEpsNL = createNewProxy(
//                    "aa11aa22-aa33-aa44-aa55-aa66aa77aa88",
//                    "EPS gateway NL",
//                    collection,
//                    "http://localhost:8088",
//                    "Gateway running in EPS data processing centre in the Netherlands", apiProviderUser);
//
//            collection = new HashSet<>();
//            collection.add(serviceApiOrderDE);
//            createNewProxy(
//                    "bb11bb22-bb33-bb44-bb55-bb66bb77bb88",
//                    "EPS gateway DE",
//                    collection,
//                    "http://localhost:8088/",
//                    "Gateway running in EPS data processing centre in Germany", apiProviderUser);
//
//            ApiKey edekaKey = createNewApiKey(
//                    "ee11ee22-ee33-ee44-ee55-ee66ee77ee88",
//                    edekaUser,
//                    DateUtils.addDays(new Date(), -40), serviceApiOrderDE);
//
//            ApiKey frankFarmerKeyServiceApiCTS = createNewApiKey(
//                    "ff11ff22-ff33-ff44-ff55-ff66ff77-cts",
//                    frankFarmerUser,
//                    DateUtils.addDays(new Date(), -25), serviceApiCTS);
//
//            ApiKey frankFarmerKeyServiceApiOrderCratesNL = createNewApiKey(
//                    "ff11ff22-ff33-ff44-ff55-ff66ff77-onl",
//                    frankFarmerUser,
//                    DateUtils.addDays(new Date(), -25), serviceApiOrderNL);
//
//            ApiKey richardRetailerKey = createNewApiKey(
//                    "rr11rr22-rr33-rr44-rr55-rr66rr77rr88",
//                    richardRetailerUser,
//                    DateUtils.addDays(new Date(), -9), serviceApiCTS);
//
//            ApiKey richardRetailerKeyExpired = createNewApiKey(
//                    "rr11rr22-rr33-rr44-rr55-rr66-expired",
//                    richardRetailerUser,
//                    DateUtils.addDays(new Date(), -100), serviceApiCTS);
//
//            ApiKey waltWholesalerKey = createNewApiKey(
//                    "ww11ww22-ww33-ww44-ww55-ww66ww77ww88",
//                    waltWholesalerUser,
//                    DateUtils.addDays(new Date(), -8), serviceApiCTS);
//
//            String metricsCounterSessionID = UUID.randomUUID().toString();
//            LocalDate today = LocalDate.now();
//
//            // Frank Farmer
//            addMetrics(proxyEpsNL,
//                    frankFarmerKeyServiceApiOrderCratesNL,
//                    "POST",
//                    "/order/large",
//                    2,
//                    1,
//                    1,
//                    metricsCounterSessionID,
//                    today.minusDays(9));
//            addMetrics(proxyEpsNL,
//                    frankFarmerKeyServiceApiOrderCratesNL,
//                    "POST",
//                    "/order/small",
//                    2,
//                    0,
//                    0,
//                    metricsCounterSessionID,
//                    today.minusDays(8));
//            addMetrics(proxyEpsNL,
//                    frankFarmerKeyServiceApiCTS,
//                    "GET",
//                    "/historic",
//                    2,
//                    0,
//                    0,
//                    metricsCounterSessionID,
//                    today.minusDays(4));
//            addMetrics(proxyEpsNL,
//                    frankFarmerKeyServiceApiCTS,
//                    "GET",
//                    "/current",
//                    9,
//                    0,
//                    0,
//                    metricsCounterSessionID,
//                    today.minusDays(2));
//
//            // Richard Retailer
//            addMetrics(proxyEpsNL,
//                    richardRetailerKey,
//                    "GET",
//                    "/historic",
//                    82,
//                    4,
//                    8,
//                    metricsCounterSessionID,
//                    today.minusDays(12));
//            addMetrics(proxyEpsNL,
//                    richardRetailerKey,
//                    "GET",
//                    "/current",
//                    8,
//                    4,
//                    0,
//                    metricsCounterSessionID,
//                    today.minusDays(6));
//            addMetrics(proxyEpsNL,
//                    richardRetailerKey,
//                    "PUT",
//                    "/current/modify",
//                    9,
//                    1,
//                    0,
//                    metricsCounterSessionID,
//                    today.minusDays(5));
//            addMetrics(proxyEpsNL,
//                    richardRetailerKey,
//                    "GET",
//                    "/current",
//                    7,
//                    0,
//                    0,
//                    metricsCounterSessionID,
//                    today.minusDays(4));
//            addMetrics(proxyEpsNL,
//                    richardRetailerKey,
//                    "POST",
//                    "/current/add",
//                    11,
//                    0,
//                    1,
//                    metricsCounterSessionID,
//                    today.minusDays(3));
//            addMetrics(proxyEpsNL,
//                    richardRetailerKey,
//                    "GET",
//                    "/current",
//                    8,
//                    0,
//                    0,
//                    metricsCounterSessionID,
//                    today.minusDays(2));
//            addMetrics(proxyEpsNL,
//                    richardRetailerKey,
//                    "GET",
//                    "/historic",
//                    9,
//                    0,
//                    2,
//                    metricsCounterSessionID,
//                    today.minusDays(1));
//            addMetrics(proxyEpsNL,
//                    richardRetailerKey,
//                    "GET",
//                    "/historic",
//                    7,
//                    0,
//                    0,
//                    metricsCounterSessionID,
//                    today);
//
//            // Walt Wholesaler
//            addMetrics(proxyEpsNL,
//                    waltWholesalerKey,
//                    "GET",
//                    "/current",
//                    40,
//                    2,
//                    0,
//                    metricsCounterSessionID,
//                    today.minusDays(9));
//            addMetrics(proxyEpsNL,
//                    waltWholesalerKey,
//                    "GET",
//                    "/historic",
//                    18,
//                    2,
//                    0,
//                    metricsCounterSessionID,
//                    today.minusDays(3));
//            addMetrics(proxyEpsNL,
//                    waltWholesalerKey,
//                    "GET",
//                    "/current",
//                    13,
//                    0,
//                    1,
//                    metricsCounterSessionID,
//                    today.minusDays(2));
//            addMetrics(proxyEpsNL,
//                    waltWholesalerKey,
//                    "GET",
//                    "/historic",
//                    17,
//                    0,
//                    0,
//                    metricsCounterSessionID,
//                    today.minusDays(1));
//            addMetrics(proxyEpsNL,
//                    waltWholesalerKey,
//                    "GET",
//                    "/current",
//                    28,
//                    0,
//                    0,
//                    metricsCounterSessionID,
//                    today);
        }
    }

//
//    private User createNewUser(String company, String username, String firstname, String familyname) {
//        User user = new User();
//        user.setCompany(company);
//        user.setUsername(username);
//        user.setFirstname(firstname);
//        user.setLastname(familyname);
//        user.setInitialized(Boolean.FALSE);
//        user.setEmail("test@gmail.com");
//        userRepository.save(user);
//        return user;
//    }
//
//    private ApiKey createNewApiKey(
//            String keyValue,
//            User consumer,
//            Date created,
//            ServiceApi serviceApi) {
//
//        ApiKey apiKey = new ApiKey();
//
//        apiKey.setUser(consumer);
//        apiKey.setCreated(created);
//        apiKey.setValidUntil(DateUtils.addMonths(apiKey.getCreated(), 3));
//        apiKey.setKeyValue(keyValue);
//        apiKey.setServiceApi(serviceApi);
//
//        apiKey = apiKeyRepository.save(apiKey);
//
//        serviceApi = serviceApiRepository.findOne(serviceApi.getId());
//        serviceApi.getApiKeys().add(apiKey);
//        serviceApiRepository.save(serviceApi);
//
 //       return apiKey;
 //   }

//    private Proxy createNewProxy(
//            String uuid,
//            String name,
//            Set<ServiceApi> serviceApis,
//            String publicUrl,
//            String description, User user) {
//
//        Proxy proxy = new Proxy();
//        proxy.setId(uuid);
//        proxy.setName(name);
//        proxy.setServiceApis(serviceApis);
//        proxy.setPublicUrl(publicUrl);
//        proxy.setPort(8088);
//        proxy.setDescription(description);
//        proxy.setOwner(user);
//        return proxyRepository.save(proxy);
//    }
//
//    private ServiceApi createNewServiceAPI(
//            User apiProvider,
//            String name,
//            String uriIdentifier,
//            String localUrl,
//            ServiceAccessPermissionPolicy serviceAccessPermissionPolicy,
//            ServiceAccessPaymentPolicy serviceAccessPaymentPolicy,
//            BigDecimal pricePerCall,
//            BigDecimal pricePerMonth,
//            String description) {
//
//        ServiceApi serviceApi = new ServiceApi();
//        serviceApi.setOwner(apiProvider);
//        serviceApi.setName(name);
//        serviceApi.setUriIdentifier(uriIdentifier);
//        serviceApi.setLocalUrl(localUrl);
//        serviceApi.setServiceAccessPermissionPolicy(serviceAccessPermissionPolicy);
//        serviceApi.setServiceAccessPaymentPolicy(serviceAccessPaymentPolicy);
//
//        serviceApi.setDescription(description);
//
//        EntryPoint entryPoint = new EntryPoint();
//        entryPoint.setName("Read historical data");
//        entryPoint.setPathPattern("/historic");
//        entryPoint.setPricePerCall(1.2);
//        entryPoint.setHttpMethod(HttpMethod.GET.toString());
//
//        // EntryPoint entryPointStored = entryPointRepository.save(entryPoint);
//        Set<EntryPoint> entryPoints = new HashSet<>();
//
//        entryPoints.add(entryPoint);
//        serviceApi.setEntryPoints(entryPoints);
//
//        return serviceApiRepository.save(serviceApi);
//    }
//
//    private void addMetrics(
//            Proxy proxy,
//            ApiKey apiKey,
//            String requestMethod,
//            String path,
//            int countHttp200,
//            int countHttp404,
//            int countHttp500,
//            String metricsCounterSessionID,
//            LocalDate dateOfApiCall) {
//
//        addMetric(proxy,
//                apiKey,
//                requestMethod,
//                path,
//                MetricType.AUTHORIZED_REQUEST,
//                countHttp200 + countHttp404 + countHttp500,
//                null,
//                metricsCounterSessionID,
//                dateOfApiCall);
//
//        addMetric(proxy,
//                apiKey,
//                requestMethod,
//                path,
//                MetricType.RESPONSE,
//                countHttp200,
//                200,
//                metricsCounterSessionID,
//                dateOfApiCall);
//        if (countHttp404 > 0) {
//            addMetric(proxy,
//                    apiKey,
//                    requestMethod,
//                    path + "/crate-id",
//                    MetricType.RESPONSE,
//                    countHttp404,
//                    404,
//                    metricsCounterSessionID,
//                    dateOfApiCall);
//        }
//        if (countHttp500 > 0) {
//            addMetric(proxy,
//                    apiKey,
//                    requestMethod,
//                    path,
//                    MetricType.RESPONSE,
//                    countHttp500,
//                    500,
//                    metricsCounterSessionID,
//                    dateOfApiCall);
//        }
//
//    }
//
//    private void addMetric(
//            Proxy proxy,
//            ApiKey apiKey,
//            String requestMethod,
//            String path,
//            MetricType type,
//            int count,
//            Integer httpResponseCode,
//            String metricsCounterSessionID,
//            LocalDate dateOfApiCall) {
//
//        Metric metric = new Metric();
//        metric.setProxy(proxy);
//
//        metric.setRequestMethod(requestMethod);
//        metric.setPath(path);
//        metric.setMetricsCounterSessionID(metricsCounterSessionID);
//        metric.setDateOfApiCall(java.sql.Date.valueOf(dateOfApiCall));
//        metric.setType(type);
//        metric.setCount(count);
//        metric.setHttpResponseCode(httpResponseCode);
//        metric.setProxy(proxy);
//        metric.setApiKey(apiKey);
//        metricRepository.save(metric);
//
//        Metric returnMetric = metricRepository.findOne(metric.getId());
//        Proxy proxies = returnMetric.getProxy();
//        Set<ServiceApi> services = proxies.getServiceApis();
//        //List<ApiKey> returnApiKeys = services.iterator().next().getApiKeys();
//        //returnApiKeys.get(0).getId();
    //}
}
