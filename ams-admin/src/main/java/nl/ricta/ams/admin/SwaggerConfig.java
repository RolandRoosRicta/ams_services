/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.ricta.ams.admin;


//import springfox.documentation.builders.ApiInfoBuilder;
//import springfox.documentation.builders.PathSelectors;
//import springfox.documentation.builders.RequestHandlerSelectors;
//import springfox.documentation.service.ApiInfo;
//import springfox.documentation.service.Contact;
//import springfox.documentation.spi.DocumentationType;
//import springfox.documentation.spring.web.plugins.Docket;
//import springfox.documentation.swagger2.annotations.EnableSwagger2;

import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import org.springframework.context.annotation.Configuration;



/**
 *
 * @author roosr
 */
//!!!!@Configuration is vital!! if you dont use it, public Docket api() bean is never called!
@Configuration
//!!!!!!!

@EnableSwagger2
public class SwaggerConfig {
    
//This needs to be called suring startup. Debug, set breakpoint, make sure you get here at startup!
@Bean
 public Docket api()
 {
     return new Docket(DocumentationType.SWAGGER_2)
     .select()
    .apis(RequestHandlerSelectors.any())
    .paths(PathSelectors.any())
    .build();
 }
// @Bean
//    public Docket api()
//    {
//	System.out.println("docket created for end-point [" + "/v2/api-docs" + "]");
//        return new Docket(DocumentationType.SWAGGER_2)
//        		.apiInfo(apiInfo())
//           		.pathMapping("/v2/api-docs")
//				.select()
//       			.apis(RequestHandlerSelectors.any())
//       			.paths(PathSelectors.any())
//       			.build();
//    }
//  private ApiInfo apiInfo()
//    {
//    	return new ApiInfoBuilder()
//    			.title("swaggertest")
//    			.description("here goes desc")
//    			.version("1.0")
//    			.build();
//    }
//	
//    @Bean
//    public DispatcherServlet dispatcherServlet()
//    {
//	System.out.println("dispatcherServlet started");
//    	return new DispatcherServlet();
//    }	
  //
    // In the browser you can find Swagger2 on http://localhost:8002/swagger-ui.html#/
    //
    
    
  
//    @Bean
//    public Docket api() {
//        return new Docket(DocumentationType.SWAGGER_2).select()
//            .apis(RequestHandlerSelectors
//                .basePackage("nl.ricta.ams.controllers"))
//            .paths(PathSelectors.regex("/.*"))
//            .build().apiInfo(apiEndPointsInfo());
//    }
//    private ApiInfo apiEndPointsInfo() {
//        return new ApiInfoBuilder().title("Travel Products REST API")
//            .description("A view to see what all entities of this API enclude")
//           // .contact(new Contact("Bebr support", "https://bebr.nl/", "support@bebr.nl"))
//            .version(getClass().getPackage().getImplementationVersion())
//            .build();
//    }
    
  

}


