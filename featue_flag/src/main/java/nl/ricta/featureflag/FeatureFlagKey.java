/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.ricta.featureflag;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

 /**
*  Meta data Annotation for AOP Feature flag functionality
*  
* <pre>
* --example :
* <code>
*    public class FeatureAnnotationAspect 
*    {
*       {@literal@}FeatureFlagKey(features = {"feature1", "feature2"}, paramNameForKeyOfFeature = "user")
*       public void Annotate( String user, Model model, ModelAndView modelandView)
*       {
*           Assert.isTrue(model.containsAttribute("feature1"));//if user = "user1" allowed in feature config.yml for feature1
*           Assert.isTrue(model.containsAttribute("feature2"));//if user = "user1" allowed in feature config.yml for feature2
*       }
*    }
* 
*   calling:
*        Model model = new ModelImpl();
*        ModelAndView modelAndView = new ModelAndView();
*        aspectTest.Annotate("user1", model,modelAndView);
*        Assert.isTrue(model.containsAttribute("feature1"));
*        Assert.isTrue(model.containsAttribute("feature2"));
* 
*   with config.yml:
*    
*   featureFlags:
        feature1:
            enabled : true
        feature2:
            enabled : true 
* </code>
* </pre>
* @author Roland Roos
*
*/
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface FeatureFlagKey 
{
    //The list of available 
    String[] features() default {""};
    String paramNameForKeyOfFeature() default "";
}
