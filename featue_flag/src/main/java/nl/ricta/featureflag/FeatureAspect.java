/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.ricta.featureflag;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.stream.Collectors;
import nl.ricta.featureflag.FeatureFlags.Feature;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;
/**
 * AOP class Responsible for injecting available features in the method api (Model, ModelAndView)
 * @author Roland Roos
 */
@Aspect
@Component
public class FeatureAspect 
{
    /**
     * Logger to log any info
     */
    private static final Logger log = LoggerFactory.getLogger(FeatureAspect.class);
    
    /**
     * Service delivering the actual Feature for a key/feature name combi
     */
    @Autowired
    private FeatureFlagService  featureConfigService; 
    
     /**
     *  Injects available feature flags into Model and/or ModelAndView of API. @see unitests for examples 
     *
     *
     * @param joinPoint  place of method annotated
     * @param featureFlagKey the object with fields of the FeatureFlagKey annotation
     * @return proceeding object return value of method being annotated
     */
    @Around("@annotation(featureFlagKey)")
    public Object featureFlagKey(
            final  ProceedingJoinPoint joinPoint,
            FeatureFlagKey featureFlagKey) 
                throws Throwable 
    {
        String features = Arrays.asList(featureFlagKey.features())
                .stream()
                .collect(Collectors.joining(","));

        log.info("for features {} of param {}", features, featureFlagKey.paramNameForKeyOfFeature());
        Method method = MethodSignature.class.cast(joinPoint.getSignature()).getMethod();
        log.debug("on {}", method.getName());
        Object[] args = joinPoint.getArgs();
        MethodSignature methodSignature = (MethodSignature) joinPoint.getStaticPart().getSignature();
        String[] paramNames = methodSignature.getParameterNames();

        Class[] paramTypes= methodSignature.getParameterTypes();
        String curKey = null;
        Model modelParam = null;
        ModelAndView modelAndViewParam = null;
        for (int argIndex = 0; argIndex < args.length; argIndex++)
        {
            String curParamName = paramNames[argIndex];

            Object curValue = args[argIndex];

            if (curParamName.equals(featureFlagKey.paramNameForKeyOfFeature()))
            {
                curKey = curValue.toString();
            }
            log.debug("arg {} value {}", curParamName, curValue);

            if (curValue instanceof Model)
            {
                modelParam = (Model)curValue;
            }
            else if (curValue instanceof ModelAndView)
            {
                modelAndViewParam = (ModelAndView)curValue;
            }
        }
        if (curKey != null)
        {
            for(String featureName : featureFlagKey.features())
            {
                Feature curFeature = featureConfigService.getFeature(curKey.toString(), featureName);
                 if (curFeature != null )
                 {
                     if (modelParam != null)
                     {
                         modelParam.addAttribute(featureName, curFeature);
                     }
                      if (modelAndViewParam != null)
                     {
                         modelAndViewParam.addObject(featureName, curFeature);
                     }
                 }
            }
        }
        long start = System.currentTimeMillis();
        
        Object proceed = joinPoint.proceed();
        long executionTime = System.currentTimeMillis() - start;
        log.debug(joinPoint.getSignature() + " executed in " + executionTime + "ms");
        return proceed;
    }
    
//    @After("@annotation(featureFlagKey) || @within(featureFlagKey)")
//    public void  featureFlagKey(
//            JoinPoint joinPoint,
//            FeatureFlagKey featureFlagKey)
//    {
//        System.out.println("featureFlagKey");
//    }
    
}
