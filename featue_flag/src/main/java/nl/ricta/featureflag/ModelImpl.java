/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.ricta.featureflag;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.springframework.ui.Model;

/**
 * A default implementation for injecting features in the model according to the @FeatureFlag annotation
 * @author roosr
 */
public class ModelImpl implements Model
{

    @Override
    public Model addAttribute(String key, Object o) 
    {
        asMap().put(key, o);
        return this;
    }

    @Override
    public Model addAttribute(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Model addAllAttributes(Collection<?> clctn) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Model addAllAttributes(Map<String, ?> map) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Model mergeAttributes(Map<String, ?> map) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean containsAttribute(String key) {
        return asMap().containsKey(key);
    }
    Map<String, Object> map = new  HashMap<String, Object> ();
    @Override
    public Map<String, Object> asMap() {
        return map;
    }

    
}
