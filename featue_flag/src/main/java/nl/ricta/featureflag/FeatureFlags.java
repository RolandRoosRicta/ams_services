/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.ricta.featureflag;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * The structure that holds the parsed yml info
 */
@Configuration
@ConfigurationProperties
public class FeatureFlags 
{
    private static final Logger log = LoggerFactory.getLogger(FeatureFlags.class);
    //really does not matter how you call your pop. As long as the method name matched
    private final Map<String, Feature> featureFlags1 = new HashMap<>();

    //it does matter how you call this method. get<rootNameInYaml> -> getFeatureFlags
    public Map<String, Feature> getFeatureFlags() 
    {
        log.debug("getFeatureFlags");
        return featureFlags1;
    }
    private final Map<String, KeyGroup> keygroups = new HashMap<>();

    //it does matter how you call this method. get<rootNameInYaml> -> getKeyGroups
    public Map<String, KeyGroup> getKeyGroups() 
    {
        log.debug("getKeyGroups");
        return keygroups;
    }
    
    @Data
    public static class Feature {

        /*
        enabled: true
        included :
            feature1Group
        excluded :
            dev.example.com.test,
            another.example.com.test 
        */
       private String enabled;
       private List<String> included;
       private List<String> excluded;
    }
    
    @Data
    public static class KeyGroup 
    {
       private List<String> keys;
    }
}
