/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.ricta.featureflag;

import java.util.List;
import java.util.Map;
import nl.ricta.featureflag.FeatureFlags.Feature;
import nl.ricta.featureflag.FeatureFlags.KeyGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;
import static org.valid4j.Assertive.*;
/**
 * Activity service implementation
 *
 * The service layer is responsible for providing accommodation services like
 * find and storing them.
 *
 *
 *
 * @author r.roos
 */
@Service
@ConfigurationProperties
public class FeatureFlagServiceImpl implements FeatureFlagService
{
    private static final Logger log = LoggerFactory.getLogger(FeatureFlagServiceImpl.class);

    //@Autowired
    private FeatureFlags  featureConfig;  

    @Autowired
    public FeatureFlagServiceImpl(FeatureFlags  featureConfig)
    {
        log.debug("FeatureFlagServiceImpl : #features = {}", featureConfig.getFeatureFlags().size());
        this.featureConfig = featureConfig;
    }
    
    public Map<String, Feature> getFeatureFlags() 
    {
        return this.featureConfig.getFeatureFlags();
    }
    
    public Feature getFeature(String forKey, String forFeatureName)
    {
        require(featureConfig != null);
        require(forKey != null);
        require(forFeatureName != null);
        log.debug("getFeature, forkey={}, forFeatureName={}", forKey, forFeatureName);
        Feature lookupFeature = featureConfig.getFeatureFlags().get(forFeatureName);
        if (lookupFeature == null)
        {
            log.debug("forkey={} has no feature {}",forKey, forFeatureName);
            return null;
        }
        if (lookupFeature.getEnabled()=="true")
        {
            log.debug("forkey={} enabled feature for everyone {}",forKey, forFeatureName);
            return lookupFeature;
        }
        else if (lookupFeature.getEnabled()=="false")
        {
            log.debug("forkey={} enable feature for none {}",forKey, forFeatureName);
            return null;
        }
        //else: filtered
        List<String> includedInFeaureList = lookupFeature.getIncluded();
        boolean direct = includedInFeaureList.contains(forKey);
        if (direct)
        {
            log.debug("forkey={} directly has feature {}",forKey, forFeatureName);
            return lookupFeature;
        }
        log.debug("looking up all included keys in keygroups, to see if key={} is included there...");
        
        for (String included : includedInFeaureList)
        {
            log.debug("lookup group for {}", included);
            KeyGroup lookupGroup = featureConfig.getKeyGroups().get(included);
            if (lookupGroup == null)
            {
                log.debug("forkey={} has no feature {}",forKey, forFeatureName);
            }
            else
            {
                boolean inGroup = lookupGroup.getKeys().contains(forKey);
                if (inGroup)
                {
                    log.debug("forkey={} has feature {}",forKey, forFeatureName);
                    return lookupFeature;
                }
            }
        }
        log.debug("forkey={} has no feature {}",forKey, forFeatureName);
        return null;
    }
}

