/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.ricta.featureflag;

import java.util.Map;
import nl.ricta.featureflag.FeatureFlags.Feature;
/**
 * FeatureFlagService service interface
 *
 *
 *
 * @author r.roos
 */

public interface FeatureFlagService 
{
    
    /**
     * All features defined in the yml
     * @return a map of FeatureName/Feature
     */
    public Map<String, Feature> getFeatureFlags() ;
    
    /**
     * Get the feature availability for a key
     * @param forKey the key. e.g "user1"
     * @param forFeatureName The featurename
     * @return The Feature if enabled, null otherwise
     */
    public Feature getFeature(String forKey, String forFeatureName);
    
}

