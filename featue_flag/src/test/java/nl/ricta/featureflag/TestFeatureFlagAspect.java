package nl.ricta.featureflag;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import nl.ricta.featureflag.FeatureFlags.Feature;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author rr
 */
//@ContextConfiguration()
//See https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-profiles
@Profile("featureflags")
@ActiveProfiles("featureflags")
@Configuration
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MocksApplicationFeatures.class)
public class TestFeatureFlagAspect
{
    private static final Logger log = LoggerFactory.getLogger(TestFeatureFlagAspect.class);
    @Autowired
    FeatureAnnotationAspect aspectTest;
    
    @Test
    public void testAnnotationIntersception() 
    {
        log.debug("TestFeatureFlagAspect::testAnnotationIntersception");
       //This wont work! You need to Autowire the object to get all annotations and AOP working!
        //aspectTest = new FeatureAnnotationAspect();
        Model model = new ModelImpl();
        ModelAndView modelAndView = new ModelAndView();
        aspectTest.Annotate("user1", model,modelAndView);
        Assert.isTrue(model.containsAttribute("feature1"));
        Assert.isTrue(model.containsAttribute("feature2"));
        
         Assert.isTrue(modelAndView.getModel().containsKey("feature1"));
        Assert.isTrue(modelAndView.getModel().containsKey("feature2"));
    } 
    
    @Test
    public void testModelAndView() 
    {
        ModelAndView modelandView = new ModelAndView();
        modelandView.addObject("feature1", new Feature());
        Assert.isTrue(modelandView.getModel().containsKey("feature1"));
    }
}
