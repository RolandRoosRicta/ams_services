package nl.ricta.featureflag;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

/**
 *
 * @author rr
 */
//@Configuration()
//See https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-profiles
@Profile("featureflags")
@ActiveProfiles("featureflags")

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MocksApplicationFeatures.class)

//Does not work, yml not supported
//@RunWith(SpringRunner.class)
//@ContextConfiguration(classes = {MocksApplicationFeatures.class})
//@TestPropertySource("classpath:application-featureflags.yml" )
public class ConfigServersTest
{
    @Before
    public void setup() 
    {
       
    }

    @Value("${servers}")
    private String[] array;  

    @Test
    public void testServersListFromConfig() 
    {
        Assert.isTrue(array != null);
        Assert.isTrue(array.length == 2);
    }
    
  
}
