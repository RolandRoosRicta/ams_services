/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.ricta.featureflag;

import java.util.HashMap;
import java.util.Map;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author roosr
 */

@Configuration
@ConfigurationProperties
public class MyConfig {

    private final Map<String, Project> baaa = new HashMap<>();

    public Map<String, Project> getMyConfig() {
        return baaa;
    }

    @Data
    public static class Project {

        public String mantisID;
        public String password;
        public String user;
 
    }
}