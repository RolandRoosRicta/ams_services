/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.ricta.featureflag;

import java.util.Arrays;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author roosr
 */
//@EnableAutoConfiguration
@Configuration
@SpringBootApplication
@ComponentScan()//{"nl.ricta*", "nl.ricta*","nl.ricta.featureflag*","nl.ricta.featureflag.*"})
//@EnableAspectJAutoProxy
public class MocksApplicationFeatures 
{
    public static void main(String[] args) 
    {
        SpringApplication.run(MocksApplicationFeatures.class, args);
    }
    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {

            System.out.println("Let's inspect the beans provided by Spring Boot:");

            String[] beanNames = ctx.getBeanDefinitionNames();
            Arrays.sort(beanNames);
            for (String beanName : beanNames) {
               // System.out.println(beanName);
            }

        };
    }
}
