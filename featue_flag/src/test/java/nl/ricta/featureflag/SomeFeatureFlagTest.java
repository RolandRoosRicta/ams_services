package nl.ricta.featureflag;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import nl.ricta.featureflag.FeatureFlags.Feature;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

/**
 *
 * @author rr
 */
//@ContextConfiguration()
//See https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-profiles
@Profile("featureflags")
@ActiveProfiles("featureflags")

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MocksApplicationFeatures.class)
public class SomeFeatureFlagTest
{
    private static final Logger log = LoggerFactory.getLogger(SomeFeatureFlagTest.class);
//    @Before
//    public void setup() 
//    {
//       
//    }
 
   @Autowired
    private MyConfig  projects;  

    @Test
    public void testProjectConfig() 
    {
        
        Assert.isTrue(projects != null);
        Assert.isTrue(projects.getMyConfig().size() == 2);
    }
    
    @Autowired
    private FeatureFlags  featureConfig;  

    @Test
    public void testFeatureConfig() 
    {
        Assert.isTrue(featureConfig != null);
        Assert.isTrue(featureConfig.getFeatureFlags().size() == 5);
        Feature f1 = featureConfig.getFeatureFlags().get("feature1");
        Assert.isTrue(f1 != null);
        Assert.isTrue(f1.getIncluded().size() ==2);
        log.debug("first incuded = {}", f1.getIncluded().get(0));
        Assert.isTrue(f1.getIncluded().get(0).equals("feature1"));
        Assert.isTrue(f1.getIncluded().get(1).equals("user2"));
    } 
    
    @Autowired
    private FeatureFlagService  featureConfigService;  

    @Test
    public void testFeatureConfigService() 
    {
        Assert.isTrue(featureConfigService != null);
        Feature f = featureConfigService.getFeature("user2", "feature1");
        Assert.isTrue(f !=null);
       
        f = featureConfigService.getFeature("user1", "feature1");
        Assert.isTrue(f !=null);
     } 
   
 

    @Test
    public void testFeatureConfig2() 
    {
        log.debug("ActivityControllerTest::testFeatureConfig2");
        Assert.isTrue(featureConfig != null);
        Assert.isTrue(featureConfig.getFeatureFlags().size() == 5);
         Assert.isTrue(featureConfigService.getFeatureFlags().size() == featureConfig.getFeatureFlags().size());
        
//        FeatureFlags.Feature f1 = featureConfig.getFeatureFlags().get("myfeature1");
//        Assert.isTrue(f1 != null);
//        Assert.isTrue(f1.getIncluded().size() ==2);
//        Assert.isTrue(f1.getIncluded().get(0) == "feature1Group");
//        Assert.isTrue(f1.getIncluded().get(1) == "user2");
        
    } 
    
}
