package nl.ricta.featureflag;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import nl.ricta.featureflag.FeatureFlags.Feature;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author rr
 */
//@ContextConfiguration()
//See https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-profiles
@Profile("featureflags")
@ActiveProfiles("featureflags")
@Configuration
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MocksApplicationFeatures.class)
public class TestFeatureFlagService
{
    private static final Logger log = LoggerFactory.getLogger(TestFeatureFlagService.class);
    @Autowired
    FeatureFlagService service;
    
    @Test
    public void nullTest() 
    {
        log.debug("nullTest");
        Assert.isNull(service.getFeature("", ""));
    } 
    
    @Test
    public void falseTest() 
    {
        Assert.isNull(service.getFeature("user3", "feature3"));
    }
    
    @Test
    public void filteredNoKeyTest() 
    {
        Assert.isNull(service.getFeature("nokey", "feature4"));
    }
    
    @Test
    public void filteredHaskeyTest() 
    {
        Assert.notNull(service.getFeature("feature2And3", "feature4"));
    }
    
    @Test
    public void filteredHasNoGroupTest() 
    {
        Assert.isNull(service.getFeature("user8","feature5"));
    }
    
    @Test
    public void filteredHasGroupTest() 
    {
        Assert.notNull(service.getFeature("user5", "feature5"));
    }
}
