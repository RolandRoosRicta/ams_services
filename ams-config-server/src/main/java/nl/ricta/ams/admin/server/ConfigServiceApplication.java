package nl.ricta.ams.admin.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Scope;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableConfigServer
@SpringBootApplication

//@EnableJpaRepositories("nl.corizon.ygg.admin.server.repository")
//@EntityScan(basePackages = {"nl.corizon.ygg.api", "nl.corizon.ygg.config"})
//@ComponentScan(basePackages = {"nl.corizon.ygg.admin.server.principal.entrypoint", "nl.corizon.ygg.admin.server.service", "nl.corizon.ygg.admin.server", "nl.corizon.ygg.admin.server.config", "nl.corizon.ygg.admin.server.service.filter"})
public class ConfigServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConfigServiceApplication.class, args);
    }
    

}
