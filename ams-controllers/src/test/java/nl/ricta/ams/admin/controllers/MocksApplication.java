/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.ricta.ams.admin.controllers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


/**
 *
 * @author roosr
 */
@SpringBootApplication
@ComponentScan(basePackages = {"nl.ricta.ams.admin.controllers*","nl.ricta.ams.admin.controllers.*", "nl.ricta.featureflag.*","nl.ricta.featureflag*"})
public class MocksApplication {
    public static void main(String[] args) {
        SpringApplication.run(MocksApplication.class, args);
    }
}
