package nl.ricta.ams.admin.controllers;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Arrays;
import java.util.List;
import nl.ricta.ams.admin.services.interfaces.ActivityHistoryService;
import nl.ricta.ams.admin.services.interfaces.ActivityService;
import nl.ricta.ams.api.vo.ActivityDTO;
import nl.ricta.ams.api.vo.ActivityState;
import nl.ricta.featureflag.FeatureFlagService;
import nl.ricta.featureflag.FeatureFlags.Feature;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import org.junit.Before;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
/**
 *
 * @author rr
 */

@ActiveProfiles("test")
//@RunWith(SpringJUnit4ClassRunner.class)
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MocksApplication.class)
@ContextConfiguration(classes = {TestContext.class, WebApplicationContext.class})
@WebAppConfiguration
//@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class ActivityControllerTest 
{
    private static final Logger log = LoggerFactory.getLogger(ActivityControllerTest.class);

    @Value("${ams.admin.sentActivityExpirationMails}")
    private boolean sentActivityExpirationMails;
     
     @Test
    public void autowiredController() 
    {
       log.debug("ActivityControllerTest::autowiredController");
       Assert.isTrue(sentActivityExpirationMails);
  
    }

    @TestConfiguration
    // @Import(FeatureAspect.class)
     static class SomeServiceImplTestContextConfiguration 
     { 
         @Bean
        public CommandLineRunner commandLineRunner(ApplicationContext ctx) 
        {
            return args -> 
            {
                log.debug("Let's inspect the beans provided by Spring Boot:");

                List<String> beanNames = Arrays.asList(ctx.getBeanDefinitionNames());
                Assert.isTrue( beanNames.contains("featureAspect"));

            };
        }
         @Bean
        @Primary
        public ActivityService activityService() 
        {
            ActivityService mockedActivityService = Mockito.mock(ActivityService.class);
            Long id = 1L;
            ActivityDTO act  = ActivityDTO.builder()
                .dateOfCreation("1-1-2019")
                .description("desc")
                .dateOfExpiration("2-1-2019")
                .id(id)
                .prio("prio")
                .requestorName("rr")
                .type("type")
                .status(ActivityState.Open.toString())
                .title("title")
                .build();
                   
            
            Mockito.when(mockedActivityService.findOneByID(id)).thenReturn(act);
            return mockedActivityService;
        }
         @Bean
        @Primary
        public ActivityHistoryService activityHistoryService() 
        {
            ActivityHistoryService mockedActivityService = Mockito.mock(ActivityHistoryService.class);
            
            return mockedActivityService;
        }
        
//         @Bean
//        @Primary
//        public FeatureFlagService activityFeatureFlagService() 
//        {
//            FeatureFlagService activityFeatureFlagService = Mockito.mock(FeatureFlagService.class);
//            Mockito.when(activityFeatureFlagService.getFeature("user1", "featureShowDatePicker")).thenReturn(new Feature());
//            Mockito.when(activityFeatureFlagService.getFeature("user1", "featureSubmitActivityChanges")).thenReturn(null);
//            return activityFeatureFlagService;
//        }
        
        
    }
   
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webAppContext;

    @Autowired
    ApplicationContext context;
     
    
    @MockBean//->Requires @RunWith(SpringRunner.class)!!!  Dont use any other runner class. 
     //@Autowired-> use static @Primary @Bean above with this
    FeatureFlagService activityFeatureFlagService;
            
    @Before
    public void setUp() throws Exception 
    {
        mockMvc = MockMvcBuilders.webAppContextSetup(webAppContext).build();
        Mockito.when(activityFeatureFlagService.getFeature("user1", "featureShowDatePicker")).thenReturn(new Feature());
        Mockito.when(activityFeatureFlagService.getFeature("user1", "featureSubmitActivityChanges")).thenReturn(null);
    }

    @Test
    public void testFindOneById()
    {
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/api/activities/findonebyid/1");
        try
        {
           ResultActions actions =  mockMvc.perform(builder);

           actions.andExpect(status().isOk());
        } 
        catch(Exception ex)
        {
           Assert.isTrue(false, ex.getMessage());
        }
    }
    
    @Test
    public void testActivityControllerModelAndView()
    {
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/api/activities/showActivityEditForm?id=1&user=user1");
        //Mockito.when(activityFeatureFlagService.getFeature("user1", "featureShowDatePicker")).thenReturn(new Feature());
       // Mockito.when(activityFeatureFlagService.getFeature("user1", "featureSubmitActivityChanges")).thenReturn(null);
        try
        {
           ResultActions actions =  mockMvc.perform(builder);

           actions.andExpect(status().isOk());
           actions.andExpect(model().attributeExists("activity"));
           actions.andExpect(model().attribute("featureShowDatePicker", notNullValue()));
           actions.andExpect(model().attribute("featureSubmitActivityChanges", nullValue()));
           actions.andExpect(view().name("admin/activityeditForm1"));
        } 
        catch(Exception ex)
        {
           Assert.isTrue(false, ex.getMessage());
        }
    }
    
   // @Test
    public void findAll()
    {
          MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/api/activities/");

         try
         {
            ResultActions actions =  mockMvc.perform(builder);

            actions
                    .andExpect(status().isOk())
                    .andExpect(model().attributeExists("todos"))
                    .andExpect(model().attribute("todos", "todos"))
                    .andExpect(view().name("admin/emptyForm"));
         }
         catch(Exception ex)
         {
            Assert.isTrue(false, ex.getMessage());
         }
    }
        
   
    
}


