/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.ricta.ams.admin.controllers;

import java.util.List;
import nl.ricta.ams.admin.services.interfaces.ActivityHistoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import nl.ricta.ams.api.vo.ActivityHistoryDTO;
/**
 *
 * @author R.roos
 */
@RestController
@RequestMapping(value = "/api/activityhistories",
        produces = {"application/json"}
)
public class ActivityHistoryController 
{
    private static final Logger log = LoggerFactory.getLogger(ActivityHistoryController.class);
    
  
    /*
        As IActivityService is implemented by ActivityServiceImpl, this instance will be wired automagically
    */
    @Autowired
    private ActivityHistoryService activityHistoryService;
    

    @CrossOrigin
    @GetMapping(value = "findall")
    public List<ActivityHistoryDTO> findAll()
    {
         List<ActivityHistoryDTO> activities = activityHistoryService.findAll();
         return activities;
    }

}
