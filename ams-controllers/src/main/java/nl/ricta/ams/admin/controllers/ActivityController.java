/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.ricta.ams.admin.controllers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;
import nl.ricta.ams.admin.services.interfaces.ActivityHistoryService;
import nl.ricta.ams.api.vo.ActivityDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import nl.ricta.ams.api.vo.ActivityState;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import nl.ricta.ams.admin.services.interfaces.ActivityService;
import nl.ricta.featureflag.FeatureFlagKey;
import nl.ricta.featureflag.FeatureFlagService;
import nl.ricta.featureflag.FeatureFlags.Feature;

/**
 *
 * @author R.roos []
 */
//@Configuration
@RestController
@RequestMapping(value = "/api/activities",
        produces = {"application/json"}
)
public class ActivityController {

    private static final Logger log = LoggerFactory.getLogger(ActivityController.class);
    public static DateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
     private static final String ADMIN_AVTIVITY_EDIT_VIEW = "admin/activityEditForm1";
    /*
        As IActivityService is implemented by ActivityServiceImpl, this instance will be wired automagically
    */
    @Autowired
    private ActivityService activityService;
    
    @Autowired
    private ActivityHistoryService activityHistoryService;
    
    @Autowired
    private FeatureFlagService  featureConfigService;
      
    
    //Testing code for testing controllers with emty code
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView findAll(Model model, ModelAndView viewModel) {
       
        log.debug("ActivityController::findAll");
        ModelAndView modelAndView  = new ModelAndView("admin/emptyForm");
        modelAndView.addObject("todos", "todos");
        return modelAndView;
    }
    @FeatureFlagKey(features = {"featureSubmitActivityChanges","featureShowDatePicker"}, paramNameForKeyOfFeature = "user")
    @RequestMapping("/showActivityEditForm")
    public ModelAndView showForm(
            @RequestParam(value = "id", required = false) Optional<Long> id, 
            @RequestParam(value = "user", required = false) String user,
             Model theModel,
             ModelAndView modelAndView)
    { 
        //Model and ModelAndView is used to pass data between 
        //controllers and views
        
        Feature featureShowDatePicker = featureConfigService.getFeature(user, "featureShowDatePicker");
        Feature featureSubmitActivityChanges = featureConfigService.getFeature(user, "featureSubmitActivityChanges");
        
        ActivityDTO dto  = getActivityDTO(id.isPresent()? id.get() : null);
        theModel.addAttribute("activity", dto); //attribute name, value
        
        //and/or
        //ModelAndView modelAndView  = new ModelAndView("admin/activityeditForm1");
        modelAndView.setViewName("admin/activityeditForm1");
        
         modelAndView.addObject("activity",dto); 
         
         String submitStyle = (featureSubmitActivityChanges == null? "display:none;" : "display:blok;");
         modelAndView.addObject("submitstyle", submitStyle);
        // modelAndView.addObject("featureShowDatePicker", featureShowDatePicker == null ? false: true);
        // modelAndView.addObject("featureSubmitActivityChanges", featureSubmitActivityChanges == null ? false: true);
         
        return modelAndView;
    }

    @CrossOrigin
    @PostMapping(value = "activitychangedsubmit")
    public ModelAndView activityChangedSubmit(
            @ModelAttribute ActivityDTO changedActivity) 
    {
        log.debug("activitychangedsubmit");
        activityChanged(changedActivity);
        
        ModelAndView view = new ModelAndView("admin/activityeditForm1");
        view.addObject("activity", changedActivity);
       return view;
    }
    
    @RequestMapping("activity/{id}")
    public ActivityDTO getActivityDTO(@PathVariable("id")  Long id) 
    {
        return  ActivityDTO.
                         builder()
                         .id(id)
                         .title("Demo geven")
                         .dateOfCreation("1-1-2019")
                         .dateOfExpiration("1-1-2019")
                         .description("Demo aan Bebr geven einde sprint")
                         .prio("highest")
                         .status(ActivityState.Closed.toString())
                         .requestorName("roland roos")
                         .build();
    }

//    @CrossOrigin
//    @RequestMapping(value = "/formedit")
//    public ActivityDTO/*ModelAndView*/ formAddNewApiKey()
//    {
//        log.debug("Edit Activity Form");
//       ActivityDTO someDto= new ActivityDTO();
//            someDto.setTitle("boe");
//            return someDto;
// 
//    }
//      @CrossOrigin
//    @GetMapping(value = "/formeditview")
//    public ModelAndView formEditView()
//    {
//        log.debug("Edit Activity Form");
//
//       // ActivityDTO activityToEdit = (ActivityDTO)activityService.findAll().toArray()[0];
//
//        ModelAndView mav = new ModelAndView("admin/api-keys/edit");
//      
//        mav.addObject("mode", 1L);
//        // mav.addObject("activityToEdit", activityToEdit);
//       // mav.setViewName("formedit");
//
//        return mav;
//    }
    
      @CrossOrigin
    @RequestMapping(value = "/activityedit", method = RequestMethod.GET)
    public ModelAndView activityEditForm() 
    {
        log.debug("Feeds method called..");
         ModelAndView view  = new ModelAndView();
         view.addObject("activity", 
                 getActivityDTO(1L));
         view.setViewName(ADMIN_AVTIVITY_EDIT_VIEW);
        return view;
    }
    
    @CrossOrigin
    @GetMapping(value = "/activity/{source}/{id}")
    public ActivityDTO test(@PathVariable("id") long id,@PathVariable("source") String source) {
      //  accommodationService.handleSourceUpdateAvailableEvent(new SourceUpdateAvailableEvent(Source.valueOf(source.toUpperCase()), id));
      ActivityDTO someDto= new ActivityDTO();
            someDto.setTitle(source+id);
        return someDto;
    }
    
    
    
//    @GetMapping(value = "/{id}")
//    public AccommodationDTO findById(@PathVariable("id") long id) {
//        log.debug("findById for id {}", id);
//        
////        AccommodationDTO result =  accommodationService.findById(id).orElseThrow(() -> {
////            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Accommodation Not Found");
////        });
//        
//        return null;
//    }
 

    
    @CrossOrigin
    @PostMapping(value = "activitychanged")
    public long activityChanged(
            ActivityDTO changedActivity) 
    {
        long newActId = activityService.updateActivity(changedActivity);
        changedActivity.setId(newActId);
        activityHistoryService.AddActivityVersion(changedActivity);

        return newActId;
    }
   
    @CrossOrigin
    @GetMapping(value = "findall/{numberof}")
    public List<ActivityDTO> findAll(
            @PathVariable("numberof") long numberOf) 
    {
         List<ActivityDTO> activities = activityService.findAll();
         return activities;
 
        //this.modelMapper = modelMapper;
       
       /* ArrayList<ActivityDTO> list = new ArrayList<ActivityDTO>();
        for (long j=0;j<numberOf;j++)
        {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.SECOND, calendar.get(Calendar.SECOND) + (int)j);
            Date dateCtreated = calendar.getTime();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            //dateRequested.setSeconds(dateRequested.getSeconds()+ (int) j);
            ActivityDTO someDto =  ActivityDTO
                    .builder()
                    .dateOfCreation(format.format(dateCtreated))
                    .dateOfExpiration((j % 2 == 0) ? format.format(new GregorianCalendar(2019, 11,11).getTime()) : format.format(new GregorianCalendar(2019,1,1).getTime()))
                    .description((j % 2 ==0) ? "desc " + j : "omschrijving a")
                    .title((j % 2 ==0) ? "zon aanvraag " + j : "warmtepomp aanvraag a" + j)
                    .id(j)
                    .requestorName((j % 2 == 0) ? "r.roos" : "a.bruijn")
                    .status((j % 3 == 0) ? ActivityState.Closed : ActivityState.Open)
                    .type((j % 3 == 0) ? ActivityType.SunRequest.toString() : ActivityType.HeathRequest.toString())
                   .build();
//            ProjectRequestDTO someDto= new ProjectRequestDTO();
//            someDto.title=sourceName+j;
//            someDto.id = Long.toString(j);
//            someDto
            list.add(someDto);
        }
        return list;*/
    }
//        log.debug("findByExternalId for source {} and id {}", sourceName, externalId);
////        try {
////            Source source = Source.valueOf(sourceName.toUpperCase());
////            List<AccommodationDTO> accommodations = accommodationService.findByExternalId(source, externalId);
////            if (accommodations.isEmpty()){
////                throw new ResponseStatusException(HttpStatus.NOT_FOUND,"No accommodation found for source "+sourceName+" and id "+externalId);
////            }
////            return accommodations;
////        } catch (IllegalArgumentException e) {
////            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Accommodaton source " + sourceName + " unknown");
////        }
//        return null;
//    }

//    @GetMapping(value = "/")
//    public List<AccommodationDTO> findByIds(@RequestParam("ids") List<Long> idList) {
//        log.debug("findByIds");
//        return null;//accommodationService.findByIds(idList);
//    }
//
//    @GetMapping(value = "/updated")
//    public List<AccommodationDTO> findUpdatedAccommodations(
//            @RequestParam(required = true) @DateTimeFormat(pattern = "dd-MM-yyyy") Date since,
//            @RequestParam(required = false) @DateTimeFormat(pattern = "dd-MM-yyyy") Date until,
//            @RequestParam(required = false) Integer size,
//            @RequestParam(required = false) Integer page
//    ) {
//        log.info("find updated accommodations since {} until {} size {} page {}",since,until,size,page);
//        return null;//accommodationService.findUpdatedAccommodations(since, until, size, page);
//    }
    @CrossOrigin
    @GetMapping(value = "findonebyid/{id}")
    public ActivityDTO findOneByID(
            @PathVariable("id") long Id) 
    {
         ActivityDTO activities =   activityService.findOneByID(Id);
         return activities;
    }
    
//    @Autowired
//    StateActivityRepository stateActivityRepository;
//    
//    @CrossOrigin
//    @GetMapping(value = "numofstates")
//    public long numOfStates( ) 
//    {
//         Iterable<ProcessStateActivityEntity> activities =   stateActivityRepository.findAll();
//       //  Iterable<ProcessStateActivityEntity> activitiesLike =   stateActivityRepository.findByPlaceContaining("%\"qty\": 24%");
//          Iterable<ProcessStateActivityEntity> activitiesLike2 =   stateActivityRepository.getActsWithJsonContaining("%24%");
//         return 10;
//    }
}
