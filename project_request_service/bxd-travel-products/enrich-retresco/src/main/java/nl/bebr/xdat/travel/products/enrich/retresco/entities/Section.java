/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.retresco.entities;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import java.util.List;
import lombok.Builder;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 *
 * @author Kilian Veenstra [kilian@corizon.nl]
 */
@JacksonXmlRootElement(localName = "section")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Section {

    @JacksonXmlProperty(isAttribute = true)
    /**
     * Functionally this is the id of the Section.
     */
    Integer type;

    @JacksonXmlProperty(isAttribute = true)
    /**
     * Name of the group of facts. E.g. facilities for the group that has facts
     * about all kind of facilities that the accommodation provides.
     */
    String name;
    @JacksonXmlElementWrapper(localName = "facts")
    @JacksonXmlProperty(localName = "fact")
    List<Fact> facts;
}
