/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.retresco.entities;

/**
 *
 * @author Kilian Veenstra [kilian@corizon.nl]
 */
public enum FactInfo {

    FAX("1", "fax", "string"),
    PHONE_HOTEL("2", "phone_hotel", "string"),
    CATEGORY_OFFICIAL("3", "category_official", "float"),
    CATEGORY_RECOMMENDED("4", "category_recommended", "float"),
    NUM_ROOMS_TOTAL("5", "num_rooms_total", "int"),
    FACILITY_BARS("6", "facility_bars", "numeric-bool"),
    FACILITY_RESTAURANTS("7", "facility_restaurants", "int"),
    FACILITY_INTERNET("8", "facility_internet", "bool"),
    FACILITY_WLAN("9", "facility_wlan", "bool"),
    FACILITY_ROOMSERVICE("10", "facility_roomservice", "bool"),
    FACILITY_CARPARK("11", "facility_carpark", "bool"),
    ROOM_BATH("12", "room_bath", "bool"),
    ROOM_SHOWER("13", "room_shower", "bool"),
    ROOM_INTERNET("14", "room_internet", "bool"),
    ROOM_AIRCON("15", "room_aircon", "bool"),
    ROOM_TV("16", "room_tv", "bool"),
    ROOM_DOUBLEBED("17", "room_doublebed", "bool"),
    FACILITY_LIFTS("18", "facility_lifts", "int"),
    FACILITY_CONFERENCEROOM("19", "facility_conferenceroom", "int"),
    ROOM_HAIRDRYER("20", "room_hairdryer", "bool"),
    ROOM_BALCONY("21", "room_balcony", "bool"),
    ROOM_TEA_COFFEE("22", "room_tea_coffee", "bool"),
    ROOM_KITCHENETTE("23", "room_kitchenette", "bool"),
    ROOM_FRIDGE("24", "room_fridge", "bool"),
    ROOM_WASHINGMACHINE("25", "room_washingmachine", "bool"),
    ROOM_SAFE("26", "room_safe", "bool"),
    ROOM_MINIBAR("27", "room_minibar", "bool"),
    SPORTS_ENTERTAINMENT_KIDS("28", "sports_entertainment_kids", "numeric-bool"),
    ROOM_LOUNGE("29", "room_lounge", "bool"),
    ROOM_MICROWAVE("30", "room_microwave", "bool"),
    SPORTS_MASSAGE("31", "sports_massage", "numeric-bool"),
    MEALS_BREAKFASTSERVED("32", "meals_breakfastserved", "bool"),
    YEAR_CONSTRUCTION("33", "year_construction", "int"),
    NUM_FLOORS_MAIN("34", "num_floors_main", "int"),
    NUM_ROOMS_SINGLE("35", "num_rooms_single", "int"),
    NUM_ROOMS_DOUBLE("36", "num_rooms_double", "int"),
    NUM_SUITES("37", "num_suites", "int"),
    NUM_VILLAS("38", "num_villas", "int"),
    HOTELTYPE_CLUBRESORT("39", "hoteltype_clubresort", "bool"),
    HOTELTYPE_SPACOMPLEX("40", "hoteltype_spacomplex", "bool"),
    HOTELTYPE_CONFERENCEHOTEL("41", "hoteltype_conferencehotel", "bool"),
    HOTELTYPE_HOTELDECHARME("42", "hoteltype_hoteldecharme", "bool"),
    HOTELTYPE_FAMILY("43", "hoteltype_family", "bool"),
    PAYMENT_VISA("44", "payment_visa", "bool"),
    PAYMENT_MASTER("45", "payment_master", "bool"),
    PAYMENT_EC("46", "payment_ec", "bool"),
    FACILITY_AIRCON("47", "facility_aircon", "bool"),
    FACILITY_RECEPTION24("48", "facility_reception24", "bool"),
    FACILITY_SAFE("49", "facility_safe", "int"),
    FACILITY_LAUNDRYSERVICE("50", "facility_laundryservice", "bool"),
    FACILITY_PLAYGROUND("51", "facility_playground", "bool"),
    ROOM_BATHTUB("52", "room_bathtub", "bool"),
    ROOM_SATCABLETV("53", "room_satcabletv", "bool"),
    ROOM_CENTRALHEATING("54", "room_centralheating", "bool"),
    ROOM_AIRCON_INDIV("55", "room_aircon_indiv", "bool"),
    MEALS_HALFBOARD("56", "meals_halfboard", "bool"),
    MEALS_FULLBOARD("57", "meals_fullboard", "bool"),
    MEALS_BREAKFASTBUFFET("58", "meals_breakfastbuffet", "bool"),
    MEALS_DINNERCARTE("59", "meals_dinnercarte", "bool"),
    MEALS_ALLINCLUSIVE("60", "meals_allinclusive", "bool"),
    MEALS_SPECIALOFFERS("61", "meals_specialoffers", "bool"),
    SPORTS_POOL_INDOOR("62", "sports_pool_indoor", "int"),
    SPORTS_POOLOUTDOOR("63", "sports_pooloutdoor", "int"),
    SPORTS_SUNLOUNGERS("64", "sports_sunloungers", "numeric-bool"),
    SPORTS_PARASOLS("65", "sports_parasols", "numeric-bool"),
    SPORTS_JACUZZI("66", "sports_jacuzzi", "numeric-bool"),
    SPORTS_SAUNA("67", "sports_sauna", "int"),
    SPORTS_STEAMBATH("68", "sports_steambath", "numeric-bool"),
    SPORTS_CANOE("69", "sports_canoe", "numeric-bool"),
    SPORTS_GYM("70", "sports_gym", "numeric-bool"),
    SPORTS_MTB("71", "sports_mtb", "numeric-bool"),
    SPORTS_NUMBEROFPOOLS("72", "sports_numberofpools", "int"),
    DIST_CITY_CENTRE("73", "dist_city_centre", "int"),
    ROOM_DIRECTDIALTEL("74", "room_directdialtel", "bool"),
    FACILITY_CAFE("75", "facility_cafe", "numeric-bool"),
    HOTELTYPE_GUESTHOUSE("76", "hoteltype_guesthouse", "bool"),
    YEAR_RENOVATION("77", "year_renovation", "int"),
    NUM_FLOORS_ANNEXE("78", "num_floors_annexe", "string"),
    M2_GARDEN("79", "m2_garden", "int"),
    HOTELTYPE_GOLFHOTEL("80", "hoteltype_golfhotel", "bool"),
    PAYMENT_AMEX("81", "payment_amex", "bool"),
    PAYMENT_DINERS("82", "payment_diners", "bool"),
    FACILITY_SHOPS("83", "facility_shops", "numeric-bool"),
    ROOM_CARPETED("84", "room_carpeted", "bool"),
    SPORTS_TABLETENNIS("85", "sports_tabletennis", "numeric-bool"),
    SPORTS_GOLF("86", "sports_golf", "numeric-bool"),
    SPORTS_TENNIS("87", "sports_tennis", "numeric-bool"),
    DIST_TOURIST_CENTRE("88", "dist_tourist_centre", "int"),
    M2_TERRACE("89", "m2_terrace", "int"),
    HOTELTYPE_CITYHOTEL("90", "hoteltype_cityhotel", "bool"),
    HOTELTYPE_HISTORIC("91", "hoteltype_historic", "bool"),
    FACILITY_RESTAURANTS_NOSMOKINGAREA("92", "facility_restaurants_nosmokingarea", "int"),
    FACILITY_MEDICALSERVICE("93", "facility_medicalservice", "bool"),
    FACILITY_BICYCLEHIRE("94", "facility_bicyclehire", "bool"),
    FACILITY_TVROOM("95", "facility_tvroom", "numeric-bool"),
    FACILITY_RESTAURANT_SMOKINGAREA("96", "facility_restaurant_smokingarea", "numeric-bool"),
    ROOM_INDIVIDUALHEATING("97", "room_individualheating", "bool"),
    DIST_BEACH("98", "dist_beach", "int"),
    ROOM_RADIO("99", "room_radio", "bool"),
    MEALS_BREAKFASTCONTINENTAL("100", "meals_breakfastcontinental", "bool"),
    DIST_PARK("101", "dist_park", "int"),
    DIST_SHOPPING("102", "dist_shopping", "int"),
    DIST_TRAIN_STATION("103", "dist_train_station", "int"),
    LOCATED_ON_MAIN_ROAD("104", "located_on_main_road", "bool"),
    NUM_APARTMENTS("105", "num_apartments", "int"),
    NUM_STUDIOS("106", "num_studios", "int"),
    HOTELTYPE_APARTMENTHOTEL("107", "hoteltype_apartmenthotel", "bool"),
    PAYMENT_JCB("108", "payment_jcb", "bool"),
    FACILITY_CHECKIN24("109", "facility_checkin24", "bool"),
    FACILITY_SUPERMARKET("110", "facility_supermarket", "numeric-bool"),
    FACILITY_WASHING("111", "facility_washing", "bool"),
    ROOM_WHEELCHAIR("112", "room_wheelchair", "bool"),
    BEACH_SANDY("113", "beach_sandy", "bool"),
    FACILITY_MONEYEXCHANGE("114", "facility_moneyexchange", "numeric-bool"),
    FACILITY_CASINO("115", "facility_casino", "numeric-bool"),
    SPORTS_POOLCHILDRENS("116", "sports_poolchildrens", "int"),
    SPORTS_SUNBATHING_TERRACE("117", "sports_sunbathing_terrace", "numeric-bool"),
    SPORTS_SURFING("118", "sports_surfing", "numeric-bool"),
    SPORTS_SAILING("119", "sports_sailing", "numeric-bool"),
    SPORTS_BILLIARDS("120", "sports_billiards", "numeric-bool"),
    ROOM_OVEN("121", "room_oven", "bool"),
    ANNEXE_BUILDINGS("122", "annexe_buildings", "int"),
    FACILITY_CLOAKROOM("123", "facility_cloakroom", "numeric-bool"),
    FACILITY_HAIRDRESSER("124", "facility_hairdresser", "numeric-bool"),
    FACILITY_PUB("125", "facility_pub", "numeric-bool"),
    FACILITY_DISCO("126", "facility_disco", "numeric-bool"),
    FACILITY_THEATRE("127", "facility_theatre", "numeric-bool"),
    FACILITY_RESTAURANTS_HIGHCHAIR("128", "facility_restaurants_highchair", "numeric-bool"),
    FACILITY_BICYCLECELLAR("129", "facility_bicyclecellar", "bool"),
    DIST_RIVER("130", "dist_river", "int"),
    DIST_BARS_PUBS("131", "dist_bars_pubs", "int"),
    DIST_NIGHTCLUBS("132", "dist_nightclubs", "int"),
    DIST_PUBLIC_TRANSPORT("133", "dist_public_transport", "int"),
    FACILITY_MINICLUB("134", "facility_miniclub", "bool"),
    HOTELTYPE_YOUTHHOSTEL("135", "hoteltype_youthhostel", "bool"),
    FACILITY_KIOSK("136", "facility_kiosk", "numeric-bool"),
    SPORTS_DARTS("137", "sports_darts", "numeric-bool"),
    DIST_STATION("138", "dist_station", "int"),
    HOTELTYPE_RURALHOUSE("139", "hoteltype_ruralhouse", "bool"),
    ROOM_KINGSIZEDBEDS("140", "room_kingsizedbeds", "bool"),
    MEALS_SPECIALDIET("141", "meals_specialdiet", "bool"),
    DIST_GOLF_COURSE("142", "dist_golf_course", "int"),
    HOTELTYPE_BUSINESS("143", "hoteltype_business", "bool"),
    HOTELTYPE_AIRPORTHOTEL("144", "hoteltype_airporthotel", "bool"),
    FACILITY_FOYER("145", "facility_foyer", "numeric-bool"),
    FACILITY_GAMESROOM("146", "facility_gamesroom", "numeric-bool"),
    FACILITY_RESTAURANTS_AIRCON("147", "facility_restaurants_aircon", "numeric-bool"),
    ROOM_HIFI("148", "room_hifi", "bool"),
    MEALS_LUNCHCARTE("149", "meals_lunchcarte", "bool"),
    MEALS_LUNCHCHOICE("150", "meals_lunchchoice", "bool"),
    SPORTS_POOLFRESHWATER("151", "sports_poolfreshwater", "numeric-bool"),
    SPORTS_SQUASH("152", "sports_squash", "numeric-bool"),
    SPORTS_AEROBICS("153", "sports_aerobics", "numeric-bool"),
    SPORTS_BASKETBALL("154", "sports_basketball", "numeric-bool"),
    SPORTS_GYMNASTICS("155", "sports_gymnastics", "numeric-bool"),
    HOTELTYPE_VILLAGE("156", "hoteltype_village", "bool"),
    SPORTS_POOLBAR("157", "sports_poolbar", "numeric-bool"),
    ROOM_FINALCLEANING("158", "room_finalcleaning", "bool"),
    MEALS_DINNERCHOICE("159", "meals_dinnerchoice", "bool"),
    SPORTS_MINIGOLF("160", "sports_minigolf", "numeric-bool"),
    SPORTS_ENTERTAINMENT("161", "sports_entertainment", "numeric-bool"),
    SPORTS_BADMINTON("162", "sports_badminton", "numeric-bool"),
    SPORTS_POOLHEATED("163", "sports_poolheated", "numeric-bool"),
    SPORTS_TANNING_STUDIO("164", "sports_tanning_studio", "numeric-bool"),
    DIST_FOREST("165", "dist_forest", "int"),
    NUM_JUNIOR_SUITES("166", "num_junior_suites", "int"),
    HOTELTYPE_ECOHOTEL("167", "hoteltype_ecohotel", "bool"),
    FACILITY_GARAGE("168", "facility_garage", "bool"),
    DIST_RESTAURANTS("169", "dist_restaurants", "int"),
    BEACH_DIRECTLY("170", "beach_directly", "bool"),
    SPORTS_WATERAEROBICS("171", "sports_wateraerobics", "numeric-bool"),
    HOTELTYPE_BEACHHOTEL("172", "hoteltype_beachhotel", "bool"),
    BEACH_PEBBLES("173", "beach_pebbles", "bool"),
    BEACH_ROCKY("174", "beach_rocky", "bool"),
    SPORTS_SCUBA("175", "sports_scuba", "numeric-bool"),
    SPORTS_WINDSURFING("176", "sports_windsurfing", "numeric-bool"),
    SPORTS_CATAMARAN("177", "sports_catamaran", "numeric-bool"),
    ROOM_BIDET("178", "room_bidet", "bool"),
    DIST_BUS_STATION("179", "dist_bus_station", "int"),
    DIST_SEA("180", "dist_sea", "int"),
    DIST_LAKE("181", "dist_lake", "int"),
    EMAIL("182", "email", "string"),
    HOTELTYPE_SKIHOTEL("183", "hoteltype_skihotel", "bool"),
    DIST_SKI_AREA("184", "dist_ski_area", "int"),
    DIST_CROSS_COUNTRY_SKIING("185", "dist_cross_country_skiing", "int"),
    SPORTS_BANANABOAT("186", "sports_bananaboat", "numeric-bool"),
    SPORTS_WATERSKIING("187", "sports_waterskiing", "numeric-bool"),
    SPORTS_JETSKI("188", "sports_jetski", "numeric-bool"),
    SPORTS_PEDALO("189", "sports_pedalo", "numeric-bool"),
    BEACH_SUNLOUNGERS("190", "beach_sunloungers", "bool"),
    BEACH_PARASOLS("191", "beach_parasols", "bool"),
    MEALS_LUNCHBUFFET("192", "meals_lunchbuffet", "bool"),
    MEALS_DINNERBUFFET("193", "meals_dinnerbuffet", "bool"),
    SPORTS_BEACHVOLLEYBALL("194", "sports_beachvolleyball", "numeric-bool"),
    SPORTS_HORSERIDING("195", "sports_horseriding", "numeric-bool"),
    SPORTS_MOTORBOAT("196", "sports_motorboat", "numeric-bool"),
    SPORTS_BOWLING("197", "sports_bowling", "numeric-bool"),
    MEALS_DRINKSINCLUDED("198", "meals_drinksincluded", "bool"),
    ROOM_TILED("199", "room_tiled", "bool"),
    SPORTS_BOCCIA("200", "sports_boccia", "numeric-bool"),
    SPORTS_POOLSALTWATER("201", "sports_poolsaltwater", "numeric-bool"),
    HOTELTYPE_MOUNTAINHOTEL("202", "hoteltype_mountainhotel", "bool"),
    BEACH_SEPARATEDBYROAD("203", "beach_separatedbyroad", "bool"),
    HOTELTYPE_FINCA("204", "hoteltype_finca", "bool"),
    HOTELTYPE_BUNGALOWCOMPLEX("205", "hoteltype_bungalowcomplex", "bool"),
    SPORTS_ARCHERY("206", "sports_archery", "numeric-bool"),
    HOTEL_CHAIN("207", "hotel_chain", "string"),
    HOTELTYPE_CASINORESORT("208", "hoteltype_casinoresort", "bool"),
    DIST_SKI_LIFT("209", "dist_ski_lift", "int"),
    NUM_BUNGALOWS("210", "num_bungalows", "int"),
    HOTELTYPE_MOUNTAINHUT("211", "hoteltype_mountainhut", "bool");

    private final String id;
    private final String name;
    private final String typeHint;

    FactInfo(String id, String name, String typeHint) {
        this.id = id;
        this.name = name;
        this.typeHint = typeHint;
    }

    /**
     * Returns the id of the fact.
     *
     * @return fact id.
     */
    public String getId() {
        return id;
    }

    /**
     * Returns the name of the fact.
     *
     * @return fact name.
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the type of the value that is given to the object value of the
     * fact.
     *
     * @return type of value.
     */
    public String getTypeHint() {
        return typeHint;
    }
}