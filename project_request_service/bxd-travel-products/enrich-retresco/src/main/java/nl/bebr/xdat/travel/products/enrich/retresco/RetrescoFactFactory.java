/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.retresco;

import java.lang.reflect.Field;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import nl.bebr.xdat.travel.products.api.AccommodationDTO;
import nl.bebr.xdat.travel.products.api.AccommodationPropSurroundingDTO;
import nl.bebr.xdat.travel.products.enrich.retresco.entities.Fact;
import nl.bebr.xdat.travel.products.enrich.retresco.entities.FactInfo;
import nl.bebr.xdat.travel.products.enrich.retresco.entities.Factsheet;
import nl.bebr.xdat.travel.products.enrich.retresco.entities.RetrescoItem;
import nl.bebr.xdat.travel.products.enrich.retresco.entities.RetrescoResponse;
import nl.bebr.xdat.travel.products.enrich.retresco.entities.Section;
import static nl.bebr.xdat.travel.products.enrich.retresco.entities.SectionInfo.OBJECT_INFORMATION;
import org.modelmapper.ModelMapper;
import static org.modelmapper.convention.MatchingStrategies.LOOSE;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Kilian Veenstra <ktveenstra@bebr.nl>
 */
@Component
public class RetrescoFactFactory {

    private static final Logger log = LoggerFactory.getLogger(RetrescoFactFactory.class);

    private final ModelMapper modelMapper;

    @Autowired(required = true)
    public RetrescoFactFactory(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
        this.modelMapper.getConfiguration().setAmbiguityIgnored(true);
        this.modelMapper.getConfiguration().setMatchingStrategy(LOOSE);

        this.modelMapper.createTypeMap(AccommodationDTO.class, FlatFactsheet.class).addMappings(mapper -> {
            mapper.map(AccommodationDTO::getCountryCode, FlatFactsheet::setCountry);
            mapper.map(AccommodationDTO::getLocationName, FlatFactsheet::setCity);
        });

        this.modelMapper.createTypeMap(AccommodationPropSurroundingDTO.class, FlatFactsheet.class).addMappings(mapper -> {
            mapper.map(AccommodationPropSurroundingDTO::getDistLake, FlatFactsheet::setDistLake);
        });
    }

    public FlatFactsheet dtoToFactsheet(AccommodationDTO accommodationDTO) {
        return modelMapper.map(accommodationDTO, FlatFactsheet.class);
    }

    public RetrescoResponse formatAccommodation(AccommodationDTO accommodationDTO) {

        FlatFactsheet retrescoFactsheet = dtoToFactsheet(accommodationDTO);

        //
        // Giata variables
        //
        List<Fact> giataFacts = createFacts(retrescoFactsheet);
        List<Section> sections = new ArrayList<>();
        Section objectInformation = new Section(OBJECT_INFORMATION.getType(), OBJECT_INFORMATION.getName(), giataFacts);
        sections.add(objectInformation);

        //
        // Make retrescoItem, basically holder for factsheet
        //
        RetrescoItem retrescoItem = RetrescoItem
                .builder()
                .giataId(accommodationDTO.getGiataId())
                .name(accommodationDTO.getName())
                .city(accommodationDTO.getLocationName())
                .country(accommodationDTO.getCountryCode())
                .factsheet(Factsheet.builder()
                        .lastUpdate(DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(ZonedDateTime.now()))
                        .sections(sections)
                        .build())
                .build();

        //
        // Return DTO containing the retrescoItem
        //
        return new RetrescoResponse(retrescoItem);
    }

    List<Fact> createFacts(FlatFactsheet factsheet) {

        List<Fact> factsList
                = Arrays.asList(FactInfo.values())
                        .stream()
                        .map(fi -> {
                            Fact fact = null;

                            try {
                                //
                                // Retrieve the appropriate property
                                //
                                Field field = FlatFactsheet.class.getDeclaredField(snakeToCamel(fi.getName()));

                                if (field != null) {

                                    field.setAccessible(true);

                                    //
                                    // Determine property type and parse accordingly
                                    //
                                    if (field.getType().isAssignableFrom(Boolean.class)) {
                                        Boolean value = (Boolean) field.get(factsheet);
                                        //
                                        // Check if bool is true, else set to null
                                        // retresco can't handle null values
                                        //
                                        value = value == null ? null : value ? value : null;
                                        fact = new Fact(fi.getId(), fi.getName(), fi.getTypeHint(), value);
                                    } else if (field.getType().isAssignableFrom(Integer.class)) {
                                        Integer value = (Integer) field.get(factsheet);
                                        fact = new Fact(fi.getId(), fi.getName(), fi.getTypeHint(), value);
                                    } else if (field.getType().isAssignableFrom(Double.class)) {
                                        Double value = (Double) field.get(factsheet);
                                        fact = new Fact(fi.getId(), fi.getName(), fi.getTypeHint(), value);
                                    } else if (field.getType().isAssignableFrom(String.class)) {
                                        String value = (String) field.get(factsheet);
                                        fact = new Fact(fi.getId(), fi.getName(), fi.getTypeHint(), value);
                                    }
                                } else {
                                    log.debug("Field not found for variable {}", snakeToCamel(fi.getName()));
                                }
                            } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
                                log.error("{}", ex);
                            }
                            return fact;

                        })
                        .filter(f -> f != null)
                        .filter(f -> f.getValue() != null)
                        .collect(Collectors.toList());

        return factsList;
    }

    /**
     * Simple method to convert snake_case to camelCase. Needed to convert
     * properties from the api response to the linked entity.
     *
     * @param snakeCase
     * @return
     */
    String snakeToCamel(String snakeCase) {
        while (snakeCase.contains("_")) {
            snakeCase = snakeCase.replaceFirst("_[a-zA-Z]", String.valueOf(Character.toUpperCase(snakeCase.charAt(snakeCase.indexOf("_") + 1))));
        }

        return snakeCase;
    }
}
