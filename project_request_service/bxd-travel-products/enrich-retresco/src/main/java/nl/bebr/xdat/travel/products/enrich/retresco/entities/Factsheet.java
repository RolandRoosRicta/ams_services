/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.retresco.entities;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Kilian Veenstra [kilian@corizon.nl]
 */
@JacksonXmlRootElement(localName = "factsheet")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Factsheet {

    @JacksonXmlProperty(isAttribute = true)
    String lastUpdate;
    @JacksonXmlElementWrapper(localName = "sections")
    @JacksonXmlProperty(localName = "section")
    List<Section> sections;
}
