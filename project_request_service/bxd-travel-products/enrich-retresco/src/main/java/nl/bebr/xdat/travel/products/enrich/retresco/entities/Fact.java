/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.retresco.entities;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Kilian Veenstra [kilian@corizon.nl]
 */
@JacksonXmlRootElement(localName = "fact")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Fact {

    @JacksonXmlProperty(isAttribute = true)
    /**
     * For now free to fill in.
     */
    String id;
    @JacksonXmlProperty(isAttribute = true)
    /**
     * Name of the Fact.
     */
    String name;
    @JacksonXmlProperty(isAttribute = true)
    /**
     * The type of the value. This can be either: int. float. bool.
     * numeric-bool. String.
     */
    String typeHint;
    Object value;
}
