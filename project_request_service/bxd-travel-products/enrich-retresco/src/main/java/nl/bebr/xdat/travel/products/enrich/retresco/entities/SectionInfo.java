/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.retresco.entities;

/**
 *
 * @author Kilian Veenstra [kilian@corizon.nl]
 */
public enum SectionInfo {

    /**
     * Information about the accommodation itself.
     *
     * E.g. telephone number and fax.
     */
    OBJECT_INFORMATION("object_information", 1001),
    /**
     *
     */
    CATEGORY("category", 1002),
    /**
     * Information about the physical object of the accommodation.
     *
     * E.g. total numbers of rooms.
     *
     */
    BUILDING_INFO("building_info", 1003),
    /**
     * Information about the nearest beach. E.g. sandy beach, peddle beach, rock
     * beach.
     */
    BEACH("beach", 1006),
    /**
     * Information about the facilities the accommodation provides.
     *
     * E.g. amount of bars, restaurants or if there is a car park present.
     */
    FACILITIES("facilities", 1007),
    /**
     * Information about the rooms of the accommodation.
     *
     * E.g. if a bath is presence of a bath,shower,minibar,tv.
     */
    ROOMS("rooms", 1008),
    /**
     * Information about the kind of meals that are served at the accommodation.
     * E.g. breakfast buffet or breakfast served.
     */
    MEALS("meals", 1009),
    /**
     * Information about the sport and entertainment activities.
     *
     * E.g. presence of a gym or tennis court.
     */
    SPORTS_ENTERTAINMENT("sports_entertainment", 1010),
    /**
     * Distances from accommodation to nearest features.
     *
     * E.g. city center, park, beach.
     */
    DISTANCES("distances", 1011);

    private String name;
    private int type;

    SectionInfo(String name, int type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public int getType() {
        return type;
    }
}
