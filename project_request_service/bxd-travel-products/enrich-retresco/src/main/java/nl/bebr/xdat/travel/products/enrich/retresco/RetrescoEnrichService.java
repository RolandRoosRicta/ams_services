/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.retresco;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import java.io.IOException;
import java.nio.charset.Charset;
import nl.bebr.xdat.travel.products.api.AccommodationDTO;
import nl.bebr.xdat.travel.products.api.AccommodationService;
import nl.bebr.xdat.travel.products.api.EnrichService;
import nl.bebr.xdat.travel.products.api.Messaging;
import nl.bebr.xdat.travel.products.api.events.RequestDataEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.support.BasicAuthenticationInterceptor;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Kilian Veenstra <kilian@corizon.nl>
 */
@Component
@RabbitListener(queues=Messaging.QUEUE_ENRICH_RETRESCO_REQUEST)
public class RetrescoEnrichService implements EnrichService {

    private static final Logger log = LoggerFactory.getLogger(RetrescoEnrichService.class);

    //
    // Get variables from the config
    //
    @Value("${bxd.travel.products.enrich.retresco.url}")
    private String retrescoUrl;
    @Value("${bxd.travel.products.enrich.retresco.username}")
    private String username;
    @Value("${bxd.travel.products.enrich.retresco.password}")
    private String password;

    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    private AccommodationService accommodationService;
    
    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private RetrescoFactFactory factFactory;

    private XmlMapper xmlMapper;

    /**
     * handle event when: - giata id is available - and retresco is not up to
     * date, or force update is enabled
     *
     * @param event
     */
//    @EventListener(condition = "#event.giataIdAvailable and (!#event.retrescoUpToDate or #event.forceUpdate)")
    @Override
    public void handleRequestDataEvent(String message) throws IOException {
        RequestDataEvent event = mapper.readValue(message, RequestDataEvent.class);
        
        
        log.debug("Handling Retresco UpdateEvent {}", event);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getInterceptors().add(new BasicAuthenticationInterceptor(username, password));
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));

        AccommodationDTO accommodationDTO = accommodationService.findById(event.getAccommodationId()).orElseThrow(() -> {
            return new IllegalStateException("Accommodation not found!");
        });

        try {
            //
            // Create string from factsheet,
            // this is needed because UTF-8 encoding HAS to be added for retresco
            // to process the xml properly.
            //
            String factSheetString = getXmlMapper().writeValueAsString(factFactory.formatAccommodation(accommodationDTO));
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_XML);
            HttpEntity<String> request = new HttpEntity<>(factSheetString, headers);

            ResponseEntity<String> response = restTemplate.postForEntity(retrescoUrl, request, String.class);
            accommodationDTO.setRetrescoText(response.getBody());

            accommodationService.saveAccommodation(accommodationDTO);
        } catch (JsonProcessingException ex) {
            log.error("{}", ex);
        }
    }

    private XmlMapper getXmlMapper() {
        //
        // instantiate xmlMapper when not done so already
        //
        if (xmlMapper == null) {
            xmlMapper = new XmlMapper();
            xmlMapper.configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true);
        }
        return xmlMapper;
    }


}
