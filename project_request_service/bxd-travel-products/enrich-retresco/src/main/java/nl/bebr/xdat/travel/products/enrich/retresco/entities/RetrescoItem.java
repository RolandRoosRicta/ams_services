/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.retresco.entities;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Kilian Veenstra [kilian@corizon.nl]
 */
@JacksonXmlRootElement(localName = "item")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class RetrescoItem {

    @JacksonXmlProperty(isAttribute = true)
    Integer giataId;
    /**
     * Name of the accommodation.
     */
    String name;
    /**
     * This can be left empty for now. Retresco is not parsing the value of this variable.
     */
    String street;
    /**
     * Name of the city where the accommodation is located.
     */
    String city;
    /**
     * The country as a 2 letter code according to ISO 3166-1.
     */
    String country;
    /**
     * A Factsheet hat contains a list of sections.
     */
    Factsheet factsheet;
}
