/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.retresco.entities;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Builder;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import nl.bebr.xdat.travel.products.enrich.retresco.entities.RetrescoItem;

/**
 *
 * @author Timon Veenstra <timon at corizon.nl>
 */
@JacksonXmlRootElement(localName = "result")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class RetrescoResponse {
    
    RetrescoItem item;
}
