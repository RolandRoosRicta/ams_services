/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.retresco;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import nl.bebr.xdat.travel.products.api.AccommodationDTO;
import nl.bebr.xdat.travel.products.api.AccommodationPropFacilityDTO;
import nl.bebr.xdat.travel.products.api.AccommodationPropRoomDTO;
import nl.bebr.xdat.travel.products.api.AccommodationPropSurroundingDTO;
import nl.bebr.xdat.travel.products.enrich.retresco.entities.Fact;
import nl.bebr.xdat.travel.products.enrich.retresco.entities.Factsheet;
import nl.bebr.xdat.travel.products.enrich.retresco.entities.RetrescoItem;
import nl.bebr.xdat.travel.products.enrich.retresco.entities.RetrescoResponse;
import nl.bebr.xdat.travel.products.enrich.retresco.entities.Section;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kilian Veenstra <ktveenstra@bebr.nl>
 */
public class RetrescoFactFactoryTest {

    private static final Logger log = LoggerFactory.getLogger(RetrescoFactFactoryTest.class);

    public RetrescoFactFactoryTest() {
        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "debug");
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of createFacts method, of class RetrescoFactFactory.
     */
    @Test
    public void testCreateFacts() {
        log.info("createFacts");
        try {
            FlatFactsheet accommodation = FlatFactsheet
                    .builder()
                    .mealsBreakfastbuffet(true)
                    .beachSandy(false)
                    .email("bvlaba")
                    .roomAircon(true)
                    .roomBath(false)
                    .sportsBasketball(23)
                    .sportsSteambath(5)
                    .build();

            List<Fact> result = new RetrescoFactFactory(new ModelMapper()).createFacts(accommodation);

            Fact mealsFact = Fact.builder()
                    .id("58")
                    .name("meals_breakfastbuffet")
                    .typeHint("bool")
                    .value(true)
                    .build();

            Fact beachFact = Fact.builder()
                    .id("113")
                    .name("beach_sandy")
                    .typeHint("bool")
                    .value(false)
                    .build();

            Fact beachFact2 = Fact.builder()
                    .id("113")
                    .name("beach_not_sandy")
                    .typeHint("bool")
                    .value(false)
                    .build();

            Fact emailFact = Fact.builder()
                    .id("182")
                    .name("email")
                    .typeHint("string")
                    .value("bvlaba")
                    .build();

            Fact roomBathFact = Fact.builder()
                    .id("12")
                    .name("room_bath")
                    .typeHint("bool")
                    .value(false)
                    .build();
            Fact roomAirconFact = Fact.builder()
                    .id("15")
                    .name("room_aircon")
                    .typeHint("bool")
                    .value(true)
                    .build();
            Fact sportsBasketballFact = Fact.builder()
                    .id("154")
                    .name("sports_basketball")
                    .typeHint("numeric-bool")
                    .value(23)
                    .build();
            Fact sportsSteambathFact = Fact.builder()
                    .id("68")
                    .name("sports_steambath")
                    .typeHint("numeric-bool")
                    .value(5)
                    .build();

            assertTrue(result.stream().anyMatch(f -> emailFact.equals(f)));
            assertFalse(result.stream().anyMatch(f -> beachFact.equals(f)));
            assertFalse(result.stream().anyMatch(f -> beachFact2.equals(f)));
            assertTrue(result.stream().anyMatch(f -> mealsFact.equals(f)));
            assertFalse(result.stream().anyMatch(f -> roomBathFact.equals(f)));
            assertTrue(result.stream().anyMatch(f -> roomAirconFact.equals(f)));
            assertTrue(result.stream().anyMatch(f -> sportsBasketballFact.equals(f)));
            assertTrue(result.stream().anyMatch(f -> sportsSteambathFact.equals(f)));

        } catch (Exception ex) {
            StringWriter swr = new StringWriter();
            PrintWriter out = new PrintWriter(swr);
            ex.printStackTrace(out);
            out.flush();
            fail(swr.getBuffer().toString());
        }

    }

    /**
     * Test of snakeToCamel method, of class RetrescoFactFactory.
     */
    @Test
    public void testSnakeToCamel() {
        log.info("snakeToCamel");
        String snakeCase = "hotelType_very_hotel_thing";
        String expResult = "hotelTypeVeryHotelThing";
        String result = new RetrescoFactFactory(new ModelMapper()).snakeToCamel(snakeCase);
        assertEquals(expResult, result);
    }

    @Test
    public void testDtoToFactsheet() {
        System.out.println("dtoToFactsheet");
        RetrescoFactFactory factory = new RetrescoFactFactory(new ModelMapper());

        AccommodationDTO accommodationDTO = AccommodationDTO.builder()
                .sourceIdMap(new HashMap<>())
                .sourceStatusMap(new HashMap<>())
                .hoteltypeHoteldecharme(true)
                .fax("fax")
                .hoteltypeFamily(false)
                .giataId(8)
                .countryCode("NL")
                .name("Zwols hotel")
                .locationName("Zwolle")
                .room(AccommodationPropRoomDTO.builder()
                        .roomAircon(true)
                        .roomBalcony(false)
                        .roomAirconIndiv(true)
                        .roomBath(false)
                        .build())
                .surrounding(AccommodationPropSurroundingDTO.builder()
                        .distLake(99)
                        .build())
                .facility(AccommodationPropFacilityDTO.builder()
                        .beachParasols(true)
                        .beachSandy(false)
                        .build())
                .build();

        FlatFactsheet factsheet = factory.dtoToFactsheet(accommodationDTO);
        assertNotNull(factsheet);
        assertFalse(factsheet.getHoteltypeFamily());
        assertTrue(factsheet.getHoteltypeHoteldecharme());
        assertEquals("fax", factsheet.getFax());
        assertEquals((int) 8, (int) factsheet.getGiataId());
        assertEquals("NL", factsheet.getCountry());
        assertEquals("Zwols hotel", factsheet.getName());
        assertEquals("Zwolle", factsheet.getCity());

        assertFalse(factsheet.getRoomBalcony());
        assertNotNull(factsheet.getRoomAircon());
        assertNotNull(factsheet.getRoomAirconIndiv());
        assertFalse(factsheet.getRoomBath());
        assertFalse(factsheet.getRoomBalcony());
        assertFalse(factsheet.getRoomBath());
        assertTrue(factsheet.getRoomAircon());
        assertTrue(factsheet.getRoomAirconIndiv());

        assertEquals(99, factsheet.getDistLake().intValue());
        assertTrue(factsheet.getBeachParasols());
        assertFalse(factsheet.getBeachSandy());
    }

    /**
     * Test of formatAccommodation method, of class RetrescoFactFactory.
     */
    @Test
    public void testFormatAccommodation() {
        System.out.println("formatAccommodation");
        AccommodationDTO accommodationDTO = AccommodationDTO.builder()
                .sourceIdMap(new HashMap<>())
                .sourceStatusMap(new HashMap<>())
                .hoteltypeHoteldecharme(true)
                .fax("fax")
                .hoteltypeFamily(false)
                .giataId(8)
                .countryCode("NL")
                .name("Zwols hotel")
                .locationName("Zwolle")
                .build();

        Fact decharmeFact = Fact.builder()
                .id("42")
                .name("hoteltype_hoteldecharme")
                .typeHint("bool")
                .value(true)
                .build();
        Fact faxFact = Fact.builder()
                .id("1")
                .name("fax")
                .typeHint("string")
                .value("fax")
                .build();

        List<Fact> factList = new ArrayList<>();
        factList.add(decharmeFact);
        factList.add(faxFact);

        Section section = Section.builder()
                .name("object_information")
                .type(1001)
                .facts(factList)
                .build();

        List<Section> sectionList = new ArrayList<>();
        sectionList.add(section);

        Factsheet factsheet = Factsheet.builder()
                .sections(sectionList)
                .build();

        RetrescoItem retrescoItem = RetrescoItem.builder()
                .city("Zwolle")
                .country("NL")
                .factsheet(factsheet)
                .giataId(8)
                .name("Zwols hotel")
                .build();

        RetrescoResponse expResult = RetrescoResponse.builder()
                .item(retrescoItem)
                .build();

        RetrescoResponse result = new RetrescoFactFactory(new ModelMapper()).formatAccommodation(accommodationDTO);

        System.out.println("RESULT: " + result);
        try {
            System.out.println("RESULT XML: " + new XmlMapper().writeValueAsString(result));
        } catch (JsonProcessingException ex) {
            java.util.logging.Logger.getLogger(RetrescoFactFactoryTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        assertNotNull(result);
        assertNotNull(result.getItem());
        assertNotNull(result.getItem().getFactsheet());
        assertNotNull(result.getItem().getFactsheet().getSections());

        assertEquals(expResult.getItem().getCity(), result.getItem().getCity());
        assertEquals(expResult.getItem().getCountry(), result.getItem().getCountry());
        assertEquals(expResult.getItem().getGiataId(), result.getItem().getGiataId());
        assertEquals(expResult.getItem().getName(), result.getItem().getName());

        assertEquals(expResult.getItem().getFactsheet().getSections().size(), result.getItem().getFactsheet().getSections().size());
        Section resultSection = result.getItem().getFactsheet().getSections().get(0);
        Section expSection = expResult.getItem().getFactsheet().getSections().get(0);
        assertEquals(expSection.getType(), resultSection.getType());
        assertEquals(expSection.getName(), resultSection.getName());
        assertEquals(expSection.getFacts().size(), resultSection.getFacts().size());

        assertTrue(resultSection.getFacts().stream().anyMatch(f -> decharmeFact.equals(f)));
        assertTrue(resultSection.getFacts().stream().anyMatch(f -> faxFact.equals(f)));
    }

}
