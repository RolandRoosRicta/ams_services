source: https://ghgml.giatamedia.com/webservice/specs/rest.en.html


The node <result><item> has the following nodes:

name
Name of the accommodation

street
empty

city
Name of the city where the accommodation is located

country
The country as a 2-letter-code according to ISO 3166-1

factsheet
The fact sheet for this property with an attribute lastUpdate indicating the time of the last modification.

Contains one node of type <sections> with one or more nodes of type <section>.

Each node <section> represents a group of facts and contains the following attributes:

type
A constant integer value to identify the group of facts

name
The name of the group of facts

Each node <section> contains one node of type <title> with the translation of the name of the group of facts.

Each node <section> contains one node of type <facts> with several nodes of type <fact>

Each node <fact> represents one fact and contains the following attributes:

id
A constant integer value to identify the fact

name
The name of the fact

typeHint
Indicates the type of the values of the fact

Possible values:

bool

int

float

string

Each node <fact> contains one node of type <title> with the translation of the name of the fact.
Each node of type <fact> contains one or more nodes of type <value> representing the values of the fact.

Possible attributes:

fee
Indicates if a fee is applicable

dummy
Indicates the value as a test value which may differ from the actual value

name
Used to identify the value when a fact has more than one value

unit
Indicates the unit in which the value is given when not deduced from the fact

Possible values:

m: meters

min: minutes

