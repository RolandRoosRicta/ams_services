/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.osm;

import java.io.PrintWriter;
import java.io.StringWriter;
import nl.bebr.xdat.travel.products.api.AccommodationDTO;
import nl.bebr.xdat.travel.products.api.AccommodationPropSurroundingDTO;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
public class OSMEnrichmentTest {

    public OSMEnrichmentTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of fillSurroundings method, of class OSMEnrichment.
     */
//    @Test
    public void testFillSurroundings() {
        System.out.println("fillSurroundings");
        try {
            AccommodationDTO acco = AccommodationDTO.builder()
                    .id(1235l)
                    .surrounding(AccommodationPropSurroundingDTO.builder().build())
                    .build();
            OSMEnrichService instance = new OSMEnrichService();
//            AccommodationDTO result = instance.fillSurroundings(acco);
//
//            System.out.println(":::" + result);

        } catch (Exception ex) {
            StringWriter swr = new StringWriter();
            PrintWriter out = new PrintWriter(swr);
            ex.printStackTrace(out);
            out.flush();
            fail(swr.getBuffer().toString());
        }
    }

    /**
     * Test of getDistanceToNearest method, of class OSMEnrichment.
     */
//    @Test
    public void testGetDistanceToNearest() {
        System.out.println("getDistanceToNearest");
        try {
//        53.2166232,6.5676525
            MapFeature eg = MapFeature.ESCAPE_GAME;
            String key = eg.getKey();
            String value = eg.getValue();
            double lat = 53.2166232;
            double lng = 6.5676525;
            int radius = eg.getSearchRadius();
            OSMEnrichService instance = new OSMEnrichService();
//        Double expResult = null;
            Double result = instance.getDistanceToNearest(key, value, lat, lng, radius);

            System.out.println("NEAREST: " + result);
//        assertEquals(expResult, result);
        } catch (Exception ex) {
            StringWriter swr = new StringWriter();
            PrintWriter out = new PrintWriter(swr);
            ex.printStackTrace(out);
            out.flush();
            fail(swr.getBuffer().toString());
        }
    }
        /**
         * Test of getDensity method, of class OSMEnrichment.
         */
        //    @Test
    public void testGetDensity() {
        System.out.println("getDensity");
        String key = "";
        String value = "";
        double lat = 0.0;
        double lng = 0.0;
        int radius = 0;
        OSMEnrichService instance = new OSMEnrichService();
        Double expResult = null;
        Double result = instance.getDensity(key, value, lat, lng, radius);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
}
