/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.osm;

import java.math.BigDecimal;
import java.math.RoundingMode;
import org.json.JSONArray;
import org.json.JSONObject;
import static org.testng.Assert.*;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
public class OSMEnrichmentHelperNGTest {

    public OSMEnrichmentHelperNGTest() {
    }

    @org.testng.annotations.BeforeClass
    public static void setUpClass() throws Exception {
    }

    @org.testng.annotations.AfterClass
    public static void tearDownClass() throws Exception {
    }

    @org.testng.annotations.BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @org.testng.annotations.AfterMethod
    public void tearDownMethod() throws Exception {
    }

    /**
     * Test of getFurthestDistanceFromJSONArray method, of class
     * OSMEnrichmentHelper.
     */
//    @org.testng.annotations.Test
    public void testGetFurthestDistanceFromJSONArray() throws Exception {
        System.out.println("getFurthestDistanceFromJSONArray");
        OSMEnrichmentHelper instance = new OSMEnrichmentHelper();

        String beaches = "{"
                + "  \"elements\": [\n"
                + "\n"
                + "{\n"
                + "  \"type\": \"node\",\n"
                + "  \"id\": 1,\n"
                + "  \"lat\": 50.0,\n"
                + "  \"lon\": 4.5,\n"
                + "  \"tags\": {\n"
                + "    \"name\": \"Scheveningen\",\n"
                + "    \"natural\": \"beach\",\n"
                + "  }\n"
                + "},\n"
                + "{\n"
                + "  \"type\": \"node\",\n"
                + "  \"id\": 2,\n"
                + "  \"lat\": 60.0,\n"
                + "  \"lon\": 4.5,\n"
                + "  \"tags\": {\n"
                + "    \"name\": \"Scheveningen\",\n"
                + "    \"natural\": \"beach\"\n"
                + "  }\n"
                + "}\n"
                + "  ]\n"
                + "}";

        double lat = 54.9;
        double lng = 4.5;

        JSONObject json = new JSONObject(beaches);
        JSONArray items = json.getJSONArray("elements");
        Double expResult = 567094.0;
        Double result = instance.getFurthestDistanceFromJSONArray(lat, lng, items);

        BigDecimal rounder = new BigDecimal(result);
        rounder = rounder.setScale(0, RoundingMode.HALF_UP);
        Double roundedResult = rounder.doubleValue();

        assertEquals(roundedResult, expResult);
    }

    /**
     * Test of filterMinimumInhabitants method, of class OSMEnrichmentHelper.
     */
    @org.testng.annotations.Test
    public void testFilterMinimumInhabitants() {
        System.out.println("filterMinimumInhabitants");
        JSONArray results = new JSONArray();

        //0
        JSONObject obj0 = new JSONObject();
        JSONObject tags0 = new JSONObject();
        tags0.put("population", 123456);
        obj0.put("tags", tags0);
        results.put(obj0);

        //1
        JSONObject obj1 = new JSONObject();
        JSONObject tags1 = new JSONObject();
        tags1.put("population", 12345);
        obj1.put("tags", tags1);
        results.put(obj1);

        //2
        JSONObject obj2 = new JSONObject();
        JSONObject tags2 = new JSONObject();
        tags2.put("population", 98);
        obj2.put("tags", tags2);
        results.put(obj2);

        //3
        JSONObject obj3 = new JSONObject();
        JSONObject tags3 = new JSONObject();
        tags3.put("population", 987654321);
        obj3.put("tags", tags3);
        results.put(obj3);

        OSMEnrichmentHelper instance = new OSMEnrichmentHelper();
        JSONArray result = instance.filterMinimumInhabitants(results);

        for (int i = 0; i < result.length(); i++) {
            JSONObject jo = result.getJSONObject(i);
            assertTrue(obj0.equals(jo) || obj3.equals(jo));
            assertFalse(obj1.equals(jo) || obj2.equals(jo));
        }

        System.out.println("RESULT: " + result);
    }

}
