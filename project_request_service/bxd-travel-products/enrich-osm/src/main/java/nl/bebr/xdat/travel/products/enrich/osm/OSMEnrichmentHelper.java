/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.osm;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import lombok.Setter;
//import nl.bebr.xdat.domain.travel.util.GeometryUtil;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author Kenrik Veenstra <kenrik@corizon.nl>
 */
@Component
public class OSMEnrichmentHelper {

    private static final int MINIMUM_INHABITANTS = 100000;

    @Value("${xdat.osm.overpass:https://lz4.overpass-api.de/api/interpreter}")
    private String overpassAddress;

    private static final Logger log = LoggerFactory.getLogger(OSMEnrichmentHelper.class);

    public String getResultsForKeyValue(String key, String value, double lat, double lng, int radius) {

        //
        // query for nearest given key and value in the given radius
        //
        String requestXML = "<osm-script output=\"json\" timeout=\"100\">\n"
                + "  <union into=\"_\">\n"
                + "    <query into=\"_\" type=\"node\">\n"
                + "      <has-kv k=\"" + key + "\" modv=\"\" regv=\"" + value + "\"/>\n"
                + "      <around lat=\"" + lat + "\" lon=\"" + lng + "\" radius=\"" + radius + "\"/>\n"
                + "    </query>\n"
                + "    <query into=\"_\" type=\"way\">\n"
                + "      <has-kv k=\"" + key + "\" modv=\"\" regv=\"" + value + "\"/>\n"
                + "      <around lat=\"" + lat + "\" lon=\"" + lng + "\" radius=\"" + radius + "\"/>\n"
                + "    </query>\n"
                + "    <query into=\"_\" type=\"relation\">\n"
                + "      <has-kv k=\"" + key + "\" modv=\"\" regv=\"" + value + "\"/>\n"
                + "      <around lat=\"" + lat + "\" lon=\"" + lng + "\" radius=\"" + radius + "\"/>\n"
                + "    </query>\n"
                + "  </union>\n"
                + "  <print e=\"\" from=\"_\" geometry=\"center\" mode=\"body\" n=\"\" order=\"quadtile\" s=\"\" w=\"\"/>\n"
                + "</osm-script>";

        try {
            URL url = new URL(overpassAddress);
            URLConnection con = url.openConnection();

            //
            // specify that we will send output and accept input
            //
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setConnectTimeout(20000);  // long timeout, but not infinite
            con.setReadTimeout(20000);
            con.setUseCaches(false);
            con.setDefaultUseCaches(false);

            //
            // tell the web server what we are sending
            //
            //TODO catch and handle socket timeouts (java.net.SocketTimeoutException: connect timed out)
            con.setRequestProperty("Content-Type", "text/xml");
            try ( OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream())) {
                writer.write(requestXML);
                writer.flush();
            }

            //
            // reading the response
            //
            InputStreamReader reader = new InputStreamReader(con.getInputStream());
            StringBuilder buf = new StringBuilder();
            char[] cbuf = new char[2048];
            int num;
            while (-1 != (num = reader.read(cbuf))) {
                buf.append(cbuf, 0, num);
            }

            return buf.toString();
        } catch (IOException ex) {
            log.error("{}", ex);
        }

        return null;
    }

    Double getFurthestDistanceFromJSONArray(double lat, double lng, JSONArray items) throws JSONException, IOException {

        double furthestDistance = Double.MIN_VALUE;

        //
        // json type can be 'node', or different, 
        // node has ready-steady lat/long to use, others have it hidden in a 
        // different json element
        //
        for (int i = 0; i < items.length(); i++) {
            JSONObject item = items.getJSONObject(i);

            if (item.getString("type").equals("node")) {

                //
                // fetch lat/lng from query results
                //
                double nodeLat = item.getDouble("lat");
                double nodeLng = item.getDouble("lon");
                double nodeDistance = distanceBetween(lat, lng, nodeLat, nodeLng);

                //
                // check if it's furthest distance so far
                //
                if (nodeDistance > furthestDistance) {
                    furthestDistance = nodeDistance;
                }

            } else if (item.has("center")
                    && item.getJSONObject("center").has("lat")
                    && item.getJSONObject("center").has("lon")) {

                double wayLat = item.getJSONObject("center").getDouble("lat");
                double wayLng = item.getJSONObject("center").getDouble("lon");
                double wayDistance = distanceBetween(lat, lng, wayLat, wayLng);
                //
                // check if it's furthest distance so far
                //
                if (wayDistance > furthestDistance) {
                    furthestDistance = wayDistance;
                }
            }
        }

        return furthestDistance;
    }

    Double getDistanceToNearestFromJSONArray(double lat, double lng, JSONArray items) throws JSONException, IOException {

        double resultLat = Double.MAX_VALUE;
        double resultLng = Double.MAX_VALUE;
        double smallestDistance = Double.MAX_VALUE;

        //
        // json type can be 'node', or different, 
        // node has ready-steady lat/long to use, others have it hidden in a 
        // different json element
        //
        for (int i = 0; i < items.length(); i++) {
            JSONObject item = items.getJSONObject(i);

            if (item.getString("type").equals("node")) {

                //
                // fetch lat/lng from query results
                //
                double nodeLat = item.getDouble("lat");
                double nodeLng = item.getDouble("lon");
                double nodeDistance = distanceBetween(lat, lng, nodeLat, nodeLng);

                //
                // check if it's shortest distance so far
                //
                if (nodeDistance < smallestDistance) {
                    smallestDistance = nodeDistance;
                    resultLat = nodeLat;
                    resultLng = nodeLng;
                }

            } else if (item.has("center")
                    && item.getJSONObject("center").has("lat")
                    && item.getJSONObject("center").has("lon")) {

                double wayLat = item.getJSONObject("center").getDouble("lat");
                double wayLng = item.getJSONObject("center").getDouble("lon");
                double wayDistance = distanceBetween(lat, lng, wayLat, wayLng);
                //
                // check if it's shortest distance so far
                //
                if (wayDistance < smallestDistance) {
                    smallestDistance = wayDistance;
                    resultLat = wayLat;
                    resultLng = wayLng;
                }
            }
        }
        return smallestDistance;
    }

    JSONArray filterMinimumInhabitants(JSONArray results) {
        //
        // Filter JSONArray for items where population is at least
        // at given maximum
        //
        JSONArray filtered = new JSONArray();

        for (int j = 0; j < results.length(); j++) {
            JSONObject item = results.getJSONObject(j);

            if (item.has("tags")
                    && item.getJSONObject("tags").has("population")
                    && item.getJSONObject("tags").get("population").getClass().equals(Integer.class)
                    && item.getJSONObject("tags").getInt("population") >= MINIMUM_INHABITANTS) {
                filtered.put(item);
            }
        }
        return filtered;
    }

    String geocode(String name) throws IOException {
        String urlBase = "http://nominatim.openstreetmap.org/search/?format=json&limit=1&q=";

        //
        // convert to html encoding
        //
        String encodedName = URLEncoder.encode(name, "UTF-8");
        URL url = new URL(urlBase + encodedName);

        //
        // Retrieve output
        //
        URLConnection conn = url.openConnection();
        ByteArrayOutputStream output = new ByteArrayOutputStream(1024);
        IOUtils.copy(conn.getInputStream(), output);
        output.close();

        try {
            //
            // send request
            //
            if (!output.toString().equals("") && !output.toString().equals("[]")) {

                JSONArray json = new JSONArray(output.toString());

                //
                // get first in array if available, retrieve lat/lon
                //
                if (json.length() >= 0
                        && json.getJSONObject(0) != null
                        && json.getJSONObject(0).getString("lat") != null
                        && json.getJSONObject(0).getString("lon") != null) {
                    return json.getJSONObject(0).getString("lat") + "," + json.getJSONObject(0).getString("lon");
                }
            }
        } catch (JSONException ex) {
            log.error("Couldn't parse JSON for nearest request {}", ex);
        }

        return null;
    }

    /**
     * Calculate distance between two points in latitude and longitude. Uses
     * Haversine method as its base.
     *
     * @param lat1, latitude for location 1
     * @param lng1, longitude for location 1
     * @param lat2, latitude for location 2
     * @param lng2, longitude for location 2
     * @return is in meter
     */
    public double distanceBetween(double lat1, double lng1, double lat2, double lng2) {
        return distanceBetween(lat1, lng1, 0.0, lat2, lng2, 0.0);
    }

    /**
     * Calculate distance between two points in latitude and longitude taking
     * into account height difference. If you are not interested in height
     * difference pass 0.0. Uses Haversine method as its base.
     *
     * @param lat1, latitude for location 1
     * @param lng1, longitude for location 1
     * @param el1, elevation for location 1
     * @param lat2, latitude for location 2
     * @param lng2, longitude for location 2
     * @param el2, elevation for location 2
     * @return
     * @returns Distance in Meters
     */
    public double distanceBetween(double lat1, double lng1, double el1,
            double lat2, double lng2, double el2) {

        final int radius = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lng1 - lng2);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = radius * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance);
    }
}
