/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.osm;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
public enum MapFeature {

    AIRPORT("aeroway", "aerodrome", "distanceToNearestAirport", Distance.FORTY_KM),
    FAST_FOOD("amenity", "fast_food", "fastFoodRestaurantDensity", Distance.ONE_KM),
    RESTAURANT("amenity", "restaurant", "restaurantDensity", Distance.ONE_KM),
    BAR("amenity", "bar", "barDensity", Distance.ONE_KM),
    BICYCLE_PARKING("amenity", "bicycle_parking", "distanceToNearestBicycleParking", Distance.FIVE_KM),
    BICYCLE_RENTAL("amenity", "bicycle_rental", "distanceToNearestBicycleRental", Distance.TWENTY_KM),
    BOAT_RENTAL("amenity", "boat_rental", "distanceToNearestBoatRental", Distance.TWENTY_KM),
    BUS_STATION("amenity", "bus_station", "distanceToNearestBusStation", Distance.FIVE_KM),
    CAR_RENTAL("amenity", "car_rental", "distanceToNearestCarRental", Distance.TWENTY_KM),
    FERRY_TERMINAL("amenity", "ferry_terminal", "distanceToNearestFerryTerminal", Distance.TWENTY_KM),
    CHARGING_STATION("amenity", "charging_station", "electricCarChargingStationDensity", Distance.TWENTY_KM),
    FUEL("amenity", "fuel", "distanceToNearestFuelStation", Distance.TWENTY_KM),
    PARKING("amenity", "parking", "distanceToNearestParking", Distance.FIVE_KM),
    TAXI("amenity", "taxi", "distanceToNearestTaxiStand", Distance.FIVE_KM),
    ATM("amenity", "atm", "atmDensity", Distance.FIVE_KM),
    BUREAU_DE_CHANGE("amenity", "bureau_de_change", "distanceToNearestCurrencyExchange", Distance.TEN_KM),
    HOSPITAL("amenity", "hospital", "distanceToNearestHospital", Distance.TWENTY_KM),
    PHARMACY("amenity", "pharmacy", "distanceToNearestPharmacy", Distance.TEN_KM),
    CASINO("amenity", "casino", "distanceToNearestCasino", Distance.TEN_KM),
    CINEMA("amenity", "cinema", "distanceToNearestCinema", Distance.TEN_KM),
    NIGHTCLUB("amenity", "nightclub", "nightclubDensity", Distance.FIVE_KM),
    PLANETARIUM("amenity", "planetarium", "distanceToNearestPlanetarium", Distance.TWENTY_KM),
    THEATRE("amenity", "theatre", "theatreDensity", Distance.FIVE_KM),
    DIVE_CENTRE("amenity", "dive_centre", "diveCentreDensity", Distance.TWENTY_KM),
    EMBASSY("amenity", "embassy", "distanceToNearestEmbassy", Distance.TWENTY_KM),
    INTERNET_CAFE("amenity", "internet_cafe", "distanceToNearestInternetCafe", Distance.TEN_KM),
    POLICE("amenity", "police", "distanceToNearestPoliceStation", Distance.TEN_KM),
    PUBLIC_BATH("amenity", "public_bath", "distanceToNearestPublicBath", Distance.TEN_KM),
    NATIONAL_PARK("boundary", "national_park", "nationalParkDensity", Distance.TWENTY_KM),
    SUPERMARKET("building", "supermarket", "distanceToNearestSupermarket", Distance.FIVE_KM),
    KIOSK("building", "kiosk", "distanceToNearestKiosk", Distance.FIVE_KM),
    CHURCH("building", "church", "distanceToNearestChurch", Distance.TEN_KM),
    MOSQUE("building", "mosque", "distanceToNearestMosque", Distance.TEN_KM),
    SYNAGOGUE("building", "synagogue", "distanceToNearestSynagogue", Distance.TEN_KM),
    STADIUM("building", "stadium", "distanceToNearestStadium", Distance.TEN_KM),
    TRAIN_STATION("building", "train_station", "distanceToNearestTrainStation", Distance.TEN_KM),
    PARKING_BUILDING("building", "parking", "distanceToNearestParkingGarage", Distance.FIVE_KM),
    SPORTS_HALL("building", "sports_hall", "distanceToNearestSportsHall", Distance.FIVE_KM),
    AMBULANCE_STATION("emergency", "ambulance_station", "distanceToNearestAmbulanceStation", Distance.TEN_KM),
    DEFIBRILLATOR("emergency", "defibrillator", "distanceToNearestDefibrillator", Distance.ONE_KM),
    EMERGENCY_WARD_ENTRANCE("emergency", "emergency_ward_entrance", "distanceToNearestEmergencyWardEntrance", Distance.TEN_KM),
    FIRE_EXTINGUISHER("emergency", "fire_extinguisher", "distanceToNearestFireExtinguisher", Distance.ONE_KM),
    MOUNTAIN_RESCUE("emergency", "mountain_rescue", "distanceToNearestMountainRescueBase", Distance.TWENTY_KM),
    ASSEMBLY_POINT_EARTHQUAKE("emergency", "assembly_point:earthquake=yes", "distanceToNearestAssemblyPointEarthquake", Distance.FIVE_KM),
    ASSEMBLY_POINT_FIRE("emergency", "assembly_point:fire=yes", "distanceToNearestAssemblyPointFire", Distance.FIVE_KM),
    ASSEMBLY_POINT_FLOOD("emergency", "assembly_point:flood=yes", "distanceToNearestAssemblyPointFlood", Distance.FIVE_KM),
    ASSEMBLY_POINT_LANDSLIDE("emergency", "assembly_point:landslide=yes", "distanceToNearestAssemblyPointLandslide", Distance.FIVE_KM),
    ASSEMBLY_POINT_TORNADO("emergency", "assembly_point:tornado=yes", "distanceToNearestAssemblyPointTornado", Distance.FIVE_KM),
    ASSEMBLY_POINT_TSUNAMI("emergency", "assembly_point:tsunami=yes", "distanceToNearestAssemblyPointTsunami", Distance.FIVE_KM),
    AIRCRAFT("historic", "aircraft", "distanceToNearestAircraft", Distance.TWENTY_KM),
    AQUEDUCT("historic", "aqueduct", "distanceToNearestAquaduct", Distance.TWENTY_KM),
    ARCHAEOLOGICAL_SITE("historic", "archaeological_site", "distanceToNearestArchaeologicalSite", Distance.TWENTY_KM),
    BATTLEFIELD("historic", "battlefield", "distanceToNearestBattlefield", Distance.TWENTY_KM),
    CANNON("historic", "cannon", "distanceToNearestCannon", Distance.TWENTY_KM),
    CASTLE("historic", "castle", "distanceToNearestCastle", Distance.TWENTY_KM),
    CITY_GATE("historic", "city_gate", "distanceToNearestCityGate", Distance.TWENTY_KM),
    ADULT_GAMING_CENTRE("leisure", "adult_gaming_centre", "distanceToNearestAdultGamingCentreGambling", Distance.TEN_KM),
    AMUSEMENT_ARCADE("leisure", "amusement_arcade", "distanceToNearestArcade", Distance.TEN_KM),
    BEACH_RESORT("leisure", "beach_resort", "beachResortDensity", Distance.TWENTY_KM),
    SWIMMING_POOL("leisure", "swimming_pool", "distanceToNearestSwimmingPool", Distance.TEN_KM),
    BIRD_HIDE("leisure", "bird_hide", "distanceToNearestBirdHide", Distance.TWENTY_KM),
    WILDLIFE_HIDE("leisure", "wildlife_hide", "distanceToNearestWildlifeHide", Distance.TWENTY_KM),
    WATER_PARK("leisure", "water_park", "waterParkDensity", Distance.TWENTY_KM),
    HORSE_RIDING("leisure", "horse_riding", "distanceToNearestHorseRiding", Distance.TWENTY_KM),
    DANCE("leisure", "dance", "dancePlaceToGoDancingDensity", Distance.ONE_KM),
    DOG_PARK("leisure", "dog_park", "distanceToNearestDogPark", Distance.ONE_KM),
    ESCAPE_GAME("leisure", "escape_game", "distanceToNearestEscapeRoom", Distance.TEN_KM),
    FISHING("leisure", "fishing", "distanceToNearestFishing", Distance.TEN_KM),
    FITNESS_CENTRE("leisure", "fitness_centre", "distanceToNearestFitnessCentre", Distance.FIVE_KM),
    ICE_RINK("leisure", "ice_rink", "distanceToNearestIceRink", Distance.TEN_KM),
    MINIATURE_GOLF("leisure", "miniature_golf", "distanceToNearestMinigolf", Distance.FIVE_KM),
    MARINA("leisure", "marina", "distanceToNearestMarina", Distance.TWENTY_KM),
    NATURE_RESERVE("leisure", "nature_reserve", "distanceToNearestNatureReserve", Distance.TWENTY_KM),
    PARK("leisure", "park", "distanceToNearestMunicipalPark", Distance.FIVE_KM),
    PLAYGROUND("leisure", "playground", "distanceToNearestPlayground", Distance.FIVE_KM),
    SPORTS_CENTRE("leisure", "sports_centre", "distanceToNearestSportsCentre", Distance.TEN_KM),
    SWIMMING_AREA("leisure", "swimming_area", "distanceToNearestSwimmingArea", Distance.TEN_KM),
    TRACK("leisure", "track", "distanceToNearestRunningTrack", Distance.TEN_KM),
    BEACH("natural", "beach", "distanceToNearestBeach", Distance.TEN_KM),
    NATURAL_FEATURE("natural", "national_park", "distanceToNearestNaturalFeature", Distance.TEN_KM),
    CITY_CENTRE("place", "city|town", "distanceToNearestCityCentre", Distance.FORTY_KM),
    ALPINE_HUT("tourism", "alpine_hut", "distanceToNearestAlpineHut", Distance.TWENTY_KM),
    AQUARIUM("tourism", "aquarium", "distanceToNearestAquarium", Distance.TEN_KM),
    ARTWORK("tourism", "artwork", "distanceToNearestArtwork", Distance.FIVE_KM),
    ATTRACTION("tourism", "attraction", "attractionDensity", Distance.FIVE_KM),
    GALLERY("tourism", "gallery", "galleryDensity", Distance.FIVE_KM),
    INFORMATION("tourism", "information", "distanceToNearestInformation", Distance.FIVE_KM),
    MUSEUM("tourism", "museum", "museumDensity", Distance.TEN_KM),
    THEME_PARK("tourism", "theme_park", "distanceToNearestThemePark", Distance.TWENTY_KM),
    VIEWPOINT("tourism", "viewpoint", "distanceToNearestViewpoint", Distance.TWENTY_KM),
    ZOO("tourism", "zoo", "distanceToNearestZoo", Distance.TWENTY_KM);

    private final String key;
    private final String value;
    private final String variableName;
    private final Integer searchRadius;

    MapFeature(String key, String value, String variableName, Integer searchRadius) {
        this.key = key;
        this.value = value;
        this.variableName = variableName;
        this.searchRadius = searchRadius;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public String getVariableName() {
        return variableName;
    }

    public Integer getSearchRadius() {
        return searchRadius;
    }

    public Boolean isDensity() {
        return getVariableName().toLowerCase().contains("density");
    }

    private static class Distance {

        public static final Integer ONE_KM = 1000;
        public static final Integer FIVE_KM = 5000;
        public static final Integer TEN_KM = 10000;
        public static final Integer TWENTY_KM = 20000;
        public static final Integer FORTY_KM = 40000;
    }

}
