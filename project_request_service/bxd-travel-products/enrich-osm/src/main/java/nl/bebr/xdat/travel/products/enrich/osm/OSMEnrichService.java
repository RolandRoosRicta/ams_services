/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.osm;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import nl.bebr.xdat.travel.products.api.Accommodation;
import nl.bebr.xdat.travel.products.api.AccommodationDTO;
import nl.bebr.xdat.travel.products.api.AccommodationPropSurroundingDTO;
import nl.bebr.xdat.travel.products.api.AccommodationService;
import nl.bebr.xdat.travel.products.api.EnrichService;
import nl.bebr.xdat.travel.products.api.Messaging;
import nl.bebr.xdat.travel.products.api.events.RequestDataEvent;
import static nl.bebr.xdat.travel.products.enrich.osm.MapFeature.CITY_CENTRE;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Kenrik Veenstra <kenrik@corizon.nl>
 */
@Component
@RabbitListener(queues = Messaging.QUEUE_ENRICH_OSM_REQUEST)
public class OSMEnrichService implements EnrichService {

    private static final Logger log = LoggerFactory.getLogger(OSMEnrichService.class);

    //
    // etc
    //
    private static final int MAX_LOOPS = 3;
    private static final int RADIUS_MULTIPLIER = 3;
    private static final int MINIMUM_DENSITY_RESULTS = 10;

    private static Random random = new Random();

    @Autowired
    private OSMEnrichmentHelper helper;

    @Autowired
    private ObjectMapper mapper;    

    @Autowired
    private AccommodationService accommodationService;

    AccommodationDTO fillSurroundings(AccommodationDTO acco) {

        AccommodationPropSurroundingDTO surrounding = acco.getSurrounding();

        AtomicInteger counter = new AtomicInteger();
        int total = MapFeature.values().length + 1;

        //
        // Loop through all the variables in MapFeature
        //
        Arrays.asList(MapFeature.values()).forEach(feature -> {
            try {

                log.info("Trying to retrieve variable: {}, {} of {}", feature.getVariableName(), counter.incrementAndGet(), total);

                //
                // Find appropriate field to set
                //
                Field field = AccommodationPropSurroundingDTO.class.getDeclaredField(feature.getVariableName());
                field.setAccessible(true);

                //
                // Use most accurate coordinates
                //
                Double lat = acco.getImputedLat() != null ? acco.getImputedLat() : acco.getLat();
                Double lng = acco.getImputedLng() != null ? acco.getImputedLng() : acco.getLng();

                //                
                // Check what kind of variable to set, either density or distance 
                // to nearsest                
                //                
                if (feature.isDensity()) {
                    Double density = getDensity(feature.getKey(), feature.getValue(), lat, lng, feature.getSearchRadius());
                    field.set(surrounding, density);
                } else {
                    Double distance = getDistanceToNearest(feature.getKey(), feature.getValue(), lat, lng, feature.getSearchRadius());
                    field.set(surrounding, distance);
                }

                acco.setOsmStatus(Accommodation.SourceStatus.UP_TO_DATE);

            } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
                log.error("{}", ex);
            }
        });
        return acco;
    }

    /**
     * Universal method to query the closest point of interest for given
     * key-value combination with provided radius and lat/long.
     *
     * @param key
     * @param value
     * @param lat
     * @param lng
     * @param radius
     * @return
     */
    Double getDistanceToNearest(String key, String value, double lat, double lng, int radius) {

        String queryResults;
        int rad = radius;

        //
        // loop to increase search radius if parsing returns null
        //
        for (int i = 0; i < MAX_LOOPS; i++) {
            //
            // Query OSM
            //
            queryResults = helper.getResultsForKeyValue(key, value, lat, lng, rad);
            if (queryResults != null) {
                try {
                    //
                    // Read results
                    //
                    JSONObject json = new JSONObject(queryResults);
                    JSONArray results = json.getJSONArray("elements");

                    //
                    // if it's about nearestCityCentre, needs to be filtered on 
                    // minimum inhabitants
                    //
                    if (CITY_CENTRE.getKey().equals(key) && CITY_CENTRE.getValue().equals(value)) {
                        results = helper.filterMinimumInhabitants(results);
                    }

                    if (results.length() > 0) {
                        //
                        // When found, determine closest by comparing coordinates
                        //
                        return helper.getDistanceToNearestFromJSONArray(lat, lng, results);
                    }
                } catch (JSONException ex) {
                    log.error("Couldn't parse nearest POI result JSON \n{}", ex);
                } catch (IOException ex) {
                    log.error("IOException when requesting nearest POI \n{}", ex);
                }
            }
            //
            // Increase radius and go again if result was empty
            //
            rad *= RADIUS_MULTIPLIER;
        }

        return null;
    }

    Double getDensity(String key, String value, double lat, double lng, int radius) {
        String queryResults;
        int rad = radius;

        //
        // loop to increase search radius if parsing returns null
        //
        for (int i = 0; i < MAX_LOOPS; i++) {
            //
            // Query OSM
            //
            queryResults = helper.getResultsForKeyValue(key, value, lat, lng, rad);
            if (queryResults != null) {
                try {
                    //
                    // Read results
                    //
                    JSONObject json = new JSONObject(queryResults);
                    JSONArray results = json.getJSONArray("elements");

                    //
                    // check if enough are found, or at last iteration
                    //
                    if (results.length() >= MINIMUM_DENSITY_RESULTS || i == MAX_LOOPS - 1 && results.length() > 0) {
                        //
                        // When found, determine furthest by comparing coordinates
                        //
                        double distanceFurthest = helper.getFurthestDistanceFromJSONArray(lat, lng, results);

                        //
                        // determine density (per km)
                        //
                        double area = Math.PI * Math.pow((double) (distanceFurthest / 1000), 2);
                        double density = (double) results.length() / area;
                        return density;
                    }
                } catch (JSONException ex) {
                    log.error("Couldn't parse nearest POI result JSON \n{}", ex);
                } catch (IOException ex) {
                    log.error("IOException when requesting nearest POI \n{}", ex);
                }
            }
            //
            // Increase radius and go again if result was empty
            //
            rad *= RADIUS_MULTIPLIER;
        }

        //
        // when nothing found, generate random but small density
        //
        log.warn("Generating random density for key {} around point {},{}", key, lat, lng);
        return random.nextDouble() / (Math.PI * Math.pow((double) (rad / 1000), 2));

    }

    /**
     * Handle the request for osm data event
     *
     * Condition: giata info should be available
     *
     * request data from osm based on lat/lng
     *
     * @param event
     */
//    @EventListener(condition = "#event.giataIdAvailable and (!#event.osmUpToDate or #event.forceUpdate)")
    @Override
    @RabbitHandler
    public void handleRequestDataEvent(String message) throws IOException {
        RequestDataEvent event = mapper.readValue(message, RequestDataEvent.class);
        log.info("Handling OSM UpdateEvent {}", event);

        //
        // Fetch accommodation by asking service politely
        //
        AccommodationDTO accommodationDTO = accommodationService.findById(event.getAccommodationId()).orElseThrow(() -> {
            return new IllegalStateException("Accommodation not found!");
        });

        //
        // Start filling process when a set of coordinates is available and
        // dto has surroundings associated.
        //
        //FIXME giata id available doesnt imply the lat and long are also filled, add to listener condition when required.
        if (accommodationDTO.getImputedLat() == null
                && accommodationDTO.getImputedLng() == null
                && accommodationDTO.getLat() == null
                && accommodationDTO.getLng() == null) {
            log.warn("No coordinates available for accommodation with id {}, not running osm enrichment", accommodationDTO.getId());
        } else if (accommodationDTO.getSurrounding() == null) {
            log.warn("No Surrounding entity associated for accommodation with id {}, not running osm enrichment", accommodationDTO.getId());
        } else {

            accommodationDTO = fillSurroundings(accommodationDTO);
            accommodationService.saveAccommodation(accommodationDTO);
        }

    }

}
