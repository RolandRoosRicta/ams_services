/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.pyton.elements;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 *
 * @author Timon Veenstra [timon at corizon.nl]
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Country {

    @JacksonXmlProperty(localName = "ID", isAttribute = true)
    private long id;

    @JacksonXmlText
    private String name;
}
