/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.pyton;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import nl.bebr.xdat.travel.products.enrich.pyton.request.WaverunnerRequestMessage;
import nl.bebr.xdat.travel.products.enrich.pyton.response.general.WaverunnerResponseMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
@Component
public class PytonHelper {
    
    private static final Integer AMOUNT_ADULTS = 2;

    private static final Logger log = LoggerFactory.getLogger(PytonHelper.class);

    Date yesterday() {
        return Date.valueOf(LocalDate.now().minus(1, ChronoUnit.DAYS));
    }

    @Value("${bxd.travel.products.enrich.pyton.url}")
    private String url;
    @Value("${bxd.travel.products.enrich.pyton.user}")
    private String waverunnerUser;
    @Value("${bxd.travel.products.enrich.pyton.password}")
    private String waverunnerPassword;

    public String removeHTML(String xml) {
        return xml.replaceAll("(\\<br[ ]*\\/\\>)", "");
    }

    Double parseStarRating(String starrating) {
        return starrating == null || "".equals(starrating) || "".equals(starrating.replaceAll("\\D+", "")) ? null : Double.parseDouble(starrating.replaceAll("\\D+", ""));
    }

    /**
     * Fetch detail info from waverunner api for accommodation with provided
     * pyton id.
     *
     * @param pytonId
     * @return
     */
    public WaverunnerResponseMessage getAccommodationDetails(int pytonId) {

        //
        // Build request message
        //
        WaverunnerRequestMessage requestMessage = WaverunnerRequestMessage.buildGetAccommodationDetails(pytonId, waverunnerUser, waverunnerPassword);

        //
        // Send and return
        //
        return sendRequest(requestMessage);
    }

    public WaverunnerResponseMessage getPriceOptionsInitial(int pytonId) {

        //
        // Build request message
        //
        WaverunnerRequestMessage requestMessage = WaverunnerRequestMessage.buildGetPriceOptionsInitial(pytonId, AMOUNT_ADULTS, waverunnerUser, waverunnerPassword);

        //
        // Send and return
        //
        return sendRequest(requestMessage);
    }

    public WaverunnerResponseMessage getPriceOptionsPerDuration(int pytonId, long durationTypeId) {

        //
        // Build request message
        //
        WaverunnerRequestMessage requestMessage = WaverunnerRequestMessage.buildGetPriceOptionsPerDurationType(pytonId, AMOUNT_ADULTS, durationTypeId, waverunnerUser, waverunnerPassword);

        //
        // Send and return
        //
        return sendRequest(requestMessage);
    }

    /**
     * send provided request message to waverunner API and give back reponse.
     *
     * @param requestMessage
     * @return
     */
    WaverunnerResponseMessage sendRequest(WaverunnerRequestMessage requestMessage) {
        WaverunnerResponseMessage result = null;
        //
        // Set up formatting
        //
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        XmlMapper mapper = new XmlMapper();
        mapper.setDateFormat(df);
        mapper.enable(SerializationFeature.INDENT_OUTPUT);

        try {
            //
            // Build request entity
            //
            String requestMessageString = mapper.writeValueAsString(requestMessage);
            log.debug("PYTON UPDATED REQUEST: \n{}", requestMessageString);

            //
            // Set up template and headers
            //
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_XML);
            HttpEntity<String> request = new HttpEntity<>(requestMessageString, headers);

            //
            // Send request
            //
            ResponseEntity<String> response = restTemplate.postForEntity(url, request, String.class);
            String responseString = removeHTML(response.getBody());
            result = mapper.readValue(responseString, WaverunnerResponseMessage.class);

            log.debug("PYTON UPDATED RESPONSE: \n{}", response);

        } catch (JsonProcessingException ex) {
            log.error("Error posting to pyton", ex);
        } catch (IOException ex) {
            log.error("Error mapping WaverunnerResponseMessage", ex);
        }

        return result;
    }
}
