/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.pyton.response.general;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Timon Veenstra [timon at corizon.nl]
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResponseInfo {

    @JacksonXmlProperty(localName = "ModuleID")
    String moduleId;
    @JacksonXmlProperty(localName = "SessionID")
    String sessionId;
    @JacksonXmlProperty(localName = "RequestID")
    String requestId;
    @JacksonXmlProperty(localName = "ResponseID")
    String responseId;
    @JacksonXmlProperty(localName = "ResponseStatus")
    String responseStatus;
}
