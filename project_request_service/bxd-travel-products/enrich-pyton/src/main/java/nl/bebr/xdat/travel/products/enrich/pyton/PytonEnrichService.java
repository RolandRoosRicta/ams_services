/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.pyton;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;
import net.javacrumbs.shedlock.core.SchedulerLock;
import static nl.bebr.xdat.travel.products.api.Accommodation.Source.PYTON;
import static nl.bebr.xdat.travel.products.api.Accommodation.SourceStatus.UP_TO_DATE;
import nl.bebr.xdat.travel.products.api.AccommodationDTO;
import nl.bebr.xdat.travel.products.api.AccommodationService;
import nl.bebr.xdat.travel.products.api.EnrichService;
import nl.bebr.xdat.travel.products.api.Messaging;
import nl.bebr.xdat.travel.products.api.events.RequestDataEvent;
import nl.bebr.xdat.travel.products.api.events.SourceUpdateAvailableEvent;
import nl.bebr.xdat.travel.products.enrich.pyton.request.WaverunnerRequestMessage;
import nl.bebr.xdat.travel.products.enrich.pyton.response.details.ResponseAccommodationDetails;
import nl.bebr.xdat.travel.products.enrich.pyton.response.details.ResponseAccommodationWaverunner;
import nl.bebr.xdat.travel.products.enrich.pyton.response.error.ResponseErrorMessage;
import nl.bebr.xdat.travel.products.enrich.pyton.response.changed.ResponseMediaCacheChanged;
import nl.bebr.xdat.travel.products.enrich.pyton.response.general.WaverunnerResponseMessage;
import org.modelmapper.ModelMapper;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author Timon Veenstra [tveenstra@bebr.nl]
 */
@Component
@RabbitListener(queues = Messaging.QUEUE_ENRICH_PYTON_REQUEST)
public class PytonEnrichService implements EnrichService {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(PytonEnrichService.class);

    @Autowired
    private ObjectMapper mapper;

    @Value("${bxd.travel.products.enrich.pyton.user}")
    private String waverunnerUser;
    @Value("${bxd.travel.products.enrich.pyton.password}")
    private String waverunnerPassword;

    private final ModelMapper modelMapper;

    @Autowired
    private AccommodationService accommodationService;

    @Autowired
    private PytonHelper helper;

    public PytonEnrichService(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
        this.modelMapper.createTypeMap(ResponseAccommodationWaverunner.class, AccommodationDTO.class).addMappings(mapper -> {
            mapper.map(ResponseAccommodationWaverunner::getRegion, AccommodationDTO::setRegionName);
        });
    }

    /**
     * Fetch list of pyton id's that have changed since provided date.
     *
     * @param date
     * @return
     */
    public List<Integer> updatedSince(Date date) {
        List<Integer> result = new ArrayList<>();

        //
        // Build request message and send
        //
        WaverunnerRequestMessage requestMessage = WaverunnerRequestMessage.buildChangedSince(date, waverunnerUser, waverunnerPassword);
        WaverunnerResponseMessage response = helper.sendRequest(requestMessage);

        //
        // add to list when succesfull 
        //
        if (response != null) {
            if ("success".equals(response.getControl().getResponseInfo().getResponseStatus())) {
                result.addAll(((ResponseMediaCacheChanged) response.getResponseDetails()).getAccommodations());
            } else {
                log.error("Pyton responded with an error");
                log.debug("raw message {}", response.toString());
                log.error("message: {}", ((ResponseErrorMessage) response.getResponseDetails()).getMessage());
            }
        } else {
            log.error("no response from pyton");
            throw new IllegalStateException();
        }

        return result;
    }

    /**
     * Process incoming RequestDataEvent, but only when pyton id is available
     * and either pyton status != UP_TO_DATE or the update's forced.
     *
     * @param message
     */
//    @EventListener(condition = "#event.pytonIdAvailable and (!#event.pytonUpToDate or #event.forceUpdate)")
    @RabbitHandler
    @Override
    public void handleRequestDataEvent(String message) throws IOException {
        log.info("handle {}", message);
        RequestDataEvent event = mapper.readValue(message, RequestDataEvent.class);
        log.debug("Handling Pyton UpdateEvent {}", event);

        //
        // Fetch DTO from service
        //
        AccommodationDTO accommodationDTO = accommodationService.findById(event.getAccommodationId()).orElseThrow(() -> {
            return new IllegalStateException("Accommodation not found!");
        });

        //
        // Request details
        //
        Long pytonId = accommodationDTO.getSourceId(PYTON);
        WaverunnerResponseMessage response = helper.getAccommodationDetails(pytonId.intValue());

        if (response != null) {
            if (response.getResponseDetails() instanceof ResponseErrorMessage) {
                log.error("Waverunner return error for pyton id {}: {}", pytonId, (ResponseErrorMessage) response.getResponseDetails());
            } else {

                ResponseAccommodationWaverunner waverunnerResponse = ((ResponseAccommodationDetails) response.getResponseDetails()).getWaverunner();

                if (waverunnerResponse != null) {
                    //
                    // when not returning error, set LSPA values to DTO
                    //
                    log.debug("accommodation response : {}", waverunnerResponse);
                    accommodationDTO.setName(waverunnerResponse.getAccommodation() == null ? null : waverunnerResponse.getAccommodation().getName());
                    accommodationDTO.setLocationName(waverunnerResponse.getPlace() == null ? null : waverunnerResponse.getPlace().getName());
                    accommodationDTO.setLocationId(waverunnerResponse.getPlace() == null ? null : (int) waverunnerResponse.getPlace().getId());
                    accommodationDTO.setRegionName(waverunnerResponse.getRegion() == null ? null : waverunnerResponse.getRegion().getName());
                    accommodationDTO.setRegionId(waverunnerResponse.getRegion() == null ? null : (int) waverunnerResponse.getRegion().getId());
                    accommodationDTO.setCountryName(waverunnerResponse.getCountry() == null ? null : waverunnerResponse.getCountry().getName());
                    accommodationDTO.setCountryId(waverunnerResponse.getCountry() == null ? null : (int) waverunnerResponse.getCountry().getId());
                    accommodationDTO.setType(waverunnerResponse.getAccommodationType() == null ? null : waverunnerResponse.getAccommodationType().getType());
                    accommodationDTO.setPytonRating(waverunnerResponse.getClassification() == null ? null : helper.parseStarRating(waverunnerResponse.getClassification().getStarCount()));
                }

            }
        } else {
            log.error("Response was null for pytonId {}!", pytonId);
        }

        //
        // Update pyton status & publish
        //
        accommodationDTO.setPytonStatus(UP_TO_DATE);
        accommodationService.saveAccommodation(accommodationDTO);
    }

    @Scheduled(cron = "${bxd.travel.products.enrich.pyton.cron}")
    @SchedulerLock(name = "PytonClient_checkForUpdates")
//    @Scheduled(fixedDelay = 100000000)
    public void checkForUpdates() throws ParseException {
        log.info("Checking for pyton accommodation updates");
        List<Integer> updated = updatedSince(helper.yesterday());
        log.debug("found {} updates for accommodations", updated.size());

        updated.stream().forEach((t) -> {
//            publisher.publishEvent(new SourceUpdateAvailableEvent(PYTON, t.longValue()));
        });
    }

}
