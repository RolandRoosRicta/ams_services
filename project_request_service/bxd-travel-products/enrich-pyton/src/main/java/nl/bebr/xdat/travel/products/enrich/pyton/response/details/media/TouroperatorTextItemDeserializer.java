/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.pyton.response.details.media;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.IOException;
import lombok.NoArgsConstructor;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
@NoArgsConstructor
public class TouroperatorTextItemDeserializer extends JsonDeserializer<TouroperatorTextItem> {

    @Override
    public TouroperatorTextItem deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException {

        //
        // Added custom deserializer to TouroperatorTextItem because the xmls
        // contained stuff like : <br /><![CDATA[Ligging]]></Header>
        // where the <br /> fucks up the whole parsing.
        //
        TouroperatorTextItem item = new TouroperatorTextItem();
        TreeNode tn = parser.readValueAsTree();

        if (tn != null) {

            if (tn.get("Season") != null) {
                item.setSeason(tn.get("Season").toString() == null ? null : tn.get("Season").toString().replaceAll("\"", ""));
            }

            if (tn.get("Order") != null) {
                item.setOrder(tn.get("Order").toString() == null ? null : Integer.parseInt(tn.get("Order").toString().replaceAll("\"", "")));
            }

            if (tn.get("Header") != null) {
                item.setHeader(tn.get("Header").toString() == null ? null : tn.get("Header").toString().replaceAll("\"", ""));
            }

            if (tn.get("Content") != null) {
                item.setContent(tn.get("Content").toString() == null ? null : tn.get("Content").toString().replaceAll("\"", ""));
            }

        }

        return new TouroperatorTextItem();

    }

}
