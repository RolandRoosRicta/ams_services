/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.pyton.response.general;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import nl.bebr.xdat.travel.products.enrich.pyton.response.details.ResponseAccommodationDetails;
import nl.bebr.xdat.travel.products.enrich.pyton.response.error.ResponseErrorMessage;
import nl.bebr.xdat.travel.products.enrich.pyton.response.changed.ResponseMediaCacheChanged;
import static com.fasterxml.jackson.annotation.JsonTypeInfo.As.WRAPPER_OBJECT;
import static com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME;
import nl.bebr.xdat.travel.products.enrich.pyton.response.price.option.ResponsePriceOptionDetails;

/**
 *
 * @author Timon Veenstra [timon at corizon.nl]
 */
@JsonTypeInfo(use = NAME, include = WRAPPER_OBJECT)
@JsonSubTypes({
    @JsonSubTypes.Type(value = ResponseMediaCacheChanged.class, name = "MediaCacheChanged"),
    @JsonSubTypes.Type(value = ResponseErrorMessage.class, name = "ErrorMessage"),
    @JsonSubTypes.Type(value = ResponseAccommodationDetails.class, name = "AccommodationDetails"),
    @JsonSubTypes.Type(value = ResponsePriceOptionDetails.class, name = "PriceOptions")
})
public interface ResponseDetails {

}
