/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.pyton.elements;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Builder;
import lombok.Data;

/**
 *
 * @author Timon Veenstra [timon at corizon.nl]
 */
@Data
@Builder
public class Persons {

    @JacksonXmlProperty(localName = "Adults")
    private Integer adults;
    @JacksonXmlProperty(localName = "Children")
    private Integer children;
    @JacksonXmlProperty(localName = "Babies")
    private Integer babies;

}
