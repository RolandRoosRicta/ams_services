/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.pyton.response.price.option;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import nl.bebr.xdat.travel.products.api.AccommodationAvailabilityDTO;
import nl.bebr.xdat.travel.products.enrich.pyton.elements.DepartureDate;
import nl.bebr.xdat.travel.products.enrich.pyton.elements.Touroperator;
import nl.bebr.xdat.travel.products.enrich.pyton.response.general.ResponseDetails;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResponsePriceOptionDetails implements ResponseDetails {

    @JacksonXmlProperty(localName = "DepartureDate")
    @JacksonXmlElementWrapper(localName = "DepartureDates")
    List<DepartureDate> departureDates;

    @JacksonXmlProperty(localName = "DurationType")
    @JacksonXmlElementWrapper(localName = "DurationTypes")
    List<DurationType> durationTypes;

    @JacksonXmlProperty(localName = "BoardType")
    @JacksonXmlElementWrapper(localName = "BoardTypes")
    List<BoardType> boardTypes;

    @JacksonXmlProperty(localName = "DeparturePoint")
    @JacksonXmlElementWrapper(localName = "DeparturePoints")
    List<DeparturePoint> departurePoints;

    @JacksonXmlProperty(localName = "TransportType")
    @JacksonXmlElementWrapper(localName = "TransportTypes")
    List<TransportType> transportTypes;

    @JacksonXmlProperty(localName = "UnitType")
    @JacksonXmlElementWrapper(localName = "UnitTypes")
    List<UnitType> unitTypes;

    @JacksonXmlProperty(localName = "Touroperator")
    @JacksonXmlElementWrapper(localName = "Touroperators")
    List<Touroperator> touroperators;

    @JsonIgnore
    private static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");

    @JsonIgnore
    /**
     * Knocks this object flat into separate AccommodationAvailabilityDTO's, one
     * for each DepartureDate.
     */
    public List<AccommodationAvailabilityDTO> parseAvailabilities() {
        List<AccommodationAvailabilityDTO> availabilities = new ArrayList<>();

        //
        // Loop departureDate
        //
        if (getDepartureDates() != null) {

            getDepartureDates().forEach(dd -> {

                //
                // Split it into readable strings
                //
                String[] parts = dd.getId().split("\\.");
                Date startDate = Date.valueOf(LocalDate.parse(parts[0], dtf));
                Date endDate = Date.valueOf(LocalDate.parse(parts[1], dtf));

                availabilities.add(
                        //
                        // TEMP:FIX
                        // Greenplum doesn't like arrays, that's why we store them
                        // as comma separated strings, lines underneath concat
                        // given lists into exactly that when not null or empty
                        //
                        AccommodationAvailabilityDTO.builder()
                                .startDate(startDate)
                                .endDate(endDate)
                                .durationType(
                                        getDurationTypes() == null || getDurationTypes().isEmpty() ? null
                                        : getDurationTypes().get(0).getValue())
                                .boardTypes(
                                        getBoardTypes() == null || getBoardTypes().isEmpty() ? null
                                        : String.join(",", getBoardTypes().stream().map(bt -> bt.getValue()).collect(Collectors.toList())))
                                .departurePoints(
                                        getDeparturePoints() == null || getDeparturePoints().isEmpty() ? null
                                        : String.join(",", getDeparturePoints().stream().map(dp -> dp.getValue()).collect(Collectors.toList())))
                                .unitTypes(
                                        getUnitTypes() == null || getUnitTypes().isEmpty() ? null
                                        : String.join(",", getUnitTypes().stream().map(ut -> ut.getValue()).collect(Collectors.toList())))
                                .touroperators(
                                        getTouroperators() == null || getTouroperators().isEmpty() ? null
                                        : String.join(",", getTouroperators().stream().map(to -> to.getName()).collect(Collectors.toList())))
                                .transportTypes(
                                        getTransportTypes() == null || getTransportTypes().isEmpty() ? null
                                        : String.join(",", getTransportTypes().stream().map(tt -> tt.getValue()).collect(Collectors.toList())))
                                .build()
                );
            });
        } else {
            throw new IllegalArgumentException("Can't parse AccommodationAvailabilityDTO's from ResponsePriceOptionDetails if departureDates are not filled!");
        }

        return availabilities;
    }

}
