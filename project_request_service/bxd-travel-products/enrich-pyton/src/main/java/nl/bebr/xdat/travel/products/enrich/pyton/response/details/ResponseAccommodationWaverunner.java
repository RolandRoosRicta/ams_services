/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.pyton.response.details;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import nl.bebr.xdat.travel.products.enrich.pyton.elements.Country;
import nl.bebr.xdat.travel.products.enrich.pyton.elements.Place;
import nl.bebr.xdat.travel.products.enrich.pyton.elements.Region;

/**
 *
 * @author Kilian Veenstra <ktveenstra@bebr.nl>
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResponseAccommodationWaverunner {

    @JacksonXmlProperty(localName = "Country")
    Country country;

    @JacksonXmlProperty(localName = "Region")
    Region region;

    @JacksonXmlProperty(localName = "Place")
    Place place;

    @JacksonXmlProperty(localName = "Accommodation")
    ResponseAccommodation accommodation;

    @JacksonXmlProperty(localName = "AccommodationType")
    ResponseAccommodationType accommodationType;

    @JacksonXmlProperty(localName = "Classification")
    Classification classification;
}
