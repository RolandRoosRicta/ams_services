/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.pyton.elements;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Global {

    public static final String SORT_FIELD_PRICE = "Price";
    public static final String SORT_ORDER_ASC = "ASC";
    public static final String SORT_ORDER_DESC = "DESC";
    public static final Integer DEFAULT_PAGE_SIZE = 10;
    public static final Integer DEFAULT_CURRENT_PAGE = 10;

    //
    // these properties have default values, that's why they're all marked
    // @Builder.Default
    //
    @JacksonXmlProperty(localName = "PageSize")
    @Builder.Default
    Integer pageSize = DEFAULT_PAGE_SIZE;
    @JacksonXmlProperty(localName = "CurrentPage")
    @Builder.Default
    Integer currentPage = DEFAULT_CURRENT_PAGE;
    @JacksonXmlProperty(localName = "SortField")
    @Builder.Default
    String sortField = SORT_FIELD_PRICE;
    @JacksonXmlProperty(localName = "SortOrder")
    @Builder.Default
    String sortOrder = SORT_ORDER_ASC;

}
