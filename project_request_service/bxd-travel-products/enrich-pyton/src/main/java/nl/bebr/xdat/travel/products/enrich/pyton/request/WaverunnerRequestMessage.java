/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.pyton.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.UUID;
import lombok.Builder;
import lombok.Data;
import nl.bebr.xdat.travel.products.enrich.pyton.elements.Accommodation;
import nl.bebr.xdat.travel.products.enrich.pyton.elements.DepartureDate;
import nl.bebr.xdat.travel.products.enrich.pyton.elements.Global;
import nl.bebr.xdat.travel.products.enrich.pyton.elements.Persons;
import static nl.bebr.xdat.travel.products.enrich.pyton.request.RequestInfo.MODULE_ID_SUPPLY;
import static nl.bebr.xdat.travel.products.enrich.pyton.request.RequestInfo.REQUEST_ID_GET_ACCOMMODATION_DETAILS;
import static nl.bebr.xdat.travel.products.enrich.pyton.request.RequestInfo.REQUEST_ID_MEDIA_CACHE_CHANGED;
import static nl.bebr.xdat.travel.products.enrich.pyton.request.RequestInfo.REQUEST_ID_PRICE_OPTIONS;
import static nl.bebr.xdat.travel.products.enrich.pyton.request.RequestInfo.REQUEST_ID_SEARCH;

/**
 *
 * @author Timon Veenstra [timon at corizon.nl]
 */
@Data
@Builder
@JacksonXmlRootElement(localName = "WaverunnerMessage")
public class WaverunnerRequestMessage {

    @JacksonXmlProperty(localName = "Control")
    private RequestControl control;
    @JacksonXmlProperty(localName = "RequestDetails")
    private RequestDetails requestDetails;

    @JsonIgnore
    public static WaverunnerRequestMessage buildChangedSince(Date since, String user, String password) {
        return WaverunnerRequestMessage.builder()
                .control(RequestControl.builder()
                        .sessionId(UUID.randomUUID().toString())
                        .senderInfo(SenderInfo.builder()
                                .login(user)
                                .password(password)
                                .build())
                        .requestInfo(RequestInfo.builder()
                                .moduleId(MODULE_ID_SUPPLY)
                                .requestId(REQUEST_ID_MEDIA_CACHE_CHANGED)
                                .build())
                        .build())
                .requestDetails(RequestMediaCacheChanged.builder()
                        .mediaCacheChanged(MediaCacheChanged.builder()
                                .lastCheckDate(since)
                                .build())
                        .build())
                .build();
    }

    @JsonIgnore
    public static WaverunnerRequestMessage buildGetAccommodationDetails(int pytonId, String user, String password) {
        return WaverunnerRequestMessage.builder()
                .control(RequestControl.builder()
                        .sessionId(UUID.randomUUID().toString())
                        .senderInfo(SenderInfo.builder()
                                .login(user)
                                .password(password)
                                .build())
                        .requestInfo(RequestInfo.builder()
                                .moduleId(MODULE_ID_SUPPLY)
                                .requestId(REQUEST_ID_GET_ACCOMMODATION_DETAILS)
                                .build())
                        .build())
                .requestDetails(RequestAccommodationDetails.builder()
                        .accommodationDetails(AccommodationDetails.builder()
                                .accommodation(Accommodation.builder().id(pytonId).build())
                                .build())
                        .build())
                .build();
    }

    @JsonIgnore
    public static WaverunnerRequestMessage buildGetPriceOptionsInitial(int pytonId, int amountAdults, String user, String password) {
        return WaverunnerRequestMessage.builder()
                .control(RequestControl.builder()
                        .sessionId(UUID.randomUUID().toString())
                        .senderInfo(SenderInfo.builder()
                                .login(user)
                                .password(password)
                                .build())
                        .requestInfo(RequestInfo.builder()
                                .moduleId(MODULE_ID_SUPPLY)
                                .requestId(REQUEST_ID_PRICE_OPTIONS)
                                .build())
                        .build())
                .requestDetails(RequestPriceOptions.builder()
                        .priceOption(PriceOption.builder()
                                .accommodation(Accommodation.builder().id(pytonId).build())
                                .persons(Persons.builder()
                                        .adults(amountAdults)
                                        .build())
                                .build())
                        .build())
                .build();
    }

    @JsonIgnore
    public static WaverunnerRequestMessage buildGetPriceOptionsPerDurationType(int pytonId, int amountAdults, long durationTypeId, String user, String password) {
        return WaverunnerRequestMessage.builder()
                .control(RequestControl.builder()
                        .sessionId(UUID.randomUUID().toString())
                        .senderInfo(SenderInfo.builder()
                                .login(user)
                                .password(password)
                                .build())
                        .requestInfo(RequestInfo.builder()
                                .moduleId(MODULE_ID_SUPPLY)
                                .requestId(REQUEST_ID_PRICE_OPTIONS)
                                .build())
                        .build())
                .requestDetails(RequestPriceOptions.builder()
                        .priceOption(PriceOption.builder()
                                .accommodation(Accommodation.builder().id(pytonId).build())
                                .persons(Persons.builder()
                                        .adults(amountAdults)
                                        .build())
                                .durationTypes(Collections.singletonList(RequestDurationType.builder()
                                        .id(durationTypeId)
                                        .build()))
                                .build())
                        .build())
                .build();
    }

    @JsonIgnore
    public static WaverunnerRequestMessage buildGetSearch(int pytonId, Date departure, int amountAdults, String user, String password) {
        return WaverunnerRequestMessage.builder()
                .control(RequestControl.builder()
                        .sessionId(UUID.randomUUID().toString())
                        .senderInfo(SenderInfo.builder()
                                .login(user)
                                .password(password)
                                .build())
                        .requestInfo(RequestInfo.builder()
                                .moduleId(MODULE_ID_SUPPLY)
                                .requestId(REQUEST_ID_SEARCH)
                                .build())
                        .build())
                .requestDetails(RequestSearch.builder()
                        .searchFilters(Search.builder()
                                .global(Global.builder().build())
                                .accommodations(Collections.singletonList(Accommodation.builder().id(pytonId).build()))
                                .departureDate(DepartureDate.formatDate(departure))
                                .persons(Persons.builder()
                                        .adults(amountAdults)
                                        .build())
                                .build())
                        .build())
                .build();
    }

}
