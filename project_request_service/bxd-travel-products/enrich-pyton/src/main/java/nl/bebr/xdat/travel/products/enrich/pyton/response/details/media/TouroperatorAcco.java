/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.pyton.response.details.media;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Kilian Veenstra <ktveenstra@bebr.nl>
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TouroperatorAcco {

    @JacksonXmlProperty(localName = "TouropID", isAttribute = true)
    private String touroperatorId;

    @JacksonXmlProperty(localName = "ID", isAttribute = true)
    private String id;

    @JacksonXmlProperty(localName = "AccoName")
    private String accoName;
    @JacksonXmlProperty(localName = "AccoType")
    private String accoType;
    @JacksonXmlProperty(localName = "Classification")
    private String classification;
    @JacksonXmlProperty(localName = "Country")
    private String country;
    @JacksonXmlProperty(localName = "Region")
    private String region;
    @JacksonXmlProperty(localName = "Place")
    private String place;

    @JacksonXmlProperty(localName = "Images")
    private List<TouroperatorImage> images;

    @JacksonXmlProperty(localName = "Texts")
    private List<TouroperatorText> texts;

}
