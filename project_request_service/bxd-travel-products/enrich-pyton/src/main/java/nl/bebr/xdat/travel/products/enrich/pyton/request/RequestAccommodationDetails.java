/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.pyton.request;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Builder;
import lombok.Data;

/**
 *
 * @author Timon Veenstra [timon at corizon.nl]
 */
@Builder
@Data
public class RequestAccommodationDetails implements RequestDetails {

    @JacksonXmlProperty(localName = "AccommodationDetails")
    AccommodationDetails accommodationDetails;
}
