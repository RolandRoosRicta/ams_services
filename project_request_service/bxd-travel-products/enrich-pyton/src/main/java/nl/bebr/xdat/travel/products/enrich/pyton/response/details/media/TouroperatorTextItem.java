/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.pyton.response.details.media;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Kilian Veenstra <ktveenstra@bebr.nl>
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TouroperatorTextItem {

    @JacksonXmlProperty(localName = "Season", isAttribute = true)
    private String season;

    @JacksonXmlProperty(localName = "Order", isAttribute = true)
    private Integer order;

    @JacksonXmlProperty(localName = "Header")
    private String header;

    @JacksonXmlProperty(localName = "Content")
    private String content;

}
