/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.pyton.response.details;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import nl.bebr.xdat.travel.products.enrich.pyton.response.details.media.ResponseAccommodationMedia;
import nl.bebr.xdat.travel.products.enrich.pyton.response.general.ResponseDetails;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Kilian Veenstra <ktveenstra@bebr.nl>
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResponseAccommodationDetails implements ResponseDetails {

    @JacksonXmlProperty(localName = "Waverunner")
    ResponseAccommodationWaverunner waverunner;

    @JacksonXmlProperty(localName = "Media")
    ResponseAccommodationMedia media;

}
