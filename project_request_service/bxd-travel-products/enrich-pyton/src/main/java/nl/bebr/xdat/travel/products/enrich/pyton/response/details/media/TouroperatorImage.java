/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.pyton.response.details.media;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Kilian Veenstra <ktveenstra@bebr.nl>
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TouroperatorImage {

    @JacksonXmlProperty(localName = "ImgType", isAttribute = true)
    private String imgType;

    @JacksonXmlProperty(localName = "Season", isAttribute = true)
    private String season;

    @JacksonXmlProperty(localName = "Order", isAttribute = true)
    private Integer order;

    @JacksonXmlProperty(localName = "Location")
    private String location;

    @JacksonXmlProperty(localName = "Description")
    private String description;

}
