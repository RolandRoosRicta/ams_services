/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.pyton;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import net.javacrumbs.shedlock.core.SchedulerLock;
import static nl.bebr.xdat.travel.products.api.Accommodation.Source.PYTON;
import nl.bebr.xdat.travel.products.api.AccommodationAvailabilityDTO;
import nl.bebr.xdat.travel.products.api.AccommodationDTO;
import nl.bebr.xdat.travel.products.api.AccommodationService;
import nl.bebr.xdat.travel.products.enrich.pyton.response.error.ResponseErrorMessage;
import nl.bebr.xdat.travel.products.enrich.pyton.response.general.WaverunnerResponseMessage;
import nl.bebr.xdat.travel.products.enrich.pyton.response.price.option.ResponsePriceOptionDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
@Component
public class PytonAvailabilityService {

    private static final Logger log = LoggerFactory.getLogger(PytonAvailabilityService.class);

    @Autowired
    ObjectMapper mapper;

    @Autowired
    private AccommodationService accommodationService;

    @Autowired
    private PytonHelper helper;

    //
    // Scheduled service to fetch availability for accommodations
    //
    @Scheduled(cron = "${bxd.travel.products.enrich.pyton.availabilityCron}")
    @SchedulerLock(name = "PytonClient_checkAvailability")
    public void handleRequestAvailabilityEvent() throws IOException {
        log.debug("Handling Scheduled Pyton AvailabilityEvent");
        //
        // Fetch DTO from service
        //
        AccommodationDTO accommodationDTO = accommodationService.findNextToGetAvailability().orElseThrow(() -> {
            return new IllegalStateException("Accommodation not found!");
        });
        log.info("Getting availability for Accommodation with id {}", accommodationDTO.getId());

        //
        // Request details
        //
        Long pytonId = accommodationDTO.getSourceId(PYTON);
        WaverunnerResponseMessage initialResponse = helper.getPriceOptionsInitial(pytonId.intValue());

        if (initialResponse != null) {
            if (initialResponse.getResponseDetails() instanceof ResponseErrorMessage) {
                log.error("Waverunner return error for pyton id {}: {}", pytonId, (ResponseErrorMessage) initialResponse.getResponseDetails());

                //
                // In case of error, we need to enter a dummy entry into the 
                // availabilities to prevent a loop on the same accommodation
                //
                if (accommodationDTO.getAvailabilities() == null || accommodationDTO.getAvailabilities().isEmpty()) {
                    accommodationDTO.setAvailabilities(Collections.singletonList(AccommodationAvailabilityDTO.builder().build()));
                }

            } else {

                ResponsePriceOptionDetails initialWaverunnerResponse = (ResponsePriceOptionDetails) initialResponse.getResponseDetails();

                if (initialWaverunnerResponse != null) {
                    //
                    // Ok, the way to obtain the actual availability information
                    // is to query this in two steps, first to get everything,
                    // so all the dates, unittypes etc.
                    // we need to know the dates per duration type so we then 
                    // query on all those variables individually.
                    // yaaaay
                    //
                    log.debug("Initial availability response : {}", initialWaverunnerResponse);

                    List<AccommodationAvailabilityDTO> availabilities = new ArrayList<>();

                    if (initialWaverunnerResponse.getDurationTypes() != null) {

                        //
                        // loop through DurationTypes and send new request for each
                        //
                        initialWaverunnerResponse.getDurationTypes().forEach(dt -> {
                            WaverunnerResponseMessage secondaryResponse = helper.getPriceOptionsPerDuration(pytonId.intValue(), dt.getId());
                            if (initialResponse.getResponseDetails() instanceof ResponseErrorMessage) {
                                log.error("Waverunner return error for pyton id {}: {}", pytonId, (ResponseErrorMessage) initialResponse.getResponseDetails());
                            } else {

                                //
                                // Parse individual 'lines' of AccommodationAvailabilities
                                //
                                ResponsePriceOptionDetails secondaryWaverunnerResponse = (ResponsePriceOptionDetails) secondaryResponse.getResponseDetails();

                                availabilities.addAll(secondaryWaverunnerResponse.parseAvailabilities());

                            }

                        });
                    }
                    //
                    // Attach the new list to the DTO
                    //
                    accommodationDTO.setAvailabilities(availabilities);

                }
            }
        } else {
            log.error("Response was null for pytonId {}!", pytonId);
        }

        accommodationService.saveAccommodation(accommodationDTO);
    }
}
