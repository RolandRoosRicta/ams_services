/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.pyton.elements;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DepartureDate {

    @JacksonXmlProperty(localName = "id", isAttribute = true)
    private String id;
    @JacksonXmlText
    private String value;

    @JsonIgnore
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

    /**
     * Formats date entity into yyyymmdd.yyyymmdd format, with the second being
     * a week later.
     *
     * @param date
     * @return
     */
    @JsonIgnore
    public static String formatDate(Date date) {
        Date weekLater = Date.valueOf(date.toLocalDate().plus(1, ChronoUnit.WEEKS));
        String first = sdf.format(date);
        String second = sdf.format(weekLater);

        return new StringBuilder()
                .append(first)
                .append(".")
                .append(second)
                .toString();
    }
}
