/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.pyton.response.error;

import nl.bebr.xdat.travel.products.enrich.pyton.response.general.ResponseDetails;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Timon Veenstra [timon at corizon.nl]
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResponseErrorMessage implements ResponseDetails {

//    @JacksonXmlProperty(localName = value = "ErrorMessage",required = true)
//    ErrorMessage errorMessage;
    
    @JacksonXmlProperty(localName = "MessageID")
    Integer messageId;
    @JacksonXmlProperty(localName = "MessageType")
    String messageType;
    @JacksonXmlProperty(localName = "Message")
    String message;    
}