/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.pyton.request;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import java.util.List;
import lombok.Builder;
import lombok.Data;
import nl.bebr.xdat.travel.products.enrich.pyton.elements.Accommodation;
import nl.bebr.xdat.travel.products.enrich.pyton.elements.Persons;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
@Data
@Builder
public class PriceOption {

    @JacksonXmlProperty(localName = "Accommodation")
    private Accommodation accommodation;

    @JacksonXmlProperty(localName = "Persons")
    private Persons persons;

    @JacksonXmlProperty(localName = "DurationType")
    @JacksonXmlElementWrapper(localName = "DurationTypes")
    List<RequestDurationType> durationTypes;
}
