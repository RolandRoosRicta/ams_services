/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.pyton;

import net.javacrumbs.shedlock.spring.annotation.EnableSchedulerLock;
import org.modelmapper.ModelMapper;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 *
 * @author Timon Veenstra [timon at corizon.nl]
 */
@SpringBootApplication
@ComponentScan(basePackages = "nl.bebr.xdat.travel.products")
@EntityScan(basePackages = "nl.bebr.xdat.travel.products")
@EnableScheduling
@EnableSchedulerLock(defaultLockAtMostFor = "PT30S")
public class EnrichPytonApplication {


    public static void main(String[] args) {
        new SpringApplicationBuilder(EnrichPytonApplication.class).run(args);//.close();
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }


    
}
