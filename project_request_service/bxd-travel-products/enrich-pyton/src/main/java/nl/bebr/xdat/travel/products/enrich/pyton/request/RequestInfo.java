/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.pyton.request;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Builder;
import lombok.Data;

/**
 *
 * @author Timon Veenstra [timon at corizon.nl]
 */
@Data
@Builder
public class RequestInfo {

    public static final String MODULE_ID_SUPPLY = "Supply";
    public static final String REQUEST_ID_SEARCH_FILTERS = "SearchFilters";// 3.1 With this request it is possible to get the filters for supply.
    public static final String REQUEST_ID_SEARCH = "Search"; // 3.2 With this request it is possible to get a list accommodations based on the given filters
    public static final String REQUEST_ID_PRICES_FOR_ACCOS = "PricesForAccos"; // 3.3 With this request it is possible to get a prices for a list of specified accommomdations. There will be a limit on the list of accommodations of 10 accommodations per request.
    public static final String REQUEST_ID_MEDIA_CACHE_CHANGED = "MediaCacheChanged"; // 5.2 With this request it is possible to a list of accommodations that have changed texts since the last call
    public static final String REQUEST_ID_GET_ACCOMMODATION_DETAILS = "AccommodationDetails"; // 3.5 With this request it is possible to get details for an accommodation
    public static final String REQUEST_ID_PRICE_OPTIONS = "PriceOptions"; // 3.6 With this request it is possible to get a price matrix for specified filters 

    @JacksonXmlProperty(localName = "ModuleID")
    String moduleId;

    @JacksonXmlProperty(localName = "RequestID")
    String requestId;
}
