/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.pyton.response.changed;

import nl.bebr.xdat.travel.products.enrich.pyton.response.general.ResponseDetails;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Timon Veenstra [timon at corizon.nl]
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResponseMediaCacheChanged implements ResponseDetails {

    @JacksonXmlProperty(localName = "Accommodation")
    @JacksonXmlElementWrapper(useWrapping = false)
    List<Integer> accommodations;

}
