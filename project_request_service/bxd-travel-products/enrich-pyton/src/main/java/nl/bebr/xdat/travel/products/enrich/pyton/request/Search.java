/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.pyton.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import java.util.List;
import lombok.Builder;
import lombok.Data;
import nl.bebr.xdat.travel.products.enrich.pyton.elements.Accommodation;
import nl.bebr.xdat.travel.products.enrich.pyton.elements.Global;
import nl.bebr.xdat.travel.products.enrich.pyton.elements.Persons;

/**
 *
 * @author Kilian Veenstra <ktveenstra@bebr.nl>
 */
@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class Search {

    @JacksonXmlProperty(localName = "Global")
    Global global;

    //
    // Format yyyymmdd.yyyymmdd (7 days)
    //
    @JacksonXmlProperty(localName = "DepartureDate")
    String departureDate;

    @JacksonXmlProperty(localName = "Persons")
    Persons persons;

    @JacksonXmlProperty(localName = "Accommodations")
    List<Accommodation> accommodations;

}
