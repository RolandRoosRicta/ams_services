/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.pyton.response.general;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Timon Veenstra [timon at corizon.nl]
 */
@Data
@Builder
@JacksonXmlRootElement(localName = "WaverunnerMessage")
@NoArgsConstructor
@AllArgsConstructor
public class WaverunnerResponseMessage {

    @JacksonXmlProperty(localName = "Control")
    private ResponseControl control;
    @JacksonXmlProperty(localName = "ResponseDetails")
    private ResponseDetails responseDetails;
}
