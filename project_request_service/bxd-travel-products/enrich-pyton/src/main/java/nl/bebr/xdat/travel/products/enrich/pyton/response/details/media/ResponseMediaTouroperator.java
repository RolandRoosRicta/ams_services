/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.pyton.response.details.media;

import nl.bebr.xdat.travel.products.enrich.pyton.elements.Touroperator;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Kilian Veenstra <ktveenstra@bebr.nl>
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResponseMediaTouroperator {

    @JacksonXmlProperty(localName = "Touroperator")
    Touroperator touroperator;

    @JacksonXmlProperty(localName = "Description")
    TouroperatorDescription touroperatorDescription;

}
