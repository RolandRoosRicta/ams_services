/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.pyton.request;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import java.util.Date;
import lombok.Builder;
import lombok.Data;

/**
 *
 * @author Timon Veenstra [timon at corizon.nl]
 */
@Data
@Builder
public class MediaCacheChanged {

    @JacksonXmlProperty(localName = "LastCheckDate")
    Date lastCheckDate;
}
