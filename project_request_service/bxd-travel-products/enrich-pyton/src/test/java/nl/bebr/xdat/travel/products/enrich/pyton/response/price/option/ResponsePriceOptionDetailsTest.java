/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.pyton.response.price.option;

import java.sql.Date;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import nl.bebr.xdat.travel.products.api.AccommodationAvailabilityDTO;
import nl.bebr.xdat.travel.products.enrich.pyton.elements.DepartureDate;
import nl.bebr.xdat.travel.products.enrich.pyton.elements.Touroperator;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
public class ResponsePriceOptionDetailsTest {

    public ResponsePriceOptionDetailsTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of parseAvailabilities method, of class ResponsePriceOptionDetails.
     */
    @Test
    public void testParseAvailabilities() {
        System.out.println("parseAvailabilities");

        ResponsePriceOptionDetails details = ResponsePriceOptionDetails.builder()
                .departureDates(Arrays.asList(
                        new DepartureDate("20190707.20190713", "07 jul - 13 jul"),
                        new DepartureDate("20190714.20190720", "14 jul - 20 jul"),
                        new DepartureDate("20190728.20190803", "28 jul - 03 aug")))
                .durationTypes(Collections.singletonList(DurationType.builder().value("8 dagen").build()))
                .boardTypes(Arrays.asList(
                        new BoardType(1l, "all inclusive"),
                        new BoardType(2l, "bricks for breakfast")))
                .departurePoints(Collections.singletonList(DeparturePoint.builder().value("Meppel").build()))
                .transportTypes(Arrays.asList(
                        new TransportType(61l, "zamboni"),
                        new TransportType(26l, "waterfiets")))
                .unitTypes(Collections.singletonList(UnitType.builder().value("1 kamer").build()))
                .touroperators(Arrays.asList(
                        new Touroperator(123, "tui"),
                        new Touroperator(1231, "corendon")))
                .build();

        List<AccommodationAvailabilityDTO> result = details.parseAvailabilities();

        assertEquals(result.size(), 3);
        assertTrue(result.stream().anyMatch(r -> "2019-07-13".equals(r.getEndDate().toString())));
        assertTrue(result.stream().anyMatch(r -> "2019-07-20".equals(r.getEndDate().toString())));
        assertTrue(result.stream().anyMatch(r -> "2019-08-03".equals(r.getEndDate().toString())));
        assertTrue(result.stream().anyMatch(r -> "2019-07-07".equals(r.getStartDate().toString())));
        assertTrue(result.stream().anyMatch(r -> "2019-07-14".equals(r.getStartDate().toString())));
        assertTrue(result.stream().anyMatch(r -> "2019-07-28".equals(r.getStartDate().toString())));
        AccommodationAvailabilityDTO dto = result.get(0);
        assertEquals(dto.getBoardTypes(), "all inclusive,bricks for breakfast");
        assertEquals(dto.getDeparturePoints(), "Meppel");
        assertEquals(dto.getTouroperators(), "tui,corendon");
        assertEquals(dto.getTransportTypes(), "zamboni,waterfiets");
        assertEquals(dto.getUnitTypes(), "1 kamer");

    }

}
