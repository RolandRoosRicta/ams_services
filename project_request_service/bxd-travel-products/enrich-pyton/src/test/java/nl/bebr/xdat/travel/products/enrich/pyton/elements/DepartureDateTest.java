/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.pyton.elements;

import java.sql.Date;
import java.time.LocalDate;
import java.time.Month;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
public class DepartureDateTest {

    public DepartureDateTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of formatDate method, of class DepartureDate.
     */
    @Test
    public void testFormatDate() {
        System.out.println("formatDate");
        Date date = Date.valueOf(LocalDate.of(2019, Month.MAY, 2));
        String expResult = "20190502.20190509";

        String result = DepartureDate.formatDate(date);
        assertEquals(expResult, result);
    }

}
