/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.pyton;

import java.util.Date;
import java.util.List;
import nl.bebr.xdat.travel.products.api.events.AccommodationEvent;
import nl.bebr.xdat.travel.products.api.events.RequestDataEvent;
import nl.bebr.xdat.travel.products.enrich.pyton.request.WaverunnerRequestMessage;
import nl.bebr.xdat.travel.products.enrich.pyton.response.general.WaverunnerResponseMessage;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.modelmapper.ModelMapper;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
public class PytonEnrichServiceTest {

    public PytonEnrichServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of removeHTML method, of class PytonEnrichService.
     */
    @Test
    public void testRemoveHTML() {
        System.out.println("removeHTML");
        String xml = "<br /><![CDATA[Ligging]]>";
        PytonHelper helper = new PytonHelper();
        String expResult = "<![CDATA[Ligging]]>";
        String result = helper.removeHTML(xml);
        assertEquals(expResult, result);
    }

    /**
     * Test of parseStarRating method, of class PytonEnrichService.
     */
    @Test
    public void testParseStarRating() {
        System.out.println("parseStarRating");
        String starrating = "3 Sterren";
        PytonHelper helper = new PytonHelper();
        Double expResult = 3.0;
        Double result = helper.parseStarRating(starrating);
        assertEquals(expResult, result);
    }
}
