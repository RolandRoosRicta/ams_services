/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.pyton.waverunner.response;

import nl.bebr.xdat.travel.products.enrich.pyton.response.details.ResponseAccommodationWaverunner;
import nl.bebr.xdat.travel.products.enrich.pyton.response.details.ResponseAccommodationDetails;
import nl.bebr.xdat.travel.products.enrich.pyton.response.changed.ResponseMediaCacheChanged;
import nl.bebr.xdat.travel.products.enrich.pyton.response.error.ResponseErrorMessage;
import nl.bebr.xdat.travel.products.enrich.pyton.response.general.WaverunnerResponseMessage;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import nl.bebr.xdat.travel.products.enrich.pyton.PytonHelper;
import nl.bebr.xdat.travel.products.enrich.pyton.response.details.media.ResponseAccommodationMedia;
import nl.bebr.xdat.travel.products.enrich.pyton.response.details.media.ResponseMediaTouroperator;
import nl.bebr.xdat.travel.products.enrich.pyton.response.details.media.TouroperatorAcco;
import nl.bebr.xdat.travel.products.enrich.pyton.response.details.media.TouroperatorDescription;
import nl.bebr.xdat.travel.products.enrich.pyton.response.details.media.TouroperatorImage;
import nl.bebr.xdat.travel.products.enrich.pyton.response.details.media.TouroperatorText;
import nl.bebr.xdat.travel.products.enrich.pyton.response.details.media.TouroperatorTextItem;
import org.modelmapper.ModelMapper;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author Timon Veenstra [timon at corizon.nl]
 */
public class WaverunnerResponseMessageNGTest {

    public WaverunnerResponseMessageNGTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }

    /**
     * Test of getControl method, of class WaverunnerResponseMessage.
     */
    @Test
    public void testDeserialization() throws FileNotFoundException, IOException {
        System.out.println("deserialization");
        URL url = this.getClass().getResource("/response-media-cache-changed.xml");
        File file = new File(url.getFile());

        XmlMapper mapper = new XmlMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);

        String xml = inputStreamToString(new FileInputStream(file));
        WaverunnerResponseMessage response = mapper.readValue(xml, WaverunnerResponseMessage.class);
        assertNotNull(response);
        assertNotNull(response.getControl());
        assertNotNull(response.getControl().getResponseInfo());
        assertNotNull(response.getControl().getResponseInfo().getModuleId());
        assertNotNull(response.getResponseDetails());
        assertEquals(response.getControl().getResponseInfo().getModuleId(), "Supply");
        ResponseMediaCacheChanged details = (ResponseMediaCacheChanged) response.getResponseDetails();
        assertNotNull(details.getAccommodations());
        assertEquals(details.getAccommodations().size(), 8);
    }

    @Test
    public void testDeserializationErrorMessage() throws FileNotFoundException, IOException {
        System.out.println("deserialization of error message");
        URL url = this.getClass().getResource("/response-error-message.xml");
        File file = new File(url.getFile());

        XmlMapper mapper = new XmlMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);

        String xml = inputStreamToString(new FileInputStream(file));
        WaverunnerResponseMessage response = mapper.readValue(xml, WaverunnerResponseMessage.class);
        assertNotNull(response);
        assertNotNull(response.getControl());
        assertNotNull(response.getControl().getResponseInfo());
        assertNotNull(response.getControl().getResponseInfo().getModuleId());
        assertNotNull(response.getResponseDetails());
        assertEquals(response.getControl().getResponseInfo().getModuleId(), "Supply");
        ResponseErrorMessage details = (ResponseErrorMessage) response.getResponseDetails();
        assertNotNull(details.getMessage());
        assertEquals(details.getMessage(), "Incorrect LastCheckDate");
        assertEquals(details.getMessageId().intValue(), 4);
        assertEquals(details.getMessageType(), "error");
    }

    @Test
    public void testDeserializationAccommodationDetails() throws FileNotFoundException, IOException {
        System.out.println("deserialization of accommodation details");
        URL url = this.getClass().getResource("/response-accommodation-details.xml");
        File file = new File(url.getFile());

        XmlMapper mapper = new XmlMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);

        String xml = inputStreamToString(new FileInputStream(file));
        WaverunnerResponseMessage response = mapper.readValue(xml, WaverunnerResponseMessage.class);
        assertNotNull(response);
        assertNotNull(response.getControl());
        assertNotNull(response.getControl().getResponseInfo());
        assertNotNull(response.getControl().getResponseInfo().getModuleId());
        assertNotNull(response.getResponseDetails());
        assertEquals(response.getControl().getResponseInfo().getModuleId(), "Supply");
        ResponseAccommodationDetails details = (ResponseAccommodationDetails) response.getResponseDetails();
        assertNotNull(details.getWaverunner());
        ResponseAccommodationWaverunner waverunnerDetails = details.getWaverunner();
        assertEquals(waverunnerDetails.getCountry().getId(), 2423);
        assertEquals(waverunnerDetails.getCountry().getName(), "Canarische Eilanden");
        assertEquals(waverunnerDetails.getRegion().getId(), 200);
        assertEquals(waverunnerDetails.getRegion().getName(), "Gran Canaria");
        assertEquals(waverunnerDetails.getPlace().getId(), 4716);
        assertEquals(waverunnerDetails.getPlace().getName(), "Playa Del Ingles");
        assertEquals(waverunnerDetails.getAccommodation().getId(), 357479);
        assertEquals(waverunnerDetails.getAccommodation().getName(), "El Palmar");
        assertEquals(waverunnerDetails.getAccommodationType().getId(), 3);
        assertEquals(waverunnerDetails.getAccommodationType().getType(), "Apartotel");
        assertEquals(waverunnerDetails.getClassification().getId(), 5);
        assertEquals(waverunnerDetails.getClassification().getCode(), "4*");
        assertEquals(waverunnerDetails.getClassification().getStarCount(), "4 Sterren");

        ResponseAccommodationMedia media = details.getMedia();
        assertNotNull(media);
        List<ResponseMediaTouroperator> to = media.getTouroperators();
        assertNotNull(to);
        assertEquals(to.size(), 4);
        Optional<ResponseMediaTouroperator> searchRMT = to.stream().filter(t -> t.getTouroperator() != null && t.getTouroperator().getId() == 231).findAny();
        assertTrue(searchRMT.isPresent());
        ResponseMediaTouroperator rmt = searchRMT.get();
        assertNotNull(rmt);
        TouroperatorDescription tod = rmt.getTouroperatorDescription();
        assertNotNull(tod);
        TouroperatorAcco toa = tod.getAcco();
        assertNotNull(toa);

        assertEquals(toa.getAccoName(), "Apartamentos El Palmar");
        assertEquals(toa.getAccoType(), "");
        assertEquals(toa.getCountry(), "Es");
        assertEquals(toa.getPlace(), "Playa Del Ingles");

        List<TouroperatorImage> images = toa.getImages();
        assertNotNull(images);
        assertEquals(images.size(), 26);
        Optional<TouroperatorImage> findImage = images.stream().filter(i -> i.getOrder() == 1 && "Logo".equals(i.getImgType())).findAny();
        assertTrue(findImage.isPresent());
        TouroperatorImage image = findImage.get();
        assertEquals(image.getImgType(), "Logo");
        assertNull(image.getDescription());
        assertEquals(image.getSeason(), "");
        assertEquals(image.getOrder().intValue(), 1);
        assertEquals(image.getLocation().trim(), "http://b2c.waverunner.nl/Common/General/Logos/Large/PK.gif");

        List<TouroperatorText> texts = toa.getTexts();
        assertNotNull(texts);
        assertEquals(texts.size(), 1);

        TouroperatorText text = texts.get(0);
        assertNotNull(text);

        List<TouroperatorTextItem> items = text.getItem();
        assertNotNull(items);
        assertEquals(items.size(), 1);

        TouroperatorTextItem item = items.get(0);
        assertNotNull(item);

        assertEquals(item.getSeason(), "Z");
        assertEquals(item.getOrder().intValue(), 1);
        assertEquals(item.getHeader(), " Ligging");
        assertTrue(item.getContent().startsWith("Het appartementenhotel is rustig "));

    }

    public String inputStreamToString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        String line;
        try ( BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        }
        return new PytonHelper().removeHTML(sb.toString());
    }

}
