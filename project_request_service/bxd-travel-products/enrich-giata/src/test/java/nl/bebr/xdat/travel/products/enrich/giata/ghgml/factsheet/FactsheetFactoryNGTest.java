/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.giata.ghgml.factsheet;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import nl.bebr.xdat.travel.products.enrich.giata.GiataFactsheet;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
public class FactsheetFactoryNGTest {

    public FactsheetFactoryNGTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }

    /**
     * Test of fromResponse method, of class FactsheetFactory.
     */
    @Test
    public void testFromResponse() {
        System.out.println("fromResponse");

        Fact fact00 = new Fact();
        fact00.setId(129);
        fact00.setName("meals_halfboard");
        fact00.setValue("true");

        Fact fact01 = new Fact();
        fact01.setId(130);
        fact01.setName("meals_fullboard");
        fact01.setValue("true");

        Fact fact10 = new Fact();
        fact10.setId(59);
        fact10.setName("beach_sandy");
        fact10.setValue("true");

        Fact fact20 = new Fact();
        fact20.setId(23);
        fact20.setName("num_floors_annexe");
        fact20.setValue("twelve");

        Fact fact21 = new Fact();
        fact21.setId(24);
        fact21.setName("m2_garden");
        fact21.setValue("123");

        Fact fact22 = new Fact();
        fact22.setId(16);
        fact22.setName("category_official");
        fact22.setValue("15.6");

        List<Fact> facts0 = new ArrayList<>();
        facts0.add(fact00);
        facts0.add(fact01);

        List<Fact> facts1 = new ArrayList<>();
        facts1.add(fact10);

        List<Fact> facts2 = new ArrayList<>();
        facts2.add(fact20);
        facts2.add(fact21);
        facts2.add(fact22);

        Section section0 = new Section();
        section0.setName("meals");
        section0.setType(1009);
        section0.setFacts(facts0);

        Section section1 = new Section();
        section1.setName("beach");
        section1.setType(1006);
        section1.setFacts(facts1);

        Section section2 = new Section();
        section2.setName("building_info");
        section2.setType(1003);
        section2.setFacts(facts2);

        List<Section> sections = new ArrayList<>();
        sections.add(section0);
        sections.add(section1);
        sections.add(section2);

        Factsheet factsheet = new Factsheet();
        factsheet.setLastUpdate(Timestamp.valueOf(LocalDateTime.now()));
        factsheet.setSections(sections);

        Item item = new Item();
        item.setCity("Meppel");
        item.setCountry("NL");
        item.setGiataId(15922);
        item.setName("De koekjesfabriek");
        item.setStreet(null);
        item.setFactsheet(factsheet);

        GHGMLResponse response = new GHGMLResponse("<>", item);

        System.out.println("RESPONSE: " + response);

        GiataFactsheet result = FactsheetFactory.fromResponse(response);

        System.out.println("RESULT " + result);
        assertEquals(result.getCity(), "Meppel");
        assertEquals(result.getCountry(), "NL");
        assertEquals((long) result.getGiataId(), (long) 15922);
        assertEquals(result.getName(), "De koekjesfabriek");
        assertNull(result.getStreet());
        assertNull(result.getBeachPebbles());
        assertTrue(result.getBeachSandy());
        assertTrue(result.getMealsFullboard());
        assertTrue(result.getMealsHalfboard());
        assertNull(result.getMealsAllinclusive());
        assertEquals(result.getNumFloorsAnnexe(), "twelve");
        assertEquals((int) result.getM2Garden(), (int) 123);
        assertNull(result.getNumRoomsDouble());
        assertEquals(result.getCategoryOfficial(), 15.6);

    }

    /**
     * Test of snakeToCamel method, of class FactsheetFactory.
     */
    @Test
    public void testSnakeToCamel() {
        System.out.println("snakeToCamel");
        String snakeCase = "this_is_a_case_for_SNAKE!";
        String expResult = "thisIsACaseForSNAKE!";
        System.out.println("in: " + snakeCase);
        String result = FactsheetFactory.snakeToCamel(snakeCase);
        System.out.println("out: " + result);
        assertEquals(result, expResult);
    }

}
