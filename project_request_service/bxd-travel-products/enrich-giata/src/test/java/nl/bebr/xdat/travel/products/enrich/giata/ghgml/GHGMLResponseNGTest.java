/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.giata.ghgml;

import nl.bebr.xdat.travel.products.enrich.giata.ghgml.factsheet.Section;
import nl.bebr.xdat.travel.products.enrich.giata.ghgml.factsheet.Factsheet;
import nl.bebr.xdat.travel.products.enrich.giata.ghgml.factsheet.GHGMLResponse;
import nl.bebr.xdat.travel.products.enrich.giata.ghgml.factsheet.Item;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.TimeZone;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
public class GHGMLResponseNGTest {

    public GHGMLResponseNGTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }

    /**
     * Test of no specific method, just xml to object verification
     */
    @Test
    public void testXMLToObject() throws FileNotFoundException, IOException {
        System.out.println("testXMLToObject");
        URL url = this.getClass().getResource("/factsheets.xml");
        File file = new File(url.getFile());

        XmlMapper mapper = new XmlMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);

        String xml = inputStreamToString(new FileInputStream(file));
        GHGMLResponse response = mapper.readValue(xml, GHGMLResponse.class);

        System.out.println("RESPONSE:\n" + response);

        //result
        assertNotNull(response);
        assertEquals(response.getSchemaLocation(), "https://ghgml.giatamedia.com/xsd/1.0/itemFactSheet.xsd");

        //item
        assertNotNull(response.getItem());
        Item item = response.getItem();
        assertEquals(item.getName(), "Ferrer Janeiro Hotel & Spa");
        assertNull(item.getStreet());
        assertEquals(item.getCity(), "Can Picafort");
        assertEquals(item.getCountry(), "ES");

        //factsheet
        assertNotNull(item.getFactsheet());
        Factsheet factsheet = item.getFactsheet();
        assertEquals(factsheet.getLastUpdate(), 
                Timestamp.valueOf(ZonedDateTime.parse("2018-06-30T10:56:20+02:00", DateTimeFormatter.ISO_OFFSET_DATE_TIME)
                        .withZoneSameInstant(TimeZone.getDefault().toZoneId())
                        .toLocalDateTime()));

        //sections
        assertNotNull(factsheet.getSections());
        List<Section> sections = factsheet.getSections();
        assertTrue(sections.stream().anyMatch(s -> s.getType() == 1001));
        assertTrue(sections.stream().anyMatch(s -> s.getType() == 1004));
        assertFalse(sections.stream().anyMatch(s -> s.getType() == 1005));
        assertTrue(sections.stream().anyMatch(s -> s.getType() == 1007));

        //section1001
        Section section1 = sections.stream().filter(s -> s.getType() == 1001).findAny().get();
        assertEquals(section1.getName(), "object_information");
        assertNotNull(section1.getFacts());
        assertEquals(section1.getFacts().size(), 2);
        assertTrue(section1.getFacts().stream().allMatch(f -> "string".equals(f.getTypeHint())));
        //fact10
        assertTrue(section1.getFacts().stream().anyMatch(f -> f.getId() == 10));
        assertTrue(section1.getFacts().stream().anyMatch(f -> "fax".equals(f.getName())));
        assertTrue(section1.getFacts().stream().anyMatch(f -> "0034971851365".equals(f.getValue())));
        //fact234
        assertTrue(section1.getFacts().stream().anyMatch(f -> f.getId() == 234));
        assertTrue(section1.getFacts().stream().anyMatch(f -> "phone_hotel".equals(f.getName())));
        assertTrue(section1.getFacts().stream().anyMatch(f -> "0034971100015".equals(f.getValue())));

        //section1004
        Section section4 = sections.stream().filter(s -> s.getType() == 1004).findAny().get();
        assertEquals(section4.getName(), "hotel_type");
        assertNotNull(section4.getFacts());
        assertEquals(section4.getFacts().size(), 1);
        assertTrue(section4.getFacts().stream().allMatch(f -> "bool".equals(f.getTypeHint())));
        //fact32
        assertTrue(section4.getFacts().stream().allMatch(f -> f.getId() == 32));
        assertTrue(section4.getFacts().stream().allMatch(f -> "hoteltype_beachhotel".equals(f.getName())));
        assertTrue(section4.getFacts().stream().allMatch(f -> "true".equals(f.getValue())));

        //section1007
        Section section7 = sections.stream().filter(s -> s.getType() == 1007).findAny().get();
        assertEquals(section7.getName(), "facilities");
        assertNotNull(section7.getFacts());
        assertEquals(section7.getFacts().size(), 3);
        assertTrue(section7.getFacts().stream().anyMatch(f -> "bool".equals(f.getTypeHint())));
        assertTrue(section7.getFacts().stream().anyMatch(f -> "int".equals(f.getTypeHint())));
        //fact67
        assertTrue(section7.getFacts().stream().anyMatch(f -> f.getId() == 67));
        assertTrue(section7.getFacts().stream().anyMatch(f -> "facility_aircon".equals(f.getName())));
        assertTrue(section7.getFacts().stream().anyMatch(f -> "true".equals(f.getValue())));
        //fact69
        assertTrue(section7.getFacts().stream().anyMatch(f -> f.getId() == 69));
        assertTrue(section7.getFacts().stream().anyMatch(f -> "facility_reception24".equals(f.getName())));
        assertTrue(section7.getFacts().stream().anyMatch(f -> "true".equals(f.getValue())));
        //fact81
        assertTrue(section7.getFacts().stream().anyMatch(f -> f.getId() == 81));
        assertTrue(section7.getFacts().stream().anyMatch(f -> "facility_bars".equals(f.getName())));
        assertTrue(section7.getFacts().stream().anyMatch(f -> "1".equals(f.getValue())));
    }

    public String inputStreamToString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        String line;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        }
        return sb.toString();
    }

}
