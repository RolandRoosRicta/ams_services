/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.giata;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import static java.lang.Boolean.TRUE;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import nl.bebr.xdat.travel.products.api.Accommodation.Source;
import static nl.bebr.xdat.travel.products.api.Accommodation.Source.GIATA;
import static nl.bebr.xdat.travel.products.api.Accommodation.Source.PYTON;
import nl.bebr.xdat.travel.products.api.Accommodation.SourceStatus;
import static nl.bebr.xdat.travel.products.api.Accommodation.SourceStatus.UP_TO_DATE;
import nl.bebr.xdat.travel.products.api.AccommodationDTO;
import nl.bebr.xdat.travel.products.enrich.giata.multicodes.Code;
import nl.bebr.xdat.travel.products.enrich.giata.multicodes.GeoCode;
import nl.bebr.xdat.travel.products.enrich.giata.multicodes.MulticodesResponse;
import nl.bebr.xdat.travel.products.enrich.giata.multicodes.Property;
import nl.bebr.xdat.travel.products.enrich.giata.multicodes.Provider;
import nl.bebr.xdat.travel.products.enrich.giata.waverunner.Item;
import nl.bebr.xdat.travel.products.enrich.giata.waverunner.WaverunnerResponse;
import static org.hamcrest.beans.SamePropertyValuesAs.samePropertyValuesAs;
import static org.junit.Assert.assertThat;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author Timon Veenstra [tveenstra@bebr.nl]
 */
public class GiataEnrichServiceNGTest {

    static {
        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "debug");
    }

    public GiataEnrichServiceNGTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }

    @TestConfiguration
    static class AccommodationServiceImplTestContextConfiguration {

        @Bean
        public GiataEnrichService accommodationService() {
            return new GiataEnrichService(new ModelMapper());
        }

    }

    /**
     * Test of getPytonIdMapping method, of class GiataClient.
     */
    @Test
    public void testWaverunnerResponseMapping() throws FileNotFoundException, IOException {
        System.out.println("testWaverunnerResponseMapping");
        URL url = this.getClass().getResource("/waverunner_response.xml");
        File file = new File(url.getFile());

        XmlMapper mapper = new XmlMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);

        String xml = inputStreamToString(new FileInputStream(file));
        WaverunnerResponse response = mapper.readValue(xml, WaverunnerResponse.class);
        assertNotNull(response);
        assertNotNull(response.getItems());
        assertEquals(response.getItems().size(), 11);

        assertTrue(response.getItems().stream().anyMatch((i) -> {
            return i.getGiataId() == 3;
        }));
        assertTrue(response.getItems().stream().anyMatch((i) -> {
            return i.getGiataId() == 21;
        }));
        Item item3 = response.getItems().stream().filter((i) -> {
            return i.getGiataId() == 3;
        }).findFirst().get();
        assertNotNull(item3);
        assertNotNull(item3.getGiataId());
        assertEquals(item3.getGiataId(), 3);
        assertNotNull(item3.getCodes());
        assertEquals(item3.getCodes().size(), 3);
        assertTrue(item3.getCodes().stream().anyMatch((c) -> {
            return c.getValue() == 450771;
        }));
        assertTrue(item3.getCodes().stream().anyMatch((c) -> {
            return c.getValue() == 408884;
        }));
        assertTrue(item3.getCodes().stream().anyMatch((c) -> {
            return c.getValue() == 10358;
        }));

    }

    public String inputStreamToString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        String line;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        }
        return sb.toString();
    }

    /**
     * Test of convertToDto method, of class GiataClient.
     */
    @Test
    public void testConvertToDto() {
        System.out.println("convertToDto");
        GiataFactsheet factsheet = GiataFactsheet
                .builder()
                .giataId(123456)
                .name("Gangsta's Paradise")
                .phoneHotel("88998877")
                .categoryOfficial(12.3)
                .locatedOnMainRoad(true)
                .build();
        Long pytonId = 987654l;
        GiataEnrichService instance = new GiataEnrichService(new ModelMapper());

        Map<Source, Long> expectedSourceIdMap = new HashMap<>();
        expectedSourceIdMap.put(GIATA, 123456L);
        expectedSourceIdMap.put(PYTON, 987654L);
        Map<Source, SourceStatus> expectedSourceStatusMap = new HashMap<>();
        expectedSourceStatusMap.put(GIATA, UP_TO_DATE);
        expectedSourceStatusMap.put(PYTON, UP_TO_DATE);
        AccommodationDTO expectedResult = AccommodationDTO.builder()
                .giataId(123456)
                .name("Gangsta's Paradise")
                .phoneHotel("88998877")
                .categoryOfficial(12.3)
                .locatedOnMainRoad(TRUE)
                .build();

        AccommodationDTO result = instance.toAccommodation(factsheet);
        assertNotNull(result);
        assertThat(result, samePropertyValuesAs(expectedResult));

    }

    /**
     *
     * @throws java.io.FileNotFoundException
     */
    @Test
    public void testMapMulticodesResponse() throws FileNotFoundException, IOException {
        System.out.println("mapMulticodesResponse");

        URL url = this.getClass().getResource("/properties.xml");
        File file = new File(url.getFile());

        XmlMapper mapper = new XmlMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);

        String xml = inputStreamToString(new FileInputStream(file));
        MulticodesResponse response = mapper.readValue(xml, MulticodesResponse.class);
        assertNotNull(response);
        assertNotNull(response.getProperty());

        Property prop = response.getProperty();

        assertEquals(prop.getName(), "Ferrer Janeiro Hotel & Spa");
        assertNotNull(prop.getCity());
        assertEquals(prop.getCity().getValue(), "Can Picafort");
        assertEquals(prop.getCity().getCityId().intValue(), 763);
        assertEquals(prop.getCountry(), "ES");

        assertNotNull(prop.getGeoCodes());
        assertEquals(prop.getGeoCodes().size(), 1);

        GeoCode geocode = prop.getGeoCodes().get(0);

        assertEquals(geocode.getAccuracy(), "address");
        assertEquals(geocode.getLatitude(), 39.759688305966);
        assertEquals(geocode.getLongitude(), 3.1674474477768);

        assertNotNull(prop.getPropertyCodes());
        assertTrue(prop.getPropertyCodes().size() > 0);

        Provider provider = prop.getPropertyCodes().get(0);
        assertNotNull(provider);
        assertNotNull(provider.getProviderCode());
        assertNotNull(provider.getProviderType());
        assertNotNull(provider.getCode());
        assertTrue(provider.getCode().size() > 0);
        assertNotNull(provider.getCode().get(0));

        Code code = provider.getCode().get(0);
        assertNotNull(code.getValue());

        assertNotNull(prop.getGhgml());

    }

}
