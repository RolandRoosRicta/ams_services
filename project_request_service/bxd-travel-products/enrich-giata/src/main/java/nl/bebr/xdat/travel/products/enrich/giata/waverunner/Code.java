/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.giata.waverunner;

import lombok.Data;

/**
 *
 * @author Timon Veenstra [tveenstra@bebr.nl]
 */
@Data
public class Code {
    private long value;
}
