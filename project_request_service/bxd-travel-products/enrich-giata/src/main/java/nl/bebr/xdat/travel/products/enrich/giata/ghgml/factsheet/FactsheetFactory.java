/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.giata.ghgml.factsheet;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import nl.bebr.xdat.travel.products.enrich.giata.GiataFactsheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
public class FactsheetFactory {

    private static final Logger log = LoggerFactory.getLogger(FactsheetFactory.class);

    /**
     * Takes API response from Giata and processes into persistable
     * GiataFactsheetEntity.
     *
     * @param response
     * @return
     */
    public static GiataFactsheet fromResponse(GHGMLResponse response) {

        Item item = response.getItem();
        long giataId = item.getGiataId();

        //
        // Constructing entity with the basic info
        //
        GiataFactsheet entity = GiataFactsheet.builder()
                .giataId((int) giataId)
                .name(item.getName())
                .street(item.getStreet())
                .city(item.getCity())
                .country(item.getCountry())
                .lastUpdate(Timestamp.valueOf(LocalDateTime.now()))
                .build();

        //
        // gathering facts through factsheet, sections
        //
        item
                .getFactsheet()
                .getSections()
                .stream()
                .flatMap(s -> s.getFacts().stream())
                .forEach(f -> {
                    //
                    // Loop facts 
                    //
                    try {
//                        log.debug("Fact! " + snakeToCamel(f.getName()) + ": " + f.getValue());

                        //
                        // Retrieve the appropriate property
                        //
                        Field field = GiataFactsheet.class.getDeclaredField(snakeToCamel(f.getName()));
                        field.setAccessible(true);

                        if (f.getValue() != null && !"".equals(f.getValue())) {
                            //
                            // Determine property type and parse accordingly
                            //
                            if (field.getType().isAssignableFrom(Boolean.class)) {
                                field.set(entity, Boolean.valueOf(f.getValue()));
                            } else if (field.getType().isAssignableFrom(Integer.class)) {
                                field.set(entity, Integer.valueOf(f.getValue()));
                            } else if (field.getType().isAssignableFrom(Double.class)) {
                                field.set(entity, Double.valueOf(f.getValue()));
                            } else if (field.getType().isAssignableFrom(String.class)) {
                                field.set(entity, f.getValue());
                            }
                        }

                    } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
                        log.error("{}", ex);
                    }
                });

        return entity;

    }

    /**
     * Simple method to conver snake_case to camelCase. Needed to convert
     * properties from the api response to the linked entity.
     *
     * @param snakeCase
     * @return
     */
    static String snakeToCamel(String snakeCase) {
        while (snakeCase.contains("_")) {
            snakeCase = snakeCase.replaceFirst("_[a-zA-Z]", String.valueOf(Character.toUpperCase(snakeCase.charAt(snakeCase.indexOf("_") + 1))));
        }

        return snakeCase;
    }
}
