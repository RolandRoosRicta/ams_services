/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.giata;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import java.io.IOException;
import nl.bebr.xdat.travel.products.api.Accommodation;
import nl.bebr.xdat.travel.products.api.AccommodationDTO;
import nl.bebr.xdat.travel.products.api.AccommodationPropFacilityDTO;
import nl.bebr.xdat.travel.products.api.AccommodationPropRoomDTO;
import nl.bebr.xdat.travel.products.api.AccommodationPropSurroundingDTO;
import nl.bebr.xdat.travel.products.api.AccommodationService;
import nl.bebr.xdat.travel.products.api.EnrichService;
import nl.bebr.xdat.travel.products.api.Messaging;
import nl.bebr.xdat.travel.products.api.events.RequestDataEvent;
import nl.bebr.xdat.travel.products.enrich.giata.ghgml.factsheet.FactsheetFactory;
import nl.bebr.xdat.travel.products.enrich.giata.ghgml.factsheet.GHGMLResponse;
import nl.bebr.xdat.travel.products.enrich.giata.multicodes.GeoCode;
import nl.bebr.xdat.travel.products.enrich.giata.multicodes.MulticodesResponse;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import static org.modelmapper.convention.MatchingStrategies.STRICT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.support.BasicAuthenticationInterceptor;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Timon Veenstra [tveenstra@bebr.nl]
 */
@Component
@RabbitListener(queues = Messaging.QUEUE_ENRICH_GIATA_REQUEST)
public class GiataEnrichService implements EnrichService {

    private static final Logger log = LoggerFactory.getLogger(GiataEnrichService.class);

    @Value("${bxd.travel.products.enrich.giata.ghgml.factsheets}")
    private String factsheetsUrl;
    @Value("${bxd.travel.products.enrich.giata.ghgml.properties}")
    private String propertiesUrl;
    @Value("${bxd.travel.products.enrich.giata.waverunner}")
    private String waverunnerUrl;
    @Value("${bxd.travel.products.enrich.giata.username}")
    private String username;
    @Value("${bxd.travel.products.enrich.giata.password}")
    private String password;

    @Autowired
    private AccommodationService accommodationService;

    @Autowired
    private ObjectMapper mapper;

    private final ModelMapper modelMapper;

    @Autowired(required = true)
    public GiataEnrichService(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
        this.modelMapper.getConfiguration().setAmbiguityIgnored(true);
        this.modelMapper.getConfiguration().setMatchingStrategy(STRICT);

        Converter<Integer, Boolean> intToBoolean = ctx -> {
            if (ctx.getSource() == null) {
                return null;
            }
            return ctx.getSource() != 0;
        };

        this.modelMapper.createTypeMap(GiataFactsheet.class, AccommodationDTO.class).addMappings(mapper -> {
            mapper.map(GiataFactsheet::getCountry, AccommodationDTO::setCountryCode);
            mapper.map(GiataFactsheet::getCity, AccommodationDTO::setLocationName);
            mapper.skip(AccommodationDTO::setFacility);
            mapper.skip(AccommodationDTO::setRoom);
            mapper.skip(AccommodationDTO::setSurrounding);
        });
    }

    /**
     * Pull factsheet from GHGML api for given GiataId
     *
     * @param giataId
     * @return
     */
    GiataFactsheet getGiataFactsheet(Long giataId) {
        //
        // set up mapper and template
        //
        XmlMapper mapper = new XmlMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getInterceptors().add(new BasicAuthenticationInterceptor(username, password));

        //
        // Setting up url, download and map to object
        //
        String url = factsheetsUrl + giataId;
        GHGMLResponse response = restTemplate.getForObject(url, GHGMLResponse.class);

        GiataFactsheet entity = FactsheetFactory.fromResponse(response);

        return entity;
    }

    /**
     * Handle the request for giata data event
     *
     * Condition: pyton id should be available
     *
     * checks if giata id is already available for this accommodation if not,
     * get the id from the mapping and send update available event.
     *
     * request data from giata based on the giata id
     *
     * @param event
     */
//    @EventListener(condition = "#event.pytonIdAvailable and (!#event.giataUpToDate or #event.forceUpdate)")
    @RabbitHandler
    @Override
    public void handleRequestDataEvent(String message) throws IOException {
        RequestDataEvent event = mapper.readValue(message, RequestDataEvent.class);

        log.debug("Handling Giata UpdateEvent {}", event);
        long pytonId = event.getSourceId(Accommodation.Source.PYTON);

        Long giataId = event.getSourceId(Accommodation.Source.GIATA);

        if (!event.isGiataIdAvailable()) {
            log.debug("Giata ID not found, retrieving from multicodes/waverunner");
            giataId = getGiataId(pytonId);
        }

        //
        // get giata data
        //
        if (giataId != null) {
            log.debug("Sending request for details from giata");
            //
            // Fill data from factsheet
            //
            GiataFactsheet factsheet = getGiataFactsheet(giataId);

            //
            // Convert to DTO's
            //
            AccommodationDTO accommodationDTO = toAccommodation(factsheet);
            accommodationDTO.setRoom(toAccommodationRoom(factsheet));
            accommodationDTO.setFacility(toAccommodationFacility(factsheet));
            accommodationDTO.setSurrounding(toAccommodationSurrounding(factsheet));

            //
            // Fill lat/long
            //
            fillLatLong(accommodationDTO);

            //
            // Publish updateEvent
            //
            accommodationService.saveAccommodation(accommodationDTO);
        } else {
            log.warn("Something went wrong, could not retrieve giata id from multicodes/waverunner");
        }

    }

    /**
     * takes pyton id and queries waverunner for the corresponding giata id.
     *
     * @param pytonId
     * @return
     */
    public Long getGiataId(long pytonId) {

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getInterceptors().add(new BasicAuthenticationInterceptor(username, password));

        //
        // Setting up url, download and map to object
        //
        String url = waverunnerUrl + pytonId;
        MulticodesResponse response = restTemplate.getForObject(url, MulticodesResponse.class);

        if (response != null && response.getProperty() != null) {
            return response.getProperty().getGiataId();
        }

        return null;
    }

    /**
     * Queries MultiCodes, parses response and strips lat/long from it, assigns
     * that to give DTO.
     *
     * @param accommodationDTO
     */
    public void fillLatLong(AccommodationDTO accommodationDTO) {

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getInterceptors().add(new BasicAuthenticationInterceptor(username, password));

        //
        // Setting up url, download and map to object
        //
        String url = propertiesUrl + accommodationDTO.getGiataId();
        MulticodesResponse response = restTemplate.getForObject(url, MulticodesResponse.class);

        //
        // When response tree isn't empty, fill lat/long
        //
        if (response != null
                && response.getProperty() != null
                && response.getProperty().getGeoCodes() != null
                && !response.getProperty().getGeoCodes().isEmpty()) {
            GeoCode coords = response.getProperty().getGeoCodes().get(0);
            accommodationDTO.setLat(coords.getLatitude());
            accommodationDTO.setLng(coords.getLongitude());
        }
    }

    AccommodationDTO toAccommodation(GiataFactsheet factsheet) {
        return modelMapper.map(factsheet, AccommodationDTO.class);
    }

    AccommodationPropFacilityDTO toAccommodationFacility(GiataFactsheet factsheet) {
        return modelMapper.map(factsheet, AccommodationPropFacilityDTO.class);
    }

    AccommodationPropRoomDTO toAccommodationRoom(GiataFactsheet factsheet) {
        return modelMapper.map(factsheet, AccommodationPropRoomDTO.class);
    }

    AccommodationPropSurroundingDTO toAccommodationSurrounding(GiataFactsheet factsheet) {
        return modelMapper.map(factsheet, AccommodationPropSurroundingDTO.class);
    }

}
