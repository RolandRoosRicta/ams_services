/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.giata.multicodes;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import java.util.List;
import lombok.Data;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
@Data
public class Provider {

    @JacksonXmlProperty(isAttribute = true)
    private String providerCode;

    @JacksonXmlProperty(isAttribute = true)
    private String providerType;

    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Code> code;
}
