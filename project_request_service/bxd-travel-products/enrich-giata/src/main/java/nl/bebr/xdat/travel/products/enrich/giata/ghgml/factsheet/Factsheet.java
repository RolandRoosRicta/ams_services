/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.giata.ghgml.factsheet;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import java.sql.Timestamp;
import java.util.List;
import lombok.Data;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
@Data
public class Factsheet {

    @JacksonXmlProperty(isAttribute = true)
    private Timestamp lastUpdate;

    private List<Section> sections;

}
