/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.giata.ghgml.factsheet;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import java.util.List;
import lombok.Data;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
@Data
public class Section {
    
    @JacksonXmlProperty(isAttribute = true)
    private int type;
    @JacksonXmlProperty(isAttribute = true)
    private String name;
    
    private List<Fact> facts;
}
