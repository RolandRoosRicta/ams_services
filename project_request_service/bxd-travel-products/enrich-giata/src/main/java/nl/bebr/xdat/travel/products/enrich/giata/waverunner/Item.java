/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.giata.waverunner;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import java.util.List;
import lombok.Data;

/**
 *
 * @author Timon Veenstra [tveenstra@bebr.nl]
 */
@Data
public class Item {

    @JacksonXmlProperty(isAttribute = true)
    private long giataId;

    @JacksonXmlElementWrapper(useWrapping = false)
    @JsonProperty("code")
    List<Code> codes;
}
