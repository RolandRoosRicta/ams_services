/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.enrich.giata.waverunner;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Timon Veenstra [tveenstra@bebr.nl]
 */
@Data
@Builder
@JacksonXmlRootElement(localName = "response")
@NoArgsConstructor
@AllArgsConstructor
public class WaverunnerResponse {
    String xsi;
    String schemaLocation;
    List<Item> items;
}
