--#
--# run as database root
--#
CREATE DATABASE bxd_travel_products;
CREATE ROLE travel_products  WITH LOGIN ENCRYPTED PASSWORD 'fqE5wd7ns4gghXMGsWzT';
GRANT ALL PRIVILEGES ON DATABASE bxd_travel_products to travel_products;
