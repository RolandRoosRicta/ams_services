#!/bin/bash

#
# Downloads jar and compares checksum, removes and redownloads if needed
#
module=${1:?please provide a module as first argument}
version=${2:?please provide a profile as second argument}

artifact=$module
group=nl.bebr.xdat

workdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
workdir=$workdir/jars

#
# check version contains SNAPSHOT, if so get from shapshot repo's
# else from release repo's
# downloading both snapshot/release jars works with mvn, but copying
# the jar to workdir doesn't, so kept release version like it was
# to be updated.
#
if [[ $version == *"SNAPSHOT"* ]]
then

    remoteRepo=http://5.9.13.240:8081/repository/maven-public
    mvn dependency:get -DremoteRepositories=$remoteRepo -DgroupId=$group -DartifactId=$artifact -Dversion=$version -Dtransitive=false
    mvn dependency:copy -Dartifact=$group:$artifact:$version -DoutputDirectory=$workdir

    localjar=$workdir/$artifact-$version.jar

else

    nexus=http://5.9.13.240:8081/repository/maven-releases

    jar=$artifact-$version.jar
    remotejar=$nexus/${group//\.//}/$artifact/$version/$jar
    remotemd5=$remotejar.md5
    localjar=$workdir/$jar
    localmd5=$localjar.md5

    #
    # try following process five times max
    #
    for i in {1..5}
    do
        echo "attempt at downloading/checking jar and md5sum #$i"
        #
        #download jar if needed
        #
        if [ ! -f $localjar ]; then
            echo "downloading missing jar from $remotejar"
            wget $remotejar -O $localjar
        fi

        #
        #remove md5 and redownload
        #
        rm $localmd5
        echo "downloading missing md5 from $remotemd5"
        wget $remotemd5 -O $localmd5

        #
        #now check md5
        #the md5sum comes without the filename, so need to check sligthly more convoluted
        #
        if md5sum -c <(printf "%s  $localjar\n" $(cat $localmd5)); then
            #
            #checksum succeeded, so loop can end
            #
            echo "md5sum checked out"
            break
        else
            #
            #checksum failed, removing files, logging attempt number and try again if <6 attempts
            #
            echo "md5sum didn't check out, retrying, this was attempt $i of max 5"
            rm $localjar
            rm $localmd5
        fi
    done

fi

