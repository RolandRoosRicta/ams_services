#!/bin/sh

while ! nc -z bxd-config-server 8899 ; do
    echo "Waiting for upcoming Config Server"
    sleep 2
done
java -jar /opt/bxd/lib/bxd-discovery-1.0-SNAPSHOT.jar
