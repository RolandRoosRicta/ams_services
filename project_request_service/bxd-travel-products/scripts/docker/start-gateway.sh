#!/bin/sh

while ! nc -z bxd-discovery 8761 ; do
    echo "Waiting for upcoming Discovery Server"
    sleep 2
done
java -jar /opt/bxd/lib/bxd-gateway-1.0-SNAPSHOT.jar
