
bash get-jar.sh bxd-config-server 1.0-SNAPSHOT
bash get-jar.sh bxd-discovery 1.0-SNAPSHOT
bash get-jar.sh bxd-gateway 1.0-SNAPSHOT

docker build --tag=alpine-java:base --rm=true -f Dockerfile.alpine-java .
#docker build --tag=bxd-config-server:latest --rm=true -f Dockerfile.bxd-config-server .
#docker build --tag=bxd-discovery:latest --rm=true -f Dockerfile.bxd-discovery .
#docker build --tag=bxd-gateway:latest --rm=true -f Dockerfile.bxd-gateway .
docker volume create --name=bxd-config-repo

docker-compose up --build

#docker stop config-server
#docker rm config-server

#docker run --name=config-server --publish=8899:8899 --volume=spring-cloud-config-repo:/var/lib/spring-cloud/config-repo config-server:latest



