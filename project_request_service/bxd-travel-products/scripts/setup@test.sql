--#
--# run as database root
--#
CREATE DATABASE bxd_travel_products;
CREATE ROLE travel_products  WITH LOGIN ENCRYPTED PASSWORD 'CWaZqLxw4B8RX74m4jvo';
GRANT ALL PRIVILEGES ON DATABASE bxd_travel_products to travel_products;
