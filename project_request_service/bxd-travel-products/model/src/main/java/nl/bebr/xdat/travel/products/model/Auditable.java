/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.model;

import java.util.Date;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import static javax.persistence.TemporalType.TIMESTAMP;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * Provides a base superclass for entities which should have auditable capabilities, providing
 * information on who or what system created each record, when this record was created, who last
 * updated the record and when this update happened.to enhance an entity with auditable capabilities, just extend this class
 and provide normal jpa entity annotations
 * 
 * 
 * 
 * 
 *
 * @author Timon Veenstra [timon at corizon.nl]
 * @param <U> provides the type for the user identification, String would suffice normally
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@AllArgsConstructor
@NoArgsConstructor
public abstract class Auditable<U> {
    
    @CreatedBy
    protected U createdBy;
    
    @CreatedDate
    @Temporal(TIMESTAMP)
    protected Date creationDate;
    
    @LastModifiedBy
    protected U lastModifiedBy;
    
    @LastModifiedDate
    @Temporal(TIMESTAMP)
    protected Date lastModifiedDate;
}
