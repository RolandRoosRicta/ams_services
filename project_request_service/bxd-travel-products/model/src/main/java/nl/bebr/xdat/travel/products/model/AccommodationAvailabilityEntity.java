/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.model;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Builder;
import lombok.Data;
import nl.bebr.xdat.travel.products.api.AccommodationAvailability;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
@Data
@Entity
@Table(name = Schema.TABLE_ACCOMMODATION_AVAILABILITY)
public class AccommodationAvailabilityEntity extends Auditable<String> implements AccommodationAvailability, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "accommodation_id")
    private AccommodationEntity accommodation;

    private Date startDate;
    private Date endDate;
    private String durationType;
    private String boardTypes;
    private String transportTypes;
    private String departurePoints;
    private String unitTypes;
    private String touroperators;

    public AccommodationAvailabilityEntity() {
    }

    @Builder
    public AccommodationAvailabilityEntity(Long id, AccommodationEntity accommodation, Date startDate, Date endDate, String durationType, String boardTypes, String transportTypes, String departurePoints, String unitTypes, String touroperators) {
        this.id = id;
        this.accommodation = accommodation;
        this.startDate = startDate;
        this.endDate = endDate;
        this.durationType = durationType;
        this.boardTypes = boardTypes;
        this.transportTypes = transportTypes;
        this.departurePoints = departurePoints;
        this.unitTypes = unitTypes;
        this.touroperators = touroperators;
    }

}
