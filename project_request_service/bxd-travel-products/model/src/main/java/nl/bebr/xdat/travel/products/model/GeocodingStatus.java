/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.model;

/**
 *
 * @author Kenrik Veenstra <kenrik@corizon.nl>
 */
public enum GeocodingStatus {

    //
    // PRECISION_COUNTRY means country was the most precise info to base geocode on
    // PRECISION_LOCATION means location was the most precise info to base geocode on
    // PRECISION_ACCOMMODATION means accommodation was the most precise info to base geocode on
    // FAILED means geocoding returned something but without any precision, this shouldn't happen
    //
    UNCHECKED, PRECISION_COUNTRY, PRECISION_LOCATION, PRECISION_ACCOMMODATION, FAILED, CHECKED
}
