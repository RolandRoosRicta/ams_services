/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Builder;
import lombok.Data;
import nl.bebr.xdat.travel.products.api.Accommodation;
import static nl.bebr.xdat.travel.products.api.Accommodation.Source.GIATA;
import static nl.bebr.xdat.travel.products.api.Accommodation.Source.OSM;
import static nl.bebr.xdat.travel.products.api.Accommodation.Source.PYTON;
import static nl.bebr.xdat.travel.products.api.Accommodation.Source.RETRESCO;
import static nl.bebr.xdat.travel.products.api.Accommodation.SourceStatus.EMPTY;
import static nl.bebr.xdat.travel.products.api.Accommodation.SourceStatus.UP_TO_DATE;

/**
 * The accommodation entity provides a base for storing accommodaton related
 * data.
 *
 * Because of the big amount of properties available for accommodations, the
 * properties are split up in several sub entities categoried in: - surroundings
 * - room - facility
 *
 * This base entity does not depend on any of the sub entities being present.
 * Relation between this base entitiy and the sub entities is a 1 on 1
 * relationshep. The underlying base table stores the ids of the sub entity
 * primary key record that related to the base entity. This relation is
 * optional, so a base entity does not have any sub entities.
 *
 * sub entities are eagerly loaded and can be obtained on demand using their
 * corresponding getter method
 *
 *
 *
 * @author Timon Veenstra <timon at corizon.nl>
 */
@Data
@Entity
@Table(name = Schema.TABLE_ACCOMMODATION)
public class AccommodationEntity extends Auditable<String> implements Accommodation, Serializable {

    public AccommodationEntity() {
    }

    @Override
    public Long getSourceId(Source source) {
        switch (source) {
            case GIATA:
                return getGiataId() == null ? null : getGiataId().longValue();
            case PYTON:
                return getPytonId() == null ? null : getPytonId().longValue();
            default:
                throw new IllegalArgumentException("Status not available for source " + source);
        }
    }

    @Builder
    public AccommodationEntity(Long id, List<AccommodationAvailabilityEntity> availabilities, AccommodationPropFacilityEntity facility, AccommodationPropRoomEntity room, AccommodationPropSurroundingEntity surrounding, Long importId, Long weeklyWeatherAveragesId, String name, Integer locationId, Integer regionId, Integer countryId, Integer pytonId, Integer giataId, Integer popularity, String type, Integer qualification, String residenceTypeCode, String careCode, String particularities, String overviewSummer, String roomTypeDescription, String attributes, Double lat, Double lng, String locationName, String regionName, String countryName, String countryCode, Double grade, String source, Double imputedLat, Double imputedLng, Double margin, Double avgCostPerNight, Double starRating, Double pytonRating, String giataAirport, Date imputationDateLocation, Date imputationDateGoogle, Date imputationDateOsm, Date imputationDateOpenflight, String assignedRegion, Long owmWeatherLocationId, String canonicalName, String canonicalAddress, Double googleRating, String reviews, Boolean isExternalData, String retrescoText, String hotelChain, String postcode, String phoneReservation, String phoneManagement, String fax, String email, String url, String phoneHotel, Double categoryOfficial, Double categoryRecommended, Boolean locatedOnMainRoad, Integer yearConstruction, Integer yearRenovation, Integer annexeBuildings, Integer numFloorsMain, String numFloorsAnnexe, Integer m2Garden, Integer m2Terrace, Integer numRoomsTotal, Integer numRoomsSingle, Integer numRoomsDouble, Integer numSuites, Integer numApartments, Integer numJuniorSuites, Integer numStudios, Integer numBungalows, Integer numVillas, Boolean hoteltypeCityhotel, Boolean hoteltypeBeachhotel, Boolean hoteltypeApartmenthotel, Boolean hoteltypeBungalowcomplex, Boolean hoteltypeGuesthouse, Boolean hoteltypeRuralhouse, Boolean hoteltypeSkihotel, Boolean hoteltypeClubresort, Boolean hoteltypeFinca, Boolean hoteltypeVillage, Boolean hoteltypeSpacomplex, Boolean hoteltypeGolfhotel, Boolean hoteltypeCasinoresort, Boolean hoteltypeAirporthotel, Boolean hoteltypeEcohotel, Boolean hoteltypeHistoric, Boolean hoteltypeConferencehotel, Boolean hoteltypeYouthhostel, Boolean hoteltypeHoteldecharme, Boolean hoteltypeMountainhotel, Boolean hoteltypeMountainhut, Boolean hoteltypeFamily, Boolean hoteltypeBusiness) {
        this.id = id;
        this.availabilities = availabilities;
        this.facility = facility;
        this.room = room;
        this.surrounding = surrounding;
        this.importId = importId;
        this.weeklyWeatherAveragesId = weeklyWeatherAveragesId;
        this.name = name;
        this.locationId = locationId;
        this.regionId = regionId;
        this.countryId = countryId;
        this.pytonId = pytonId;
        this.giataId = giataId;
        this.popularity = popularity;
        this.type = type;
        this.qualification = qualification;
        this.residenceTypeCode = residenceTypeCode;
        this.careCode = careCode;
        this.particularities = particularities;
        this.overviewSummer = overviewSummer;
        this.roomTypeDescription = roomTypeDescription;
        this.attributes = attributes;
        this.lat = lat;
        this.lng = lng;
        this.locationName = locationName;
        this.regionName = regionName;
        this.countryName = countryName;
        this.countryCode = countryCode;
        this.grade = grade;
        this.source = source;
        this.imputedLat = imputedLat;
        this.imputedLng = imputedLng;
        this.margin = margin;
        this.avgCostPerNight = avgCostPerNight;
        this.starRating = starRating;
        this.pytonRating = pytonRating;
        this.giataAirport = giataAirport;
        this.imputationDateLocation = imputationDateLocation;
        this.imputationDateGoogle = imputationDateGoogle;
        this.imputationDateOsm = imputationDateOsm;
        this.imputationDateOpenflight = imputationDateOpenflight;
        this.assignedRegion = assignedRegion;
        this.owmWeatherLocationId = owmWeatherLocationId;
        this.canonicalName = canonicalName;
        this.canonicalAddress = canonicalAddress;
        this.googleRating = googleRating;
        this.reviews = reviews;
        this.isExternalData = isExternalData;
        this.retrescoText = retrescoText;
        this.hotelChain = hotelChain;
        this.postcode = postcode;
        this.phoneReservation = phoneReservation;
        this.phoneManagement = phoneManagement;
        this.fax = fax;
        this.email = email;
        this.url = url;
        this.phoneHotel = phoneHotel;
        this.categoryOfficial = categoryOfficial;
        this.categoryRecommended = categoryRecommended;
        this.locatedOnMainRoad = locatedOnMainRoad;
        this.yearConstruction = yearConstruction;
        this.yearRenovation = yearRenovation;
        this.annexeBuildings = annexeBuildings;
        this.numFloorsMain = numFloorsMain;
        this.numFloorsAnnexe = numFloorsAnnexe;
        this.m2Garden = m2Garden;
        this.m2Terrace = m2Terrace;
        this.numRoomsTotal = numRoomsTotal;
        this.numRoomsSingle = numRoomsSingle;
        this.numRoomsDouble = numRoomsDouble;
        this.numSuites = numSuites;
        this.numApartments = numApartments;
        this.numJuniorSuites = numJuniorSuites;
        this.numStudios = numStudios;
        this.numBungalows = numBungalows;
        this.numVillas = numVillas;
        this.hoteltypeCityhotel = hoteltypeCityhotel;
        this.hoteltypeBeachhotel = hoteltypeBeachhotel;
        this.hoteltypeApartmenthotel = hoteltypeApartmenthotel;
        this.hoteltypeBungalowcomplex = hoteltypeBungalowcomplex;
        this.hoteltypeGuesthouse = hoteltypeGuesthouse;
        this.hoteltypeRuralhouse = hoteltypeRuralhouse;
        this.hoteltypeSkihotel = hoteltypeSkihotel;
        this.hoteltypeClubresort = hoteltypeClubresort;
        this.hoteltypeFinca = hoteltypeFinca;
        this.hoteltypeVillage = hoteltypeVillage;
        this.hoteltypeSpacomplex = hoteltypeSpacomplex;
        this.hoteltypeGolfhotel = hoteltypeGolfhotel;
        this.hoteltypeCasinoresort = hoteltypeCasinoresort;
        this.hoteltypeAirporthotel = hoteltypeAirporthotel;
        this.hoteltypeEcohotel = hoteltypeEcohotel;
        this.hoteltypeHistoric = hoteltypeHistoric;
        this.hoteltypeConferencehotel = hoteltypeConferencehotel;
        this.hoteltypeYouthhostel = hoteltypeYouthhostel;
        this.hoteltypeHoteldecharme = hoteltypeHoteldecharme;
        this.hoteltypeMountainhotel = hoteltypeMountainhotel;
        this.hoteltypeMountainhut = hoteltypeMountainhut;
        this.hoteltypeFamily = hoteltypeFamily;
        this.hoteltypeBusiness = hoteltypeBusiness;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(referencedColumnName = "id")
    private AccommodationPropFacilityEntity facility;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(referencedColumnName = "id")
    private AccommodationPropRoomEntity room;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(referencedColumnName = "id")
    private AccommodationPropSurroundingEntity surrounding;

    @OneToMany(mappedBy = "accommodation", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<AccommodationAvailabilityEntity> availabilities = new ArrayList<>();

    private Long importId;

    public static final String PROP_WEEKLY_WEATHER_AVERAGES_ID = "weeklyWeatherAveragesId";
    private Long weeklyWeatherAveragesId;

    public static final String PROP_NAME = "name";

    String name;
    public static final String PROP_LOCATION_ID = "locationId";
    Integer locationId;
    public static final String PROP_REGION_ID = "regionId";
    Integer regionId;
    public static final String PROP_COUNTRY_ID = "countryId";
    Integer countryId;
    public static final String PROP_PYTON_ID = "pytonId";
    Integer pytonId;
    public static final String PROP_GIATA_ID = "giataId";
    Integer giataId;
    public static final String PROP_POPULARITY = "popularity";
    Integer popularity;
    public static final String PROP_TYPE = "type";
    String type;
    public static final String PROP_QUALIFICATION = "qualification";
    Integer qualification;
    public static final String PROP_RESIDENCE_TYPE_CODE = "residenceTypeCode";
    String residenceTypeCode;
    public static final String PROP_CARE_CODE = "careCode";
    String careCode;

    //
    // Following properties are mapped as Transient because we're not doing
    // anything with them at the moment and they would take a lot of memory to
    // store
    //
//    public static final String PROP_DISCOVER = "discover";
//    @Transient
//    String discover;
    public static final String PROP_PARTICULARITIES = "particularities";
    @Column(columnDefinition = "text")
    String particularities;

//    public static final String PROP_SITE_DESCRIPTION_EXTRA = "siteDescriptionExtra";
//    @Transient
//    String siteDescriptionExtra;
    public static final String PROP_OVERVIEW_SUMMER = "overviewSummer";
    @Column(columnDefinition = "text")
    String overviewSummer;
//    public static final String PROP_OVERVIEW_WINTER = "overviewWinter";
//    @Transient
//    String overviewWinter;
//    public static final String PROP_ALTERNATIVE_NAMES = "alternativeNames";
//    @Transient
//    String alternativeNames;
//    public static final String PROP_META_KEYWORDS = "metaKeywords";
//    @Transient
//    String metaKeywords;
    public static final String PROP_ROOM_TYPE_DESCRIPTION = "roomTypeDescription";
    @Column(columnDefinition = "text")
    String roomTypeDescription;
    public static final String PROP_ATTRIBUTES = "attributes";
    @Column(columnDefinition = "text")
    String attributes;
    public static final String PROP_LAT = "lat";
    Double lat;
    public static final String PROP_LNG = "lng";
    Double lng;

//    public static final String PROP_SITE_DESCRIPTION = "siteDescription";
//    @Transient
//    String siteDescription;
    public static final String PROP_LOCATION_NAME = "locationName";
    String locationName;
    public static final String PROP_REGION_NAME = "regionName";
    String regionName;
    public static final String PROP_COUNTRY_NAME = "countryName";
    String countryName;
    public static final String PROP_COUNTRY_CODE = "countryCode";
    String countryCode;

    public static final String PROP_GRADE = "grade";
    Double grade;

    public static final String PROP_SOURCE = "source";
    String source;

    //
    // Properties generated with imputation
    //
    public static final String PROP_ROOMS = "rooms";
    public static final String PROP_ROOM_TYPE = PROP_ROOMS + ".type";
    public static final String PROP_ROOM_ATTRIBUTES = PROP_ROOMS + ".attributes";
//    @Transient
//    RoomDescription[] rooms;
    public static final String PROP_IMPUTED_LAT = "imputedLat";
    Double imputedLat;
    public static final String PROP_IMPUTED_LNG = "imputedLng";
    Double imputedLng;

    public static final String PROP_MARGIN = "margin";
    Double margin;

    public static final String PROP_GOOGLE_STATUS = "googleStatus";
    @Column(length = 32, columnDefinition = "varchar(32) default 'EMPTY'")
    @Enumerated(EnumType.STRING)
    SourceStatus googleStatus = SourceStatus.EMPTY;
    public static final String PROP_OSM_STATUS = "osmStatus";
    @Column(length = 32, columnDefinition = "varchar(32) default 'EMPTY'")
    @Enumerated(EnumType.STRING)
    SourceStatus osmStatus = SourceStatus.EMPTY;
    public static final String PROP_PYTON_STATUS = "pytonStatus";
    @Column(length = 32, columnDefinition = "varchar(32) default 'EMPTY'")
    @Enumerated(EnumType.STRING)
    SourceStatus pytonStatus = SourceStatus.EMPTY;
    public static final String PROP_AVG_COST_PER_NIGHT = "avgCostPerNight";
    Double avgCostPerNight;
    public static final String PROP_STAR_RATING = "starRating";
    Double starRating;
    public static final String PROP_PYTON_RATING = "pytonRating";
    Double pytonRating;
    public static final String PROP_GIATA_AIRPORT = "giataAirport";
    @Column(length = 32)
    String giataAirport;
    public static final String PROP_IMPUTATION_DATE_LOCATION = "imputationDateLocation";
    Date imputationDateLocation;
    public static final String PROP_IMPUTATION_DATE_GOOGLE = "imputationDateGoogle";
    Date imputationDateGoogle;
    public static final String PROP_IMPUTATION_DATE_OSM = "imputationDateOsm";
    Date imputationDateOsm;
    public static final String PROP_IMPUTATION_DATE_OPENFLIGHT = "imputationDateOpenflight";
    Date imputationDateOpenflight;
    public static final String PROP_ASSIGNED_REGION = "assignedRegion";
    String assignedRegion;
    public static final String PROP_OWM_WEATHER_LOCATION_ID = "owmWeatherLocationId";
    Long owmWeatherLocationId;

    //
    // other Google variables
    //
    public static final String PROP_CANONICAL_NAME = "canonicalName";
    String canonicalName;
    public static final String PROP_CANONICAL_ADDRESS = "canonicalAddress";
    String canonicalAddress;
    public static final String PROP_CANONICAL_GOOGLE_RATING = "googleRatings";
    Double googleRating;

    public static final String PROP_REVIEWS = "reviews";
    @Column(columnDefinition = "text")
    String reviews;

    public static final String PROP_IS_EXTERNAL_DATA = "isExternalData";
    Boolean isExternalData;

    public static final String PROP_RETRESCO_TEXT = "retrescoText";

    @Column(columnDefinition = "text")
    String retrescoText;

    private String hotelChain;
    private String postcode;
    private String phoneReservation;
    private String phoneManagement;
    private String fax;
    private String email;
    private String url;
    private String phoneHotel;
    private Double categoryOfficial;
    private Double categoryRecommended;
    private Boolean locatedOnMainRoad;
    private Integer yearConstruction;
    private Integer yearRenovation;
    private Integer annexeBuildings;
    private Integer numFloorsMain;
    private String numFloorsAnnexe;
    private Integer m2Garden;
    private Integer m2Terrace;
    private Integer numRoomsTotal;
    private Integer numRoomsSingle;
    private Integer numRoomsDouble;
    private Integer numSuites;
    private Integer numApartments;
    private Integer numJuniorSuites;
    private Integer numStudios;
    private Integer numBungalows;
    private Integer numVillas;
    private Boolean hoteltypeCityhotel;
    private Boolean hoteltypeBeachhotel;
    private Boolean hoteltypeApartmenthotel;
    private Boolean hoteltypeBungalowcomplex;
    private Boolean hoteltypeGuesthouse;
    private Boolean hoteltypeRuralhouse;
    private Boolean hoteltypeSkihotel;
    private Boolean hoteltypeClubresort;
    private Boolean hoteltypeFinca;
    private Boolean hoteltypeVillage;
    private Boolean hoteltypeSpacomplex;
    private Boolean hoteltypeGolfhotel;
    private Boolean hoteltypeCasinoresort;
    private Boolean hoteltypeAirporthotel;
    private Boolean hoteltypeEcohotel;
    private Boolean hoteltypeHistoric;
    private Boolean hoteltypeConferencehotel;
    private Boolean hoteltypeYouthhostel;
    private Boolean hoteltypeHoteldecharme;
    private Boolean hoteltypeMountainhotel;
    private Boolean hoteltypeMountainhut;
    private Boolean hoteltypeFamily;
    private Boolean hoteltypeBusiness;

    @Override
    public SourceStatus getSourceStatus(Source source) {
        switch (source) {
            case GIATA:
                return getGiataId() != null ? UP_TO_DATE : EMPTY;
            case PYTON:
                return getPytonId() != null ? getPytonStatus() : EMPTY;
            case RETRESCO:
                return getRetrescoText() != null ? UP_TO_DATE : EMPTY;
            case OSM:
                return getOsmStatus() != null ? getOsmStatus() : EMPTY;
            default:
                return EMPTY;
        }
    }

    @Override
    public Map<Source, Long> getSourceIdMap() {
        Map<Source, Long> sourceIdMap = new HashMap<>();
        sourceIdMap.put(PYTON, getSourceId(PYTON));
        sourceIdMap.put(GIATA, getSourceId(GIATA));
        return Collections.unmodifiableMap(sourceIdMap);
    }

    @Override
    public Map<Source, SourceStatus> getSourceStatusMap() {
        Map<Source, SourceStatus> sourceStatusMap = new HashMap<>();
        sourceStatusMap.put(PYTON, getSourceStatus(PYTON));
        sourceStatusMap.put(GIATA, getSourceStatus(GIATA));
        sourceStatusMap.put(RETRESCO, getSourceStatus(RETRESCO));
        sourceStatusMap.put(OSM, getSourceStatus(OSM));
        return Collections.unmodifiableMap(sourceStatusMap);
    }

}
