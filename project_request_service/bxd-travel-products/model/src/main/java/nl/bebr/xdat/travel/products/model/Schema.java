/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.model;

/**
 *
 * @author Timon Veenstra <timon at corizon.nl>
 */
public interface Schema {
    public static final String NAME = "travel_products";
    public static final String TABLE_ACCOMMODATION = "accommodations";
    public static final String TABLE_ACCOMMODATION_AVAILABILITY = "accommodation_availabilities";
    public static final String TABLE_ACCOMMODATION_PROP_FACILITY = "accommodation_prop_facilities";
    public static final String TABLE_ACCOMMODATION_PROP_ROOM = "accommodation_prop_rooms";
    public static final String TABLE_ACCOMMODATION_PROP_SURROUNDING = "accommodation_prop_surroundings";
    public static final String TABLE_GIATA_PYTON_MAPPING = "giata_pyton_mapping";
    public static final String TABLE_GIATA_FACTSHEET = "giata_factsheets";
}
