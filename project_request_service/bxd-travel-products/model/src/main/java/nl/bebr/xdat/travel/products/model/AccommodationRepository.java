/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.model;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Timon Veenstra <timon at corizon.nl>
 */
public interface AccommodationRepository extends JpaRepository<AccommodationEntity, Long> {

//    @Query("select * from accommodations where pyton_id = :pytonId")
    List<AccommodationEntity> findByPytonId(@Param("pytonId") int pytonId);

    List<AccommodationEntity> findAllByLastModifiedDateBetweenOrderByIdAsc(Date since, Date until, Pageable pageable);

    @Modifying
    @Query("update AccommodationEntity a set a.retrescoText = :retrescoText where a.id = :id")
    void updateRetrescoText(@Param("retrescoText") String retrescoText, @Param("id") Long id);

    @Query(value = "select * from accommodations where id = "
            + "(select a.id from accommodations a "
            + "left outer join accommodation_availabilities av on a.id = av.accommodation_id "
            + "order by av.last_modified_date asc nulls first "
            + "limit 1)", nativeQuery = true)
    Optional<AccommodationEntity> findNextToGetAvailability();

}
