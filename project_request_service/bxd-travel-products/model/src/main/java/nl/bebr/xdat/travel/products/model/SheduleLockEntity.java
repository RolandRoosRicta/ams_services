/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.model;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * Entity represents the backing table for the shedlock framework.
 * Created so the table will automatically be created by jpa.
 * 
 * WARNING: to not use to manipulated scheduled locks outside of the framwwork!
 *
 * @author Timon Veenstra [timon at corizon.nl]
 */
@Entity
@Table(name = "shedlock")
public class SheduleLockEntity implements Serializable {

    @Id
    @Column(length = 64)
    String name;
    Timestamp lockUntil;
    Timestamp lockedAt;
    @Column(length = 255)
    String lockedBy ;
}
