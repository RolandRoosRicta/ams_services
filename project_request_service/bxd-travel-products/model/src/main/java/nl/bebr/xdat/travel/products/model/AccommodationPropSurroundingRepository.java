/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.model;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Timon Veenstra [timon at corizon.nl]
 */
public interface AccommodationPropSurroundingRepository extends JpaRepository<AccommodationPropSurroundingEntity, Long>{
    
}
