/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.model;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.AuditorAware;

/**
 * AuditorAware implementation providing the identity of the "one" creating or
 * updating an entity.
 * 
 * Please not the generic type of the implementing interface should match the 
 * generic type used in the entity exteding auditable base class.
 * 
 * A bean is created in the persistenceConfig class.
 * 
 * This implementation retrieves the hostname of the operating system the 
 * job is currently running on. Since the system is jon orientied this provided
 * the most accurate indication of who created or updated a record;
 *
 * @author Timon Veenstra [timon at corizon.nl]
 */
//TODO: check if it works with component annotation instead of creating the bean in the config class
public class AuditorAwareImpl implements AuditorAware<String> {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(AuditorAwareImpl.class);    
    
    @Override
    public Optional<String> getCurrentAuditor() {
        String auditor = "UNKNOWN";
        try {
            InetAddress ip = InetAddress.getLocalHost();
            auditor = ip.toString();
        } catch (UnknownHostException ex) {
            log.warn("unable to determine current hostname",ex);
        }
        return Optional.of(auditor);
    }
}
