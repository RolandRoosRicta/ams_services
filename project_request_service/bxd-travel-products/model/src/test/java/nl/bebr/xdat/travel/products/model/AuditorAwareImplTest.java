/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.model;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Optional;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Test;

/**
 *
 * @author Timon Veenstra  <timon at corizon.nl>
 */
public class AuditorAwareImplTest {
    
    public AuditorAwareImplTest() {
    }


    /**
     * Test of getCurrentAuditor method, of class AuditorAwareImpl.
     */
    @Test
    public void testGetCurrentAuditor() throws UnknownHostException {
        System.out.println("getCurrentAuditor");
        AuditorAwareImpl instance = new AuditorAwareImpl();
        Optional<String> result = instance.getCurrentAuditor();
        assertTrue(result.isPresent());
        assertEquals(InetAddress.getLocalHost().toString(), result.get());
    }
    
}
