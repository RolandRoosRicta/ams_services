/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.api;

/**
 *
 * @author Timon Veenstra [timon at corizon.nl]
 */
public class Messaging {
    public static final String EXCHANGE = "bxd.travel-products.exchange";
    public static final String QUEUE_ENRICH_PYTON_REQUEST = "enrich.pyton.request";
    public static final String QUEUE_ENRICH_GIATA_REQUEST = "enrich.giata.request";
    public static final String QUEUE_ENRICH_RETRESCO_REQUEST = "enrich.retresco.request";
    public static final String QUEUE_ENRICH_OSM_REQUEST = "enrich.osm.request";
    public static final String QUEUE_ENRICH_ALL_REQUEST = "enrich.*.request";
}
