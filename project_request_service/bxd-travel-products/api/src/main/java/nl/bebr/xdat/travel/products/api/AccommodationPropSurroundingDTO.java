/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.api;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccommodationPropSurroundingDTO implements AccommodationPropSurrounding, DTO {

    Long id;
    private Integer distCityCentre;
    private Integer distTouristCentre;
    private Integer distBeach;
    private Integer distSea;
    private Integer distLake;
    private Integer distRiver;
    private Integer distForest;
    private Integer distPark;
    private Integer distShopping;
    private Integer distRestaurants;
    private Integer distBarsPubs;
    private Integer distNightclubs;
    private Integer distGolfCourse;
    private Integer distPublicTransport;
    private Integer distBusStation;
    private Integer distTrainStation;
    private Integer distSkiArea;
    private Integer distSkiLift;
    private Integer distCrossCountrySkiing;
    private Integer distStation;

    private Double distanceToNearestCityCentre;
    private Double distanceToNearestNaturalFeature;
    private Double distanceToNearestAirport;
    private Double distanceToNearestBeach;

    private Double barDensity;
    private Double restaurantDensity;
    private Double museumDensity;

    private Double fastFoodRestaurantDensity;
    private Double distanceToNearestBicycleParking;
    private Double distanceToNearestBicycleRental;
    private Double distanceToNearestBoatRental;
    private Double distanceToNearestBusStation;
    private Double distanceToNearestCarRental;
    private Double distanceToNearestFerryTerminal;
    private Double electricCarChargingStationDensity;
    private Double distanceToNearestFuelStation;
    private Double distanceToNearestParking;
    private Double distanceToNearestTaxiStand;
    private Double atmDensity;
    private Double distanceToNearestCurrencyExchange;
    private Double distanceToNearestHospital;
    private Double distanceToNearestPharmacy;
    private Double distanceToNearestCasino;
    private Double distanceToNearestCinema;
    private Double nightclubDensity;
    private Double distanceToNearestPlanetarium;
    private Double theatreDensity;
    private Double diveCentreDensity;
    private Double distanceToNearestEmbassy;
    private Double distanceToNearestInternetCafe;
    private Double distanceToNearestPoliceStation;
    private Double distanceToNearestPublicBath;
    private Double nationalParkDensity;
    private Double distanceToNearestSupermarket;
    private Double distanceToNearestKiosk;
    private Double distanceToNearestChurch;
    private Double distanceToNearestMosque;
    private Double distanceToNearestSynagogue;
    private Double distanceToNearestStadium;
    private Double distanceToNearestTrainStation;
    private Double distanceToNearestParkingGarage;
    private Double distanceToNearestSportsHall;
    private Double distanceToNearestAmbulanceStation;
    private Double distanceToNearestDefibrillator;
    private Double distanceToNearestEmergencyWardEntrance;
    private Double distanceToNearestFireExtinguisher;
    private Double distanceToNearestMountainRescueBase;
    private Double distanceToNearestAssemblyPointEarthquake;
    private Double distanceToNearestAssemblyPointFire;
    private Double distanceToNearestAssemblyPointFlood;
    private Double distanceToNearestAssemblyPointLandslide;
    private Double distanceToNearestAssemblyPointTornado;
    private Double distanceToNearestAssemblyPointTsunami;
    private Double distanceToNearestAircraft;
    private Double distanceToNearestAquaduct;
    private Double distanceToNearestArchaeologicalSite;
    private Double distanceToNearestBattlefield;
    private Double distanceToNearestCannon;
    private Double distanceToNearestCastle;
    private Double distanceToNearestCityGate;
    private Double distanceToNearestAdultGamingCentreGambling;
    private Double distanceToNearestArcade;
    private Double beachResortDensity;
    private Double distanceToNearestSwimmingPool;
    private Double distanceToNearestBirdHide;
    private Double distanceToNearestWildlifeHide;
    private Double waterParkDensity;
    private Double distanceToNearestHorseRiding;
    private Double dancePlaceToGoDancingDensity;
    private Double distanceToNearestDogPark;
    private Double distanceToNearestEscapeRoom;
    private Double distanceToNearestFishing;
    private Double distanceToNearestFitnessCentre;
    private Double distanceToNearestIceRink;
    private Double distanceToNearestMinigolf;
    private Double distanceToNearestMarina;
    private Double distanceToNearestNatureReserve;
    private Double distanceToNearestMunicipalPark;
    private Double distanceToNearestPlayground;
    private Double distanceToNearestSportsCentre;
    private Double distanceToNearestSwimmingArea;
    private Double distanceToNearestRunningTrack;
    private Double distanceToNearestAlpineHut;
    private Double distanceToNearestAquarium;
    private Double distanceToNearestArtwork;
    private Double attractionDensity;
    private Double galleryDensity;
    private Double distanceToNearestInformation;
    private Double distanceToNearestThemePark;
    private Double distanceToNearestViewpoint;
    private Double distanceToNearestZoo;

}
