/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.api;

import java.sql.Date;
import java.util.List;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
public interface AccommodationAvailability {

    Long getId();

    Date getStartDate();

    Date getEndDate();

    String getDurationType();

    String getBoardTypes();

    String getTransportTypes();

    String getDeparturePoints();

    String getUnitTypes();
    
    String getTouroperators();

}
