/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.api;

import java.sql.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccommodationAvailabilityDTO implements AccommodationAvailability, DTO {

    private Long id;
    private Date startDate;
    private Date endDate;
    private String durationType;
    private String boardTypes;
    private String transportTypes;
    private String departurePoints;
    private String unitTypes;
    private String touroperators;

}
