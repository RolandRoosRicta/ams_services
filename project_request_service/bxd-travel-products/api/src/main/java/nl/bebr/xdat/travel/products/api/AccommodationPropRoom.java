/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.api;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
public interface AccommodationPropRoom {

    Boolean getRoomBath();

    Boolean getRoomShower();

    Boolean getRoomBathtub();

    Boolean getRoomBidet();

    Boolean getRoomHairdryer();

    Boolean getRoomDirectdialtel();

    Boolean getRoomSatcabletv();

    Boolean getRoomRadio();

    Boolean getRoomHifi();

    Boolean getRoomInternet();

    Boolean getRoomKitchenette();

    Boolean getRoomMinibar();

    Boolean getRoomFridge();

    Boolean getRoomKingsizedbeds();

    Boolean getRoomTiled();

    Boolean getRoomCarpeted();

    Boolean getRoomAircon();

    Boolean getRoomCentralheating();

    Boolean getRoomSafe();

    Boolean getRoomFinalcleaning();

    Boolean getRoomLounge();

    Boolean getRoomBalcony();

    Boolean getRoomTv();

    Boolean getRoomDoublebed();

    Boolean getRoomAirconIndiv();

    Boolean getRoomIndividualheating();

    Boolean getRoomOven();

    Boolean getRoomMicrowave();

    Boolean getRoomTeaCoffee();

    Boolean getRoomWashingmachine();

    Boolean getRoomWheelchair();

}
