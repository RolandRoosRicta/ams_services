/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.api.events;

import lombok.Data;
import nl.bebr.xdat.travel.products.api.Accommodation.Source;

/**
 *
 * @author Timon Veenstra [tveenstra@bebr.nl]
 */
public class SourceUpdateAvailableEvent extends SourceEvent<Source>{
    
    public SourceUpdateAvailableEvent(Source source,Long sourceId) {
        super(source, sourceId, source);
    }
}
