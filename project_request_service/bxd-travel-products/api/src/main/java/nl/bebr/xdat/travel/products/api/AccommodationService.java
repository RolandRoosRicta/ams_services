/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.api;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import nl.bebr.xdat.travel.products.api.Accommodation.Source;
import nl.bebr.xdat.travel.products.api.events.SourceUpdateAvailableEvent;

/**
 *
 * @author Timon Veenstra <timon at corizon.nl>
 */
public interface AccommodationService {
    Optional<AccommodationDTO> findById( Long id);
    List<AccommodationDTO> findByExternalId(Source source, Long id);
    List<AccommodationDTO> findByIds( List<Long> ids);
    List<AccommodationDTO> findUpdatedAccommodations(Date since,Date until,Integer size, Integer page);
    void saveAccommodation(AccommodationDTO accommodationDTO);
    void handleSourceUpdateAvailableEvent(SourceUpdateAvailableEvent event) ;
    Optional<AccommodationDTO> findNextToGetAvailability();
    
//    AccommodationDTO updateAccommodationText(Long accommodationId, String text);
}
