/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.api.events;

import java.util.Map;
import lombok.ToString;
import nl.bebr.xdat.travel.products.api.Accommodation.Source;
import static nl.bebr.xdat.travel.products.api.Accommodation.Source.GIATA;
import static nl.bebr.xdat.travel.products.api.Accommodation.Source.OSM;
import static nl.bebr.xdat.travel.products.api.Accommodation.Source.PYTON;
import static nl.bebr.xdat.travel.products.api.Accommodation.Source.RETRESCO;
import nl.bebr.xdat.travel.products.api.Accommodation.SourceStatus;
import static nl.bebr.xdat.travel.products.api.Accommodation.SourceStatus.UP_TO_DATE;

/**
 * Event to broadcast the request for more data. enrich module implementations
 * can decide to act on this event to provide this requested data,
 *
 * When they do, they are respected to trigger an updateEvent with a DTO
 * containing the obtained data. The service layer will then decide how to
 * persist this information.
 *
 * The boolean paramater forceUpdate can be used to indicate that new data
 * should be retrieved even though the data seems to be up to date.
 *
 * @author Timon Veenstra [timon at corizon.nl]
 */
@ToString
public class RequestDataEvent  {

    public RequestDataEvent() {
        this.sourceIdMap = null;
        this.sourceStatusMap = null;
        this.accommodationId = null;
        this.forceUpdate = false;
    }
    
    public RequestDataEvent(Long accommodationId, Map<Source, Long> sourceIdMap, Map<Source, SourceStatus> sourceStatusMap) {
        this(accommodationId, sourceIdMap, sourceStatusMap, false);
    }

    public RequestDataEvent(Long accommodationId, Map<Source, Long> sourceIdMap, Map<Source, SourceStatus> sourceStatusMap, boolean forceUpdate) {
        this.sourceIdMap = sourceIdMap;
        this.sourceStatusMap = sourceStatusMap;
        this.accommodationId = accommodationId;
        this.forceUpdate = forceUpdate;
    }

    private final Map<Source, Long> sourceIdMap;
    private final Map<Source, SourceStatus> sourceStatusMap;
    private final Long accommodationId;
    private final boolean forceUpdate;

    public Long getAccommodationId() {
        return accommodationId;
    }

    public boolean isForceUpdate() {
        return forceUpdate;
    }

    public SourceStatus getSourceStatus(Source source) {
        return sourceStatusMap.get(source);
    }

    public Long getSourceId(Source source) {
        return sourceIdMap.get(source);
    }

    // convenience methods so spring expression language can be used to filter
    // interesting objects
    // for example:
    // @EventListener(condition = "#event.pytonIdAvailable and (!#event.giataUpToDate or #event.forceUpdate)")
    public boolean isPytonIdAvailable() {
        return sourceIdMap.containsKey(PYTON) && sourceIdMap.get(PYTON) != null;
    }

    public boolean isPytonUpToDate() {
        return sourceStatusMap.containsKey(PYTON) && UP_TO_DATE.equals(sourceStatusMap.get(PYTON));
    }

    public boolean isGiataIdAvailable() {
        return sourceIdMap.containsKey(GIATA) && sourceIdMap.get(GIATA) != null;
    }

    public boolean isGiataUpToDate() {
        return sourceStatusMap.containsKey(GIATA) && UP_TO_DATE.equals(sourceStatusMap.get(GIATA));
    }

    public boolean isRetrescoUpToDate() {
        return sourceStatusMap.containsKey(RETRESCO) && UP_TO_DATE.equals(sourceStatusMap.get(RETRESCO));
    }

    public boolean isOsmUpToDate() {
        return sourceStatusMap.containsKey(OSM) && UP_TO_DATE.equals(sourceStatusMap.get(OSM));
    }
}
