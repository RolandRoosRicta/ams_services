/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.api;

import java.io.IOException;

/**
 * an enrich service implementation listens for request data events and 
 * provides the data as requested via an update event containing a DTO with the 
 * obtained data.
 * 
 * Implementing classes can use autowiring to obtain an instance of the application event
 * publisher, needed to broadcast the results:
 * 
 *   @Autowired(required = true)
 *   private ApplicationEventPublisher publisher;
 *   ...
 *   publisher.publishEvent(new UpdateEvent(GIATA, giataId, event.getAccommodationId(), accommodationDTO));
 *  
 * @author Timon Veenstra  <timon at corizon.nl>
 */
public interface EnrichService {
    
    /**
     * Handle the request for data
     * 
     * Implementing methods should be annotated with the annotation, @EventListener for example
     * 
     * @throws java.io.IOException
     * @EventListener(condition = "#event.pytonIdAvailable and (!#event.giataUpToDate or #event.forceUpdate)")
     * 
     * Results shohld be published in a DTO using the applicationEventPublusher:
     * 
     * publisher.publishEvent(new UpdateEvent(GIATA, giataId, event.getAccommodationId(), accommodationDTO));
     * 
     * @param message 
     */
    void handleRequestDataEvent(String message) throws IOException;
}
