/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.api;

import java.util.Date;
import java.util.Map;

/**
 * The accommodation interface described the base properties for accommodations.
 *
 * Typically this interface is implemented by a data transfer object (DTO) in
 * this API module, and an entity in the model module. Services can use this
 * interface to provide functionality supporting either of those
 * implementations, providing more flexibility.
 *
 * Base properties can be found in this interface. Room properties in the
 * AccommodationPropRoom interface Facility properties in the
 * AccommodaitonPropFacility interface and properties related to the
 * accommodations surrounding in the AccommodationPropSurrounding interface
 *
 * @author Timon Veenstra <timon at corizon.nl>
 */
public interface Accommodation {

    Long getId();

    String getName();

    Integer getGiataId();

    Map<Source, Long> getSourceIdMap();

    Map<Source, SourceStatus> getSourceStatusMap();

    Integer getPopularity();

    String getType();

    Integer getQualification();

    String getResidenceTypeCode();

    String getCareCode();

    String getParticularities();

    String getOverviewSummer();

    String getRoomTypeDescription();

    String getAttributes();

    Double getLat();

    Double getLng();

    String getLocationName();

    Integer getLocationId();
    
    String getRegionName();

    Integer getRegionId();
    
    String getCountryName();

    Integer getCountryId();

    String getCountryCode();

    Double getGrade();

    String getSource();

    Double getImputedLat();

    Double getImputedLng();

    Double getMargin();

    SourceStatus getGoogleStatus();

    SourceStatus getOsmStatus();

    SourceStatus getPytonStatus();

    Double getAvgCostPerNight();

    Double getStarRating();
    
    Double getPytonRating();

    String getGiataAirport();

    Date getImputationDateLocation();

    Date getImputationDateGoogle();

    Date getImputationDateOsm();

    Date getImputationDateOpenflight();

    String getAssignedRegion();

    Long getOwmWeatherLocationId();

    String getCanonicalName();

    String getCanonicalAddress();

    Double getGoogleRating();

    String getReviews();

    Boolean getIsExternalData();

    String getRetrescoText();

    String getHotelChain();

    String getPostcode();

    String getPhoneReservation();

    String getPhoneManagement();

    String getFax();

    String getEmail();

    String getUrl();

    String getPhoneHotel();

    Double getCategoryOfficial();

    Double getCategoryRecommended();

    Boolean getLocatedOnMainRoad();

    Integer getYearConstruction();

    Integer getYearRenovation();

    Integer getAnnexeBuildings();

    Integer getNumFloorsMain();

    String getNumFloorsAnnexe();

    Integer getM2Garden();

    Integer getM2Terrace();

    Integer getNumRoomsTotal();

    Integer getNumRoomsSingle();

    Integer getNumRoomsDouble();

    Integer getNumSuites();

    Integer getNumApartments();

    Integer getNumJuniorSuites();

    Integer getNumStudios();

    Integer getNumBungalows();

    Integer getNumVillas();

    Boolean getHoteltypeCityhotel();

    Boolean getHoteltypeBeachhotel();

    Boolean getHoteltypeApartmenthotel();

    Boolean getHoteltypeBungalowcomplex();

    Boolean getHoteltypeGuesthouse();

    Boolean getHoteltypeRuralhouse();

    Boolean getHoteltypeSkihotel();

    Boolean getHoteltypeClubresort();

    Boolean getHoteltypeFinca();

    Boolean getHoteltypeVillage();

    Boolean getHoteltypeSpacomplex();

    Boolean getHoteltypeGolfhotel();

    Boolean getHoteltypeCasinoresort();

    Boolean getHoteltypeAirporthotel();

    Boolean getHoteltypeEcohotel();

    Boolean getHoteltypeHistoric();

    Boolean getHoteltypeConferencehotel();

    Boolean getHoteltypeYouthhostel();

    Boolean getHoteltypeHoteldecharme();

    Boolean getHoteltypeMountainhotel();

    Boolean getHoteltypeMountainhut();

    Boolean getHoteltypeFamily();

    Boolean getHoteltypeBusiness();

    SourceStatus getSourceStatus(Source source);

    Long getSourceId(Source source);

    public enum Source {
        PYTON, GIATA, RETRESCO, OSM, RESULT;
    }

    public enum SourceStatus {
        EMPTY, DATA_AVAILABLE, UP_TO_DATE;
    }

}
