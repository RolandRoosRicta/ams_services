/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.api;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccommodationPropFacilityDTO implements AccommodationPropFacility, DTO {

    Long id;
    private Boolean paymentAmex;
    private Boolean paymentVisa;
    private Boolean paymentMaster;
    private Boolean paymentDiners;
    private Boolean paymentJcb;
    private Boolean paymentEc;
    private Boolean beachSandy;
    private Boolean beachPebbles;
    private Boolean beachRocky;
    private Boolean beachSunloungers;
    private Boolean beachParasols;
    private Boolean beachDirectly;
    private Boolean beachSeparatedbyroad;
    private Boolean facilityAircon;
    private Boolean facilityReceptionarea;
    private Boolean facilityReception24;
    private Boolean facilityCheckin24;
    private Integer facilitySafe;
    private Integer facilityMoneyexchange;
    private Integer facilityCloakroom;
    private Integer facilityFoyer;
    private Integer facilityLifts;
    private Integer facilityCafe;
    private Integer facilityKiosk;
    private Integer facilitySupermarket;
    private Integer facilityShops;
    private Integer facilityHairdresser;
    private Integer facilityBars;
    private Integer facilityPub;
    private Integer facilityDisco;
    private Integer facilityTheatre;
    private Integer facilityCasino;
    private Integer facilityGamesroom;
    private Integer facilityRestaurants;
    private Integer facilityRestaurantsAircon;
    private Integer facilityRestaurantsNosmokingarea;
    private Integer facilityRestaurantsHighchair;
    private Integer facilityConferenceroom;
    private Boolean facilityMobilephonenet;
    private Boolean facilityInternet;
    private Boolean facilityWlan;
    private Boolean facilityRoomservice;
    private Boolean facilityLaundryservice;
    private Boolean facilityMedicalservice;
    private Boolean facilityBicyclecellar;
    private Boolean facilityBicyclehire;
    private Boolean facilityCarpark;
    private Boolean facilityGarage;
    private Boolean facilityMiniclub;
    private Boolean facilityPlayground;
    private Integer facilityTvroom;
    private Integer facilityRestaurantSmokingarea;
    private Boolean facilityWashing;
    private Boolean mealsHalfboard;
    private Boolean mealsFullboard;
    private Boolean mealsBreakfastbuffet;
    private Boolean mealsBreakfastserved;
    private Boolean mealsLunchbuffet;
    private Boolean mealsLunchcarte;
    private Boolean mealsLunchchoice;
    private Boolean mealsDinnerbuffet;
    private Boolean mealsDinnercarte;
    private Boolean mealsDinnerchoice;
    private Boolean mealsAllinclusive;
    private Boolean mealsDrinksincluded;
    private Boolean mealsSpecialdiet;
    private Boolean mealsSpecialoffers;
    private Boolean mealsBreakfastcontinental;
    private Integer sportsPoolIndoor;
    private Integer sportsPooloutdoor;
    private Integer sportsPoolfreshwater;
    private Integer sportsPoolsaltwater;
    private Integer sportsPoolchildrens;
    private Integer sportsPoolbar;
    private Integer sportsSunloungers;
    private Integer sportsParasols;
    private Integer sportsWateraerobics;
    private Integer sportsJacuzzi;
    private Integer sportsSauna;
    private Integer sportsSunbathingTerrace;
    private Integer sportsSteambath;
    private Integer sportsMassage;
    private Integer sportsSpecialSpaPack;
    private Integer sportsBananaboat;
    private Integer sportsWaterskiing;
    private Integer sportsJetski;
    private Integer sportsMotorboat;
    private Integer sportsScuba;
    private Integer sportsSurfing;
    private Integer sportsWindsurfing;
    private Integer sportsSailing;
    private Integer sportsCatamaran;
    private Integer sportsCanoe;
    private Integer sportsPedalo;
    private Integer sportsTabletennis;
    private Integer sportsSquash;
    private Integer sportsAerobics;
    private Integer sportsGym;
    private Integer sportsArchery;
    private Integer sportsHorseriding;
    private Integer sportsMtb;
    private Integer sportsBasketball;
    private Integer sportsBeachvolleyball;
    private Integer sportsBilliards;
    private Integer sportsBoccia;
    private Integer sportsBowling;
    private Integer sportsMinigolf;
    private Integer sportsGolf;
    private Integer sportsEntertainment;
    private Integer sportsEntertainmentKids;
    private Integer sportsTennis;
    private Integer sportsBadminton;
    private Integer sportsNumberofpools;
    private Integer sportsPoolheated;
    private Integer sportsGymnastics;
    private Integer sportsDarts;
    private Integer sportsTanningStudio;

}
