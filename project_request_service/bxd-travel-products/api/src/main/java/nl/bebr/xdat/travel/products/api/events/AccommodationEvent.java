/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.api.events;

import com.fasterxml.jackson.annotation.JsonIgnore;
import nl.bebr.xdat.travel.products.api.Accommodation;
import nl.bebr.xdat.travel.products.api.Accommodation.Source;
import static nl.bebr.xdat.travel.products.api.Accommodation.Source.GIATA;
import static nl.bebr.xdat.travel.products.api.Accommodation.Source.OSM;
import static nl.bebr.xdat.travel.products.api.Accommodation.Source.PYTON;
import static nl.bebr.xdat.travel.products.api.Accommodation.Source.RETRESCO;
import nl.bebr.xdat.travel.products.api.Accommodation.SourceStatus;
import static nl.bebr.xdat.travel.products.api.Accommodation.SourceStatus.UP_TO_DATE;
import org.springframework.context.ApplicationEvent;

/**
 *
 * @author Timon Veenstra <timon at corizon.nl>
 */
public class AccommodationEvent extends ApplicationEvent {

    private Long accomodationId;

    public AccommodationEvent(Accommodation accommodation) {
        super(accommodation);
        this.accomodationId = accommodation.getId();
    }

    @JsonIgnore
    @Override
    public Object getSource() {
        return super.getSource();
    }

    public Long getAccomodationId() {
        return accomodationId;
    }

    public SourceStatus getSourceStatus(Source source) {
        return ((Accommodation) getSource()).getSourceStatus(source);
    }

    public boolean isPytonUpToDate() {
        return UP_TO_DATE.equals(getSourceStatus(PYTON));
    }

    public boolean isGiataUpToDate() {
        return UP_TO_DATE.equals(getSourceStatus(GIATA));
    }

    public boolean isRetrescoUpToDate() {
        return UP_TO_DATE.equals(getSourceStatus(RETRESCO));
    }

    public boolean isOsmUpToDate() {
        return UP_TO_DATE.equals(getSourceStatus(OSM));
    }

    public void setAccomodationId(Long accomodationId) {
        this.accomodationId = accomodationId;
    }

}
