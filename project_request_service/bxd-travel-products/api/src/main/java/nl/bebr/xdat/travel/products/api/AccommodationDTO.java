/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.api;

import java.sql.Date;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import nl.bebr.xdat.travel.products.api.Accommodation.Source;

/**
 * Data Transfer Object for accommodation properties
 *
 * Prevent the need for exposing a database entity to an outside API
 *
 *
 * @author Timon Veenstra <timon at corizon.nl>
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccommodationDTO implements Accommodation, DTO {

    Long id;
    AccommodationPropFacilityDTO facility;
    AccommodationPropRoomDTO room;
    AccommodationPropSurroundingDTO surrounding;
    List<AccommodationAvailabilityDTO> availabilities;

    String name;

    Integer giataId;
    Map<Source, Long> sourceIdMap;
    Map<Source, SourceStatus> sourceStatusMap;
    Integer popularity;
    String type;
    Integer qualification;
    String residenceTypeCode;
    String careCode;
    String particularities;
    String overviewSummer;
    String roomTypeDescription;
    String attributes;
    Double lat;
    Double lng;
    String locationName;
    Integer locationId;
    String regionName;
    Integer regionId;
    String countryName;
    Integer countryId;
    String countryCode;
    Double grade;
    String source;
    Double imputedLat;
    Double imputedLng;
    Double margin;
    SourceStatus googleStatus;
    SourceStatus osmStatus;
    SourceStatus pytonStatus;
    Double avgCostPerNight;
    Double starRating;
    Double pytonRating;
    String giataAirport;
    Date imputationDateLocation;
    Date imputationDateGoogle;
    Date imputationDateOsm;
    Date imputationDateOpenflight;
    String assignedRegion;
    Long owmWeatherLocationId;
    String canonicalName;
    String canonicalAddress;
    Double googleRating;
    String reviews;
    Boolean isExternalData;
    String retrescoText;

    private String hotelChain;
    private String postcode;
    private String phoneReservation;
    private String phoneManagement;
    private String fax;
    private String email;
    private String url;
    private String phoneHotel;
    private Double categoryOfficial;
    private Double categoryRecommended;

    private Boolean locatedOnMainRoad;
    private Integer yearConstruction;
    private Integer yearRenovation;
    private Integer annexeBuildings;
    private Integer numFloorsMain;
    private String numFloorsAnnexe;
    private Integer m2Garden;
    private Integer m2Terrace;
    private Integer numRoomsTotal;
    private Integer numRoomsSingle;
    private Integer numRoomsDouble;
    private Integer numSuites;
    private Integer numApartments;
    private Integer numJuniorSuites;
    private Integer numStudios;
    private Integer numBungalows;
    private Integer numVillas;

    private Boolean hoteltypeCityhotel;
    private Boolean hoteltypeBeachhotel;
    private Boolean hoteltypeApartmenthotel;
    private Boolean hoteltypeBungalowcomplex;
    private Boolean hoteltypeGuesthouse;
    private Boolean hoteltypeRuralhouse;
    private Boolean hoteltypeSkihotel;
    private Boolean hoteltypeClubresort;
    private Boolean hoteltypeFinca;
    private Boolean hoteltypeVillage;
    private Boolean hoteltypeSpacomplex;
    private Boolean hoteltypeGolfhotel;
    private Boolean hoteltypeCasinoresort;
    private Boolean hoteltypeAirporthotel;
    private Boolean hoteltypeEcohotel;
    private Boolean hoteltypeHistoric;
    private Boolean hoteltypeConferencehotel;
    private Boolean hoteltypeYouthhostel;
    private Boolean hoteltypeHoteldecharme;
    private Boolean hoteltypeMountainhotel;
    private Boolean hoteltypeMountainhut;
    private Boolean hoteltypeFamily;
    private Boolean hoteltypeBusiness;

    @Override
    public SourceStatus getSourceStatus(Source source) {
        return sourceStatusMap.get(source);
    }

    @Override
    public Long getSourceId(Source source) {
        return sourceIdMap.get(source);
    }

}
