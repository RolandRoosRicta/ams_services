/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.api;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
public interface AccommodationPropSurrounding {

    Integer getDistCityCentre();

    Integer getDistTouristCentre();

    Integer getDistBeach();

    Integer getDistSea();

    Integer getDistLake();

    Integer getDistRiver();

    Integer getDistForest();

    Integer getDistPark();

    Integer getDistShopping();

    Integer getDistRestaurants();

    Integer getDistBarsPubs();

    Integer getDistNightclubs();

    Integer getDistGolfCourse();

    Integer getDistPublicTransport();

    Integer getDistBusStation();

    Integer getDistTrainStation();

    Integer getDistSkiArea();

    Integer getDistSkiLift();

    Integer getDistCrossCountrySkiing();

    Integer getDistStation();

    Double getDistanceToNearestCityCentre();

    Double getDistanceToNearestNaturalFeature();

    Double getDistanceToNearestAirport();

    Double getDistanceToNearestBeach();

    Double getBarDensity();

    Double getRestaurantDensity();

    Double getMuseumDensity();

    Double getFastFoodRestaurantDensity();

    Double getDistanceToNearestBicycleParking();

    Double getDistanceToNearestBicycleRental();

    Double getDistanceToNearestBoatRental();

    Double getDistanceToNearestBusStation();

    Double getDistanceToNearestCarRental();

    Double getDistanceToNearestFerryTerminal();

    Double getElectricCarChargingStationDensity();

    Double getDistanceToNearestFuelStation();

    Double getDistanceToNearestParking();

    Double getDistanceToNearestTaxiStand();

    Double getAtmDensity();

    Double getDistanceToNearestCurrencyExchange();

    Double getDistanceToNearestHospital();

    Double getDistanceToNearestPharmacy();

    Double getDistanceToNearestCasino();

    Double getDistanceToNearestCinema();

    Double getNightclubDensity();

    Double getDistanceToNearestPlanetarium();

    Double getTheatreDensity();

    Double getDiveCentreDensity();

    Double getDistanceToNearestEmbassy();

    Double getDistanceToNearestInternetCafe();

    Double getDistanceToNearestPoliceStation();

    Double getDistanceToNearestPublicBath();

    Double getNationalParkDensity();

    Double getDistanceToNearestSupermarket();

    Double getDistanceToNearestKiosk();

    Double getDistanceToNearestChurch();

    Double getDistanceToNearestMosque();

    Double getDistanceToNearestSynagogue();

    Double getDistanceToNearestStadium();

    Double getDistanceToNearestTrainStation();

    Double getDistanceToNearestParkingGarage();

    Double getDistanceToNearestSportsHall();

    Double getDistanceToNearestAmbulanceStation();

    Double getDistanceToNearestDefibrillator();

    Double getDistanceToNearestEmergencyWardEntrance();

    Double getDistanceToNearestFireExtinguisher();

    Double getDistanceToNearestMountainRescueBase();

    Double getDistanceToNearestAssemblyPointEarthquake();

    Double getDistanceToNearestAssemblyPointFire();

    Double getDistanceToNearestAssemblyPointFlood();

    Double getDistanceToNearestAssemblyPointLandslide();

    Double getDistanceToNearestAssemblyPointTornado();

    Double getDistanceToNearestAssemblyPointTsunami();

    Double getDistanceToNearestAircraft();

    Double getDistanceToNearestAquaduct();

    Double getDistanceToNearestArchaeologicalSite();

    Double getDistanceToNearestBattlefield();

    Double getDistanceToNearestCannon();

    Double getDistanceToNearestCastle();

    Double getDistanceToNearestCityGate();

    Double getDistanceToNearestAdultGamingCentreGambling();

    Double getDistanceToNearestArcade();

    Double getBeachResortDensity();

    Double getDistanceToNearestSwimmingPool();

    Double getDistanceToNearestBirdHide();

    Double getDistanceToNearestWildlifeHide();

    Double getWaterParkDensity();

    Double getDistanceToNearestHorseRiding();

    Double getDancePlaceToGoDancingDensity();

    Double getDistanceToNearestDogPark();

    Double getDistanceToNearestEscapeRoom();

    Double getDistanceToNearestFishing();

    Double getDistanceToNearestFitnessCentre();

    Double getDistanceToNearestIceRink();

    Double getDistanceToNearestMinigolf();

    Double getDistanceToNearestMarina();

    Double getDistanceToNearestNatureReserve();

    Double getDistanceToNearestMunicipalPark();

    Double getDistanceToNearestPlayground();

    Double getDistanceToNearestSportsCentre();

    Double getDistanceToNearestSwimmingArea();

    Double getDistanceToNearestRunningTrack();

    Double getDistanceToNearestAlpineHut();

    Double getDistanceToNearestAquarium();

    Double getDistanceToNearestArtwork();

    Double getAttractionDensity();

    Double getGalleryDensity();

    Double getDistanceToNearestInformation();

    Double getDistanceToNearestThemePark();

    Double getDistanceToNearestViewpoint();

    Double getDistanceToNearestZoo();

}
