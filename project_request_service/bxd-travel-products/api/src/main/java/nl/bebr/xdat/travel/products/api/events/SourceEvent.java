/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.api.events;

import lombok.Data;
import nl.bebr.xdat.travel.products.api.Accommodation.Source;
import static nl.bebr.xdat.travel.products.api.Accommodation.Source.GIATA;
import static nl.bebr.xdat.travel.products.api.Accommodation.Source.OSM;
import static nl.bebr.xdat.travel.products.api.Accommodation.Source.PYTON;
import static nl.bebr.xdat.travel.products.api.Accommodation.Source.RETRESCO;
import org.springframework.context.ApplicationEvent;

/**
 *
 * @author Timon Veenstra [timon at corizon.nl]
 * @param <S> type of source event object
 */
@Data
public abstract class SourceEvent<S> extends ApplicationEvent {

    public SourceEvent(Source accommodationSource, Long sourceId, S sourceObject) {
        super(sourceObject);
        this.accommodationSource = accommodationSource;
        this.sourceId = sourceId;
    }

    public S getSourceObject() {
        return (S) getSource();
    }

    private final Source accommodationSource;
    private final Long sourceId;

    public boolean isPyton() {
        return PYTON.equals(accommodationSource);
    }

    public boolean isGiata() {
        return GIATA.equals(accommodationSource);
    }

    public boolean isRetresco() {
        return RETRESCO.equals(accommodationSource);
    }

    public boolean isOSM() {
        return OSM.equals(accommodationSource);
    }

}
