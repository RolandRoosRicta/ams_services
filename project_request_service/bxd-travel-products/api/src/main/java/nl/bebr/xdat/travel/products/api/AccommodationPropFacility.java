/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.api;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
public interface AccommodationPropFacility {

    Boolean getPaymentAmex();

    Boolean getPaymentVisa();

    Boolean getPaymentMaster();

    Boolean getPaymentDiners();

    Boolean getPaymentJcb();

    Boolean getPaymentEc();

    Boolean getBeachSandy();

    Boolean getBeachPebbles();

    Boolean getBeachRocky();

    Boolean getBeachSunloungers();

    Boolean getBeachParasols();

    Boolean getBeachDirectly();

    Boolean getBeachSeparatedbyroad();

    Boolean getFacilityAircon();

    Boolean getFacilityReceptionarea();

    Boolean getFacilityReception24();

    Boolean getFacilityCheckin24();

    Integer getFacilitySafe();

    Integer getFacilityMoneyexchange();

    Integer getFacilityCloakroom();

    Integer getFacilityFoyer();

    Integer getFacilityLifts();

    Integer getFacilityCafe();

    Integer getFacilityKiosk();

    Integer getFacilitySupermarket();

    Integer getFacilityShops();

    Integer getFacilityHairdresser();

    Integer getFacilityBars();

    Integer getFacilityPub();

    Integer getFacilityDisco();

    Integer getFacilityTheatre();

    Integer getFacilityCasino();

    Integer getFacilityGamesroom();

    Integer getFacilityRestaurants();

    Integer getFacilityRestaurantsAircon();

    Integer getFacilityRestaurantsNosmokingarea();

    Integer getFacilityRestaurantsHighchair();

    Integer getFacilityConferenceroom();

    Boolean getFacilityMobilephonenet();

    Boolean getFacilityInternet();

    Boolean getFacilityWlan();

    Boolean getFacilityRoomservice();

    Boolean getFacilityLaundryservice();

    Boolean getFacilityMedicalservice();

    Boolean getFacilityBicyclecellar();

    Boolean getFacilityBicyclehire();

    Boolean getFacilityCarpark();

    Boolean getFacilityGarage();

    Boolean getFacilityMiniclub();

    Boolean getFacilityPlayground();

    Integer getFacilityTvroom();

    Integer getFacilityRestaurantSmokingarea();

    Boolean getFacilityWashing();

    Boolean getMealsHalfboard();

    Boolean getMealsFullboard();

    Boolean getMealsBreakfastbuffet();

    Boolean getMealsBreakfastserved();

    Boolean getMealsLunchbuffet();

    Boolean getMealsLunchcarte();

    Boolean getMealsLunchchoice();

    Boolean getMealsDinnerbuffet();

    Boolean getMealsDinnercarte();

    Boolean getMealsDinnerchoice();

    Boolean getMealsAllinclusive();

    Boolean getMealsDrinksincluded();

    Boolean getMealsSpecialdiet();

    Boolean getMealsSpecialoffers();

    Boolean getMealsBreakfastcontinental();

    Integer getSportsPoolIndoor();

    Integer getSportsPooloutdoor();

    Integer getSportsPoolfreshwater();

    Integer getSportsPoolsaltwater();

    Integer getSportsPoolchildrens();

    Integer getSportsPoolbar();

    Integer getSportsSunloungers();

    Integer getSportsParasols();

    Integer getSportsWateraerobics();

    Integer getSportsJacuzzi();

    Integer getSportsSauna();

    Integer getSportsSunbathingTerrace();

    Integer getSportsSteambath();

    Integer getSportsMassage();

    Integer getSportsSpecialSpaPack();

    Integer getSportsBananaboat();

    Integer getSportsWaterskiing();

    Integer getSportsJetski();

    Integer getSportsMotorboat();

    Integer getSportsScuba();

    Integer getSportsSurfing();

    Integer getSportsWindsurfing();

    Integer getSportsSailing();

    Integer getSportsCatamaran();

    Integer getSportsCanoe();

    Integer getSportsPedalo();

    Integer getSportsTabletennis();

    Integer getSportsSquash();

    Integer getSportsAerobics();

    Integer getSportsGym();

    Integer getSportsArchery();

    Integer getSportsHorseriding();

    Integer getSportsMtb();

    Integer getSportsBasketball();

    Integer getSportsBeachvolleyball();

    Integer getSportsBilliards();

    Integer getSportsBoccia();

    Integer getSportsBowling();

    Integer getSportsMinigolf();

    Integer getSportsGolf();

    Integer getSportsEntertainment();

    Integer getSportsEntertainmentKids();

    Integer getSportsTennis();

    Integer getSportsBadminton();

    Integer getSportsNumberofpools();

    Integer getSportsPoolheated();

    Integer getSportsGymnastics();

    Integer getSportsDarts();

    Integer getSportsTanningStudio();

}
