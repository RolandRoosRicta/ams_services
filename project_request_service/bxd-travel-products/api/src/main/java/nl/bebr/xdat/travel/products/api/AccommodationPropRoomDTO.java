/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.api;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Kenrik Veenstra [kenrik@corizon.nl]
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccommodationPropRoomDTO implements AccommodationPropRoom,DTO {

    Long id;
    private Boolean roomBath;
    private Boolean roomShower;
    private Boolean roomBathtub;
    private Boolean roomBidet;
    private Boolean roomHairdryer;
    private Boolean roomDirectdialtel;
    private Boolean roomSatcabletv;
    private Boolean roomRadio;
    private Boolean roomHifi;
    private Boolean roomInternet;
    private Boolean roomKitchenette;
    private Boolean roomMinibar;
    private Boolean roomFridge;
    private Boolean roomKingsizedbeds;
    private Boolean roomTiled;
    private Boolean roomCarpeted;
    private Boolean roomAircon;
    private Boolean roomCentralheating;
    private Boolean roomSafe;
    private Boolean roomFinalcleaning;
    private Boolean roomLounge;
    private Boolean roomBalcony;
    private Boolean roomTv;
    private Boolean roomDoublebed;
    private Boolean roomAirconIndiv;
    private Boolean roomIndividualheating;
    private Boolean roomOven;
    private Boolean roomMicrowave;
    private Boolean roomTeaCoffee;
    private Boolean roomWashingmachine;
    private Boolean roomWheelchair;

}
