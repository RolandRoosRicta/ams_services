/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.rest;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.ser.BeanSerializer;
import com.fasterxml.jackson.databind.ser.DefaultSerializerProvider;
import com.fasterxml.jackson.databind.ser.SerializerFactory;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;
import lombok.EqualsAndHashCode.Exclude;
import net.minidev.json.JSONObject;
//import static nl.bebr.xdat.travel.products.api.Accommodation.Source.GIATA;
//import static nl.bebr.xdat.travel.products.api.Accommodation.Source.PYTON;
//import nl.bebr.xdat.travel.products.api.AccommodationDTO;
//import nl.bebr.xdat.travel.products.api.AccommodationService;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.ArgumentMatchers.any;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 *
 * @author Timon Veenstra [timon at corizon.nl]
 */
@RunWith(SpringRunner.class)
//@WebMvcTest(AccommodationsRestController.class)
@SpringBootTest(webEnvironment = WebEnvironment.MOCK, classes = AccommodationsRestController.class)
@EnableWebMvc
@AutoConfigureMockMvc
public class AccommodationsRestControllerTest {

    static {
        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "debug");
    }

    public AccommodationsRestControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Autowired
    private MockMvc mvc;

    //@MockBean
   // AccommodationService mockService;

    /**
     * Test of serialization of dto 
     */
    @Test
    public void testSerializeDto() throws Exception
    {
        ProjectRequestDTO someDto = new ProjectRequestDTO.ProjectRequestDTOBuilder()
                    .id("10")
                    .build();
        String serialized = new ObjectMapper()
        .setSerializationInclusion(Include.NON_NULL)
        .writeValueAsString(someDto);
        
        Assert.assertNotEquals(serialized, null);
        Assert.assertEquals(serialized, "{\"id\":\"10\"}");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        someDto = new ProjectRequestDTO.ProjectRequestDTOBuilder()
                    .dateRequested(format.format(new GregorianCalendar(2019, 11,12).getTime()))
                    .dateExpiration(format.format(new GregorianCalendar(2019, 11,11).getTime()))
                    .description("desc")
                    .title("aanvraag")
                    .id("1")
                    .requestor("a.bruijn")
                    .requestStatus(RequestState.Closed)
                    .requestType(RequestType.Heath)
                .build();
         serialized = new ObjectMapper()
        .setSerializationInclusion(Include.NON_NULL)
        .writeValueAsString(someDto);
           
        Assert.assertEquals(serialized,"{\"title\":\"aanvraag\",\"id\":\"1\",\"description\":\"desc\",\"requestor\":\"a.bruijn\",\"dateRequested\":\"2019-12-12 12:00:00\",\"dateExpiration\":\"2019-12-11 12:00:00\",\"requestStatus\":\"Closed\",\"requestType\":\"Heath\"}"); 
     }
    
    /**
     * Test of findByExternalId method, of class AccommodationsRestController.
     */
    @Test
    public void testFindByExternalId() throws Exception {
        System.out.println("findByExternalId");
        long externalId = 123L;
        long externalIdGiata = 456L;
        String accoName = "ASDfghjHGFDSdFGHJKJHgfdsDFGHJhytRETyhu";

     
    
//        List<AccommodationDTO> expResult = new ArrayList<>();
//        expResult.add(AccommodationDTO.builder().name(accoName).build());
//        Mockito.when(mockService.findByExternalId(PYTON, externalId)).thenReturn(expResult);
//        Mockito.when(mockService.findByExternalId(GIATA, externalIdGiata)).thenReturn(expResult);
//
//        mvc.perform(get("/api/accommodations/pyton/" + externalId)
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$", hasSize(1)))
//                .andExpect(jsonPath("$[0].name", is(accoName))
//                );
//        mvc.perform(get("/api/accommodations/giata/" + externalIdGiata)
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$", hasSize(1)))
//                .andExpect(jsonPath("$[0].name", is(accoName))
//                );
//        mvc.perform(get("/api/accommodations/pyton/112233445566")
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isNotFound());
//
//        mvc.perform(get("/api/accommodations/nonexistingsource/112233445566")
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isBadRequest());
    }

    /**
     * Test of findById method, of class AccommodationsRestController.
     */
    @Test
    public void testFindById() throws Exception {
        System.out.println("findById");
//        long id = 0L;
//        String accoName = "ASDfghjHGFDSdFGHJKJHgfdsDFGHJhytRETyhu";
//
//        Optional<AccommodationDTO> acco = Optional.of(AccommodationDTO.builder().name(accoName).build());
//        Mockito.when(mockService.findById(id)).thenReturn(acco);
//
//        mvc.perform(get("/api/accommodations/" + id)
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.name", is(accoName))
//                );
//
//        mvc.perform(get("/api/accommodations/122345676545678654")
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isNotFound()
//                );
//    }
//
//    /**
//     * Test of findByIds method, of class AccommodationsRestController.
//     */
//    @Test
//    public void testFindByIds() throws Exception {
//        System.out.println("findByIds");
//        List<Long> ids = new ArrayList<>();
//        ids.add(123l);
//        ids.add(456l);
//        ids.add(789l);
//        String accoName1 = "1ASDfghjHGFDSdFGHJKJHgfdsDFGHJhytRETyhu";
//        String accoName2 = "2ASDfghjHGFDSdFGHJKJHgfdsDFGHJhytRETyhu";
//        String accoName3 = "3ASDfghjHGFDSdFGHJKJHgfdsDFGHJhytRETyhu";
//
//        List<AccommodationDTO> expResult = new ArrayList<>();
//        expResult.add(AccommodationDTO.builder().name(accoName1).build());
//        expResult.add(AccommodationDTO.builder().name(accoName2).build());
//        expResult.add(AccommodationDTO.builder().name(accoName3).build());
//        Mockito.when(mockService.findByIds(ids)).thenReturn(expResult);
//
//        mvc.perform(get("/api/accommodations/?ids=123,456,789")
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$", hasSize(3)))
//                .andExpect(jsonPath("$[0].name", is(accoName1)))
//                .andExpect(jsonPath("$[1].name", is(accoName2)))
//                .andExpect(jsonPath("$[2].name", is(accoName3)));
    }

    /**
     * Test of findUpdatedAccommodations method, of class
     * AccommodationsRestController.
     */
    @Test
    public void testFindUpdatedAccommodations() throws Exception {
        System.out.println("findUpdatedAccommodations");
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY");
        Date since = sdf.parse("01-02-2003");
        Date until = sdf.parse("04-05-2006");
        Integer size = 1;
        Integer page = 3;
        String accoName1 = "1ASDfghjHGFDSdFGHJKJHgfdsDFGHJhytRETyhu";
        String accoName2 = "2ASDfghjHGFDSdFGHJKJHgfdsDFGHJhytRETyhu";
        String accoName3 = "3ASDfghjHGFDSdFGHJKJHgfdsDFGHJhytRETyhu";

//        List<AccommodationDTO> expResult = new ArrayList<>();
//        expResult.add(AccommodationDTO.builder().name(accoName1).build());
//        expResult.add(AccommodationDTO.builder().name(accoName2).build());
//        expResult.add(AccommodationDTO.builder().name(accoName3).build());
//        Mockito.when(mockService.findUpdatedAccommodations(since, until, size, page)).thenReturn(expResult);
//        Mockito.when(mockService.findUpdatedAccommodations(any(), any(), any(), any())).thenReturn(expResult);
//
//        mvc.perform(get("/api/accommodations/updated?since="+sdf.format(since)+"&until="+sdf.format(until)+"&page="+page+"&size="+size)
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$", hasSize(3)))
//                .andExpect(jsonPath("$[0].name", is(accoName1)))
//                .andExpect(jsonPath("$[1].name", is(accoName2)))
//                .andExpect(jsonPath("$[2].name", is(accoName3)))
//                ;
//        Mockito.verify(mockService);
                
    }

}
