package nl.bebr.xdat.travel.products.rest;
//import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author roosr
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class ProjectRequestDTO 
{
    public String title;
    public String id;
    public String description;
    public String requestor;
    public String dateRequested;
    public String dateExpiration;
    public RequestState requestStatus;
    public RequestType requestType;
//  public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }

}
