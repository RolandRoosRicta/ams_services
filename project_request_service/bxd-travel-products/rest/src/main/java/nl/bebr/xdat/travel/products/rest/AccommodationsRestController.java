/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.rest;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import static java.util.concurrent.TimeUnit.SECONDS;
import nl.bebr.xdat.travel.products.rest.ProjectRequestDTO.ProjectRequestDTOBuilder;
//import nl.bebr.xdat.travel.products.api.Accommodation.Source;
//import nl.bebr.xdat.travel.products.api.AccommodationDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Timon Veenstra [timon at corizon.nl]
 */
@RestController
@RequestMapping(value = "/api/accommodations",
        produces = {"application/json"}
//        ,consumes = { "application/json", "application/xml" }
)
public class AccommodationsRestController {

    private static final Logger log = LoggerFactory.getLogger(AccommodationsRestController.class);
    public static DateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");

    
    //@Autowired
    //private AccommodationService accommodationService;
    
    //@Autowired(required = true)
    //private ApplicationEventPublisher publisher;    
    @CrossOrigin
    @GetMapping(value = "/test/{source}/{id}")
    public ProjectRequestDTO test(@PathVariable("id") long id,@PathVariable("source") String source) {
      //  accommodationService.handleSourceUpdateAvailableEvent(new SourceUpdateAvailableEvent(Source.valueOf(source.toUpperCase()), id));
      ProjectRequestDTO someDto= new ProjectRequestDTO();
            someDto.title= source+id;
        return someDto;
    }
    @CrossOrigin
    @GetMapping(value = "/update/{source}/{id}")
    public String triggerUpdate(@PathVariable("id") long id,@PathVariable("source") String source) {
      //  accommodationService.handleSourceUpdateAvailableEvent(new SourceUpdateAvailableEvent(Source.valueOf(source.toUpperCase()), id));
        return "{'poep':0}";//"{ok" + source + id;
    }
    
//    @GetMapping(value = "/{id}")
//    public AccommodationDTO findById(@PathVariable("id") long id) {
//        log.debug("findById for id {}", id);
//        
////        AccommodationDTO result =  accommodationService.findById(id).orElseThrow(() -> {
////            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Accommodation Not Found");
////        });
//        
//        return null;
//    }

    @CrossOrigin
    @GetMapping(value = "find/{source}/{id}")
    public ArrayList<ProjectRequestDTO> findByExternalId(
            @PathVariable("source") String sourceName, 
            @PathVariable("id") long externalId) 
    {
        ArrayList<ProjectRequestDTO> list = new ArrayList<ProjectRequestDTO>();
        for (long j=0;j<externalId;j++)
        {
            ProjectRequestDTO someDto;
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.SECOND, calendar.get(Calendar.SECOND) + (int)j);
            Date dateRequested = calendar.getTime();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            //dateRequested.setSeconds(dateRequested.getSeconds()+ (int) j);
            someDto = new ProjectRequestDTOBuilder()
                    .dateRequested(format.format(dateRequested))
                    .dateExpiration((j % 2 == 0) ? format.format(new GregorianCalendar(2019, 11,11).getTime()) : format.format(new GregorianCalendar(2019,1,1).getTime()))
                    .description((j % 2 ==0) ? "desc " + j : "omschrijving a")
                    .title((j % 2 ==0) ? "zon aanvraag " + j : "warmtepomp aanvraag a" + j)
                    .id(Long.toString(j))
                    .requestor((j % 2 == 0) ? "r.roos" : "a.bruijn")
                    .requestStatus((j % 3 == 0) ? RequestState.Closed : RequestState.Open)
                    .requestType((j % 3 == 0) ? RequestType.Sun : RequestType.Heath)
                   .build();
//            ProjectRequestDTO someDto= new ProjectRequestDTO();
//            someDto.title=sourceName+j;
//            someDto.id = Long.toString(j);
//            someDto
            list.add(someDto);
        }
        return list;
    }
//        log.debug("findByExternalId for source {} and id {}", sourceName, externalId);
////        try {
////            Source source = Source.valueOf(sourceName.toUpperCase());
////            List<AccommodationDTO> accommodations = accommodationService.findByExternalId(source, externalId);
////            if (accommodations.isEmpty()){
////                throw new ResponseStatusException(HttpStatus.NOT_FOUND,"No accommodation found for source "+sourceName+" and id "+externalId);
////            }
////            return accommodations;
////        } catch (IllegalArgumentException e) {
////            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Accommodaton source " + sourceName + " unknown");
////        }
//        return null;
//    }

//    @GetMapping(value = "/")
//    public List<AccommodationDTO> findByIds(@RequestParam("ids") List<Long> idList) {
//        log.debug("findByIds");
//        return null;//accommodationService.findByIds(idList);
//    }
//
//    @GetMapping(value = "/updated")
//    public List<AccommodationDTO> findUpdatedAccommodations(
//            @RequestParam(required = true) @DateTimeFormat(pattern = "dd-MM-yyyy") Date since,
//            @RequestParam(required = false) @DateTimeFormat(pattern = "dd-MM-yyyy") Date until,
//            @RequestParam(required = false) Integer size,
//            @RequestParam(required = false) Integer page
//    ) {
//        log.info("find updated accommodations since {} until {} size {} page {}",since,until,size,page);
//        return null;//accommodationService.findUpdatedAccommodations(since, until, size, page);
//    }

}
