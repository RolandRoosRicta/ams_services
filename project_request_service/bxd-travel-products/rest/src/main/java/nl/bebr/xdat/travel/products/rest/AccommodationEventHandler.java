/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
//import nl.bebr.xdat.travel.products.api.events.AccommodationEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

/**
 *
 * @author Timon Veenstra [timon at corizon.nl]
 */
@Component
public class AccommodationEventHandler extends TextWebSocketHandler {

    private static final Logger log = LoggerFactory.getLogger(AccommodationEventHandler.class);

    private static final Set<WebSocketSession> SESSIONS = Collections.synchronizedSet(new HashSet<>());
    private final ObjectMapper mapper = new ObjectMapper();

//    @EventListener()
//    public void handleAccommodationEvent(AccommodationEvent event) {
//        log.debug("Handling handleAccommodationEvent {}", event);
//        log.debug("Amount of active sessions: {}", SESSIONS.size());
//        SESSIONS.forEach((webSocketSession) -> {
//            try {
//                log.debug("sending message to socket {}", webSocketSession.getId());
//                webSocketSession.sendMessage(new TextMessage(mapper.writeValueAsString(event)));
//            } catch (IOException ex) {
//                log.error("sendMessage failed", ex);
//            }
//        });
//    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        log.debug("received message {}", message);
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        //the messages will be broadcasted to all users.
        log.debug("registering session with id: {}", session.getId());
        SESSIONS.add(session);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        SESSIONS.remove(session);
    }

    protected static Set<WebSocketSession> getSessions() {
        return SESSIONS;
    }

}
