package nl.bebr.xdat.travel.products.rest;

import net.javacrumbs.shedlock.spring.annotation.EnableSchedulerLock;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@ComponentScan(basePackages = "nl.bebr.xdat.travel.products")
@EntityScan(basePackages = "nl.bebr.xdat.travel.products")
@EnableSchedulerLock(defaultLockAtMostFor = "PT30S")
public class AccommodationApplication {

    private static final Logger log = LoggerFactory.getLogger(AccommodationApplication.class);

    public static void main(String[] args) {
        new SpringApplicationBuilder(AccommodationApplication.class).web(WebApplicationType.SERVLET).run(args);//.close();
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }


}
