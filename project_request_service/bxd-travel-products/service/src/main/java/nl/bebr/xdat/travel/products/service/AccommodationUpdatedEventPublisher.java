/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.service;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;
import nl.bebr.xdat.travel.products.api.events.AccommodationEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

/**
 *
 * @author Timon Veenstra <timon at corizon.nl>
 */
//TODO check if this component is still actually doing something
@Component
public class AccommodationUpdatedEventPublisher implements ApplicationListener<AccommodationEvent>, Consumer<AccommodationEvent> {

    private static final Logger log = LoggerFactory.getLogger(AccommodationUpdatedEventPublisher.class);

    private final Executor executor;
    private final BlockingQueue<AccommodationEvent> queue = new LinkedBlockingQueue<>();

    public AccommodationUpdatedEventPublisher(@Qualifier("applicationTaskExecutor") Executor executor) {
        this.executor = executor;
    }

    @Override
    public void onApplicationEvent(AccommodationEvent event) {
        log.info("onApplicationEvent: {}", event == null ? null : event.getAccomodationId());
        this.queue.offer(event);
    }

    @Override
    public void accept(AccommodationEvent sink) {
        log.info("accept");
        this.executor.execute(() -> {
            while (true) {
                try {
                    AccommodationEvent event = queue.take(); // <5>
                    log.debug("doing something?");
//                    sink.next(event); // <6>
                } catch (InterruptedException e) {
                    ReflectionUtils.rethrowRuntimeException(e);
                }
            }
        });
    }

}
