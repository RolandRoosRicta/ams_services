/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.toList;
import nl.bebr.xdat.travel.products.api.Accommodation.Source;
import static nl.bebr.xdat.travel.products.api.Accommodation.Source.*;
import nl.bebr.xdat.travel.products.api.Accommodation.SourceStatus;
import nl.bebr.xdat.travel.products.api.AccommodationDTO;
import nl.bebr.xdat.travel.products.api.events.AccommodationEvent;
import nl.bebr.xdat.travel.products.api.AccommodationPropFacilityDTO;
import nl.bebr.xdat.travel.products.api.AccommodationPropRoomDTO;
import nl.bebr.xdat.travel.products.api.AccommodationPropSurroundingDTO;
import nl.bebr.xdat.travel.products.api.AccommodationService;
import nl.bebr.xdat.travel.products.api.Messaging;
import nl.bebr.xdat.travel.products.api.events.RequestDataEvent;
import nl.bebr.xdat.travel.products.api.events.SourceUpdateAvailableEvent;
import nl.bebr.xdat.travel.products.model.AccommodationAvailabilityEntity;
import nl.bebr.xdat.travel.products.model.AccommodationEntity;
import nl.bebr.xdat.travel.products.model.AccommodationPropFacilityEntity;
import nl.bebr.xdat.travel.products.model.AccommodationPropRoomEntity;
import nl.bebr.xdat.travel.products.model.AccommodationPropSurroundingEntity;
import nl.bebr.xdat.travel.products.model.AccommodationRepository;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import static org.modelmapper.convention.MatchingStrategies.STRICT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

/**
 * Accommodion service implementation
 *
 * The service layer is responsible for providing accommodation services like
 * find and storing them.
 *
 * The accommodation service only communicated in Data Transfer Objects, hiding
 * the entities from the persistence layer in the model project.
 *
 *
 *
 *
 * @author Timon Veenstra <timon at corizon.nl>
 */
@Component
public class AccommodationServiceImpl implements AccommodationService {

    private static final Logger log = LoggerFactory.getLogger(AccommodationServiceImpl.class);

    @Autowired(required = true)
    private AccommodationRepository accommodationRepository;

    private final ModelMapper modelMapper;

    @Autowired(required = true)
    private ApplicationEventPublisher publisher;
    @Autowired(required = true)
    private RabbitTemplate rabbitTemplate;

    @Autowired(required = true)
    private Exchange exchange;

    @Autowired
    private ObjectMapper mapper;

    @Autowired(required = true)
    public AccommodationServiceImpl(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
        this.modelMapper.getConfiguration().setMatchingStrategy(STRICT);
        this.modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());

        this.modelMapper.createTypeMap(AccommodationPropFacilityDTO.class, AccommodationPropFacilityEntity.class).addMappings(mapper -> {
            mapper.map(AccommodationPropFacilityDTO::getId, AccommodationPropFacilityEntity::setId);

        });
        this.modelMapper.createTypeMap(AccommodationPropRoomDTO.class, AccommodationPropRoomEntity.class).addMappings(mapper -> {
            mapper.map(AccommodationPropRoomDTO::getId, AccommodationPropRoomEntity::setId);
        });
        this.modelMapper.createTypeMap(AccommodationPropSurroundingDTO.class, AccommodationPropSurroundingEntity.class).addMappings(mapper -> {
            mapper.map(AccommodationPropSurroundingDTO::getId, AccommodationPropSurroundingEntity::setId);
        });
    }

    @Override
    public Optional<AccommodationDTO> findById(Long id) {
        Optional<AccommodationDTO> accommodation = accommodationRepository.findById(id).map((a) -> {
            return convertToDto(a);
        });
        return accommodation;
    }

    @Override
    public List<AccommodationDTO> findByIds(List<Long> ids) {
        return accommodationRepository.findAllById(ids).stream().map((a) -> {
            return convertToDto(a);
        }).collect(toList());
    }

    @Override
    public List<AccommodationDTO> findUpdatedAccommodations(Date since, Date until, Integer size, Integer page) {
        // set default to now
        until = (until != null) ? until : new Date();
        return accommodationRepository.findAllByLastModifiedDateBetweenOrderByIdAsc(since, until, new PageRequest((page == null) ? 1 : page, (size == null) ? 10 : size)).stream().map((a) -> {
            return convertToDto(a);
        }).collect(toList());
    }

    /**
     * handles source update available events for sources other then pyton.
     *
     *
     * @param event
     */
    //TODO provide implementation for handleOtherSourceUpdateAvailableEvent
    @EventListener(condition = "!#event.pyton")
    public void handleOtherSourceUpdateAvailableEvent(SourceUpdateAvailableEvent event) {
        log.warn("source update available event occurred, but not implemnted yet!, event: {}", event);
    }

    /**
     * handles pyton source update available events.
     *
     * looks for accommodations with the provided pyton id. if not found it will
     * create one and request additional information for this new accommodation.
     *
     * when one or more accmmodations are available for thi pyton id, it will
     * request source updates for those found.
     *
     * @param event
     */
    @Override
    public void handleSourceUpdateAvailableEvent(SourceUpdateAvailableEvent event) {
        log.debug("accommodation service implementation handling AccommodationSourceUpdateEvent {}", event);

        if (!event.isPyton()) {
            throw new UnsupportedOperationException("source update events other then pyton not supported yet");
        }
        //
        // find accommodation for the incoming pyton id.
        //
        List<AccommodationEntity> accommodations = accommodationRepository.findByPytonId(event.getSourceId().intValue());
        if (accommodations.isEmpty()) {

            //
            // pyton id is not found, so create a new accommodation and request for more
            // data from other sources
            //
            log.debug("Found new accommodation");
            AccommodationEntity accommodation = AccommodationEntity.builder().pytonId(event.getSourceId().intValue()).build();
            accommodation = accommodationRepository.save(accommodation);
            log.debug("created accommodation with id {}", accommodation.getId());

            try {
                String message = mapper.writeValueAsString(new RequestDataEvent(accommodation.getId(), accommodation.getSourceIdMap(), accommodation.getSourceStatusMap()));
                rabbitTemplate.convertAndSend(exchange.getName(), Messaging.QUEUE_ENRICH_PYTON_REQUEST, mapper.writeValueAsString(message));
                rabbitTemplate.convertAndSend(exchange.getName(), Messaging.QUEUE_ENRICH_GIATA_REQUEST, mapper.writeValueAsString(message));
            } catch (JsonProcessingException ex) {
                log.error("error while converting event to json", ex);
            }
        } else {
            //
            // one or more accommodatons are found for the provided pyton id.
            // request for additional information from the other sources
            //
            //TODO determine when update should be forced, perhaps based on last update timestsmp?
            accommodations.forEach((a) -> {
                log.debug("requesting updates for accommodation with id {}", a.getId());

                RequestDataEvent requestDataEvent = new RequestDataEvent(a.getId(), a.getSourceIdMap(), a.getSourceStatusMap());

                try {
                    String message = mapper.writeValueAsString(new RequestDataEvent(a.getId(), a.getSourceIdMap(), a.getSourceStatusMap()));
                    rabbitTemplate.convertAndSend(exchange.getName(), Messaging.QUEUE_ENRICH_PYTON_REQUEST, mapper.writeValueAsString(message));
                    rabbitTemplate.convertAndSend(exchange.getName(), Messaging.QUEUE_ENRICH_GIATA_REQUEST, mapper.writeValueAsString(message));
                } catch (JsonProcessingException ex) {
                    log.error("error while converting event to json", ex);
                }
            });
        }

    }

    /**
     * handle an update event, which contains new or updated accomoodation data
     * not that accommodations should always already exist.
     *
     * @throws IllegalArgumentException when event does not contain a DTO
     * @throws IllegalStateException when the accommodation does not exist in
     * the database
     * @param accommodationDTO
     */
    @Override
    public void saveAccommodation(AccommodationDTO accommodationDTO) {
        log.debug("saveAccommodation {}", accommodationDTO);
        //
        // Check data and state
        //
        if (accommodationDTO == null) {
            throw new IllegalArgumentException("Update event should always have an AccommodationDTO as source object");
        }
        AccommodationEntity accommodationEntity = accommodationRepository.findById(accommodationDTO.getId()).orElseThrow(() -> {
            throw new IllegalStateException("Update event should always be on an existing accommodation, did not find any for id " + accommodationDTO.getId());
        });
        log.debug("accommodation id {}", accommodationEntity.getId());
        //
        // provide id wiring so entities can be persisted in one go
        //
        if (accommodationEntity.getRoom() != null) {
            accommodationDTO.getRoom().setId(accommodationEntity.getRoom().getId());
        }

        if (accommodationEntity.getFacility() != null) {
            accommodationDTO.getFacility().setId(accommodationEntity.getFacility().getId());
        }
        if (accommodationEntity.getSurrounding() != null) {
            accommodationDTO.getSurrounding().setId(accommodationEntity.getSurrounding().getId());
        }
        //
        // map the properties of the DTO's to Entities.
        // Be aware that STRICT mapping should be used, or id's will be overwritten
        // by something it sees fit caucing entities not to be stored when they are new
        //
        modelMapper.map(convertToEntity(accommodationDTO), accommodationEntity);
        accommodationEntity = accommodationRepository.save(accommodationEntity);
        //
        // publish an accommodation event indicating new data is available for the outside world
        //
        log.debug("publishing accommodation event");
        publisher.publishEvent(new AccommodationEvent(convertToDto(accommodationEntity)));

        boolean allSourceUpToDate = !accommodationEntity.getSourceStatusMap().values().contains(SourceStatus.EMPTY)
                && !accommodationEntity.getSourceStatusMap().values().contains(SourceStatus.DATA_AVAILABLE);
        //
        // If not all sources are up to date, request for additional information
        // BE AWARE, force update should NEVER be used in the case, or it will create
        // and endless chain of request events
        //
        RequestDataEvent requestDataEvent = new RequestDataEvent(accommodationEntity.getId(), accommodationEntity.getSourceIdMap(), accommodationEntity.getSourceStatusMap());
        if (!SourceStatus.UP_TO_DATE.equals(accommodationEntity.getSourceStatus(OSM))) {
            try {
                rabbitTemplate.convertAndSend(exchange.getName(), Messaging.QUEUE_ENRICH_OSM_REQUEST, mapper.writeValueAsString(requestDataEvent));
            } catch (JsonProcessingException ex) {
                log.error("Error requesting OSM update", ex);
            }
        }
        if (!SourceStatus.UP_TO_DATE.equals(accommodationEntity.getSourceStatus(PYTON))) {
            try {
                rabbitTemplate.convertAndSend(exchange.getName(), Messaging.QUEUE_ENRICH_PYTON_REQUEST, mapper.writeValueAsString(requestDataEvent));
            } catch (JsonProcessingException ex) {
                log.error("Error requesting PYTON update", ex);
            }
        }
        if (!SourceStatus.UP_TO_DATE.equals(accommodationEntity.getSourceStatus(GIATA))) {
            try {
                rabbitTemplate.convertAndSend(exchange.getName(), Messaging.QUEUE_ENRICH_GIATA_REQUEST, mapper.writeValueAsString(requestDataEvent));
            } catch (JsonProcessingException ex) {
                log.error("Error requesting GIATA update", ex);
            }
        }
        if (!SourceStatus.UP_TO_DATE.equals(accommodationEntity.getSourceStatus(RETRESCO))) {
            try {
                rabbitTemplate.convertAndSend(exchange.getName(), Messaging.QUEUE_ENRICH_RETRESCO_REQUEST, mapper.writeValueAsString(requestDataEvent));
            } catch (JsonProcessingException ex) {
                log.error("Error requesting RETRESCO update", ex);
            }
        }
    }

    private AccommodationDTO convertToDto(AccommodationEntity accommodationEntity) {
        AccommodationDTO accommodationDTO = modelMapper.map(accommodationEntity, AccommodationDTO.class);

        Map<Source, Long> sourceIds = new HashMap<>();
        Map<Source, SourceStatus> sourceStatusMap = new HashMap<>();
        if (accommodationEntity.getPytonId() != null) {
            sourceIds.put(PYTON, accommodationEntity.getPytonId().longValue());
            sourceStatusMap.put(PYTON, SourceStatus.UP_TO_DATE);
        } else {
            sourceStatusMap.put(PYTON, SourceStatus.EMPTY);
        }
        if (accommodationEntity.getGiataId() != null) {
            sourceIds.put(GIATA, accommodationEntity.getGiataId().longValue());
            sourceStatusMap.put(GIATA, SourceStatus.UP_TO_DATE);
        } else {
            sourceStatusMap.put(GIATA, SourceStatus.EMPTY);
        }
        if (accommodationEntity.getRetrescoText() != null) {
            sourceStatusMap.put(RETRESCO, SourceStatus.UP_TO_DATE);
        } else {
            sourceStatusMap.put(RETRESCO, SourceStatus.EMPTY);
        }
        if (accommodationEntity.getOsmStatus() != null) {
            sourceStatusMap.put(OSM, accommodationEntity.getOsmStatus());
        } else {
            sourceStatusMap.put(OSM, SourceStatus.EMPTY);
        }
        accommodationDTO.setSourceIdMap(sourceIds);
        accommodationDTO.setSourceStatusMap(sourceStatusMap);

        return accommodationDTO;
    }

    private AccommodationEntity convertToEntity(AccommodationDTO dto) {

        //
        // Needed more explicit mapping because of the following scenario:
        // if all variables of eg a roomDTO are null, it won't be mapped when 
        // just mapping AccommodationDTO -> AccommodationEntity.
        // this way it will get mapped.
        //
        AccommodationEntity entity = modelMapper.map(dto, AccommodationEntity.class);
        entity.setRoom(
                dto.getRoom() == null ? null
                : modelMapper.map(dto.getRoom(), AccommodationPropRoomEntity.class));
        entity.setFacility(
                dto.getFacility() == null ? null
                : modelMapper.map(dto.getFacility(), AccommodationPropFacilityEntity.class));
        entity.setSurrounding(
                dto.getSurrounding() == null ? null
                : modelMapper.map(dto.getSurrounding(), AccommodationPropSurroundingEntity.class));
        entity.setAvailabilities(
                dto.getAvailabilities() == null || dto.getAvailabilities().isEmpty() ? null
                : dto.getAvailabilities()
                        .stream()
                        .map(av -> {
                            //
                            // AccommodationAvailability needs it's accommodation
                            // set explicitly
                            //
                            AccommodationAvailabilityEntity ave = modelMapper.map(av, AccommodationAvailabilityEntity.class);
                            ave.setAccommodation(entity);
                            return ave;
                        })
                        .collect(Collectors.toList()));

        return entity;
    }

    /**
     * find accommodations by source id
     *
     * @param source the source of the id (PYTON/GIATA)
     * @param id the external id to look for
     * @return zero or more accommodation data transfer objects
     */
    @Override
    public List<AccommodationDTO> findByExternalId(Source source, Long id) {
        List<AccommodationDTO> result = new ArrayList<>();
        switch (source) {
            case PYTON:
                result.addAll(accommodationRepository.findByPytonId(id.intValue()).stream().map((a) -> {
                    return convertToDto(a);
                }).collect(toList()));
                break;
            default:
                throw new UnsupportedOperationException("findByExternalId not implemented yet for source " + source);
        }
        return result;
    }

    @Override
    public Optional<AccommodationDTO> findNextToGetAvailability() {
        return accommodationRepository.findNextToGetAvailability().map((a) -> convertToDto(a));
    }

}
