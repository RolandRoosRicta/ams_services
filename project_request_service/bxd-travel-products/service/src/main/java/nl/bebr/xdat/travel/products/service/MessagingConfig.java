/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.service;

import nl.bebr.xdat.travel.products.api.Messaging;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author Timon Veenstra [timon at corizon.nl]
 */
@Configuration
public class MessagingConfig {

    @Bean
    public Exchange eventExchange() {
        return new TopicExchange(Messaging.EXCHANGE);
    }

    @Bean
    public Queue queueEnrichPytonRequest() {
        return new Queue(Messaging.QUEUE_ENRICH_PYTON_REQUEST);
    }

    @Bean
    public Queue queueEnrichRetrescoRequest() {
        return new Queue(Messaging.QUEUE_ENRICH_RETRESCO_REQUEST);
    }

    @Bean
    public Queue queueEnrichGiataRequest() {
        return new Queue(Messaging.QUEUE_ENRICH_GIATA_REQUEST);
    }

    @Bean
    public Queue queueEnrichOSMRequest() {
        return new Queue(Messaging.QUEUE_ENRICH_OSM_REQUEST);
    }

    @Bean
    public Binding pytonBinding(Queue queueEnrichPytonRequest, Exchange eventExchange) {
        return BindingBuilder
                .bind(queueEnrichPytonRequest)
                .to(eventExchange)
                .with(Messaging.QUEUE_ENRICH_PYTON_REQUEST)
                .noargs();
    }

    @Bean
    public Binding giataBinding(Queue queueEnrichGiataRequest, Exchange eventExchange) {
        return BindingBuilder
                .bind(queueEnrichGiataRequest)
                .to(eventExchange)
                .with(Messaging.QUEUE_ENRICH_GIATA_REQUEST)
                .noargs();
    }

    @Bean
    public Binding retrescoBinding(Queue queueEnrichRetrescoRequest, Exchange eventExchange) {
        return BindingBuilder
                .bind(queueEnrichRetrescoRequest)
                .to(eventExchange)
                .with(Messaging.QUEUE_ENRICH_RETRESCO_REQUEST)
                .noargs();
    }

    @Bean
    public Binding osmBinding(Queue queueEnrichOSMRequest, Exchange eventExchange) {
        return BindingBuilder
                .bind(queueEnrichOSMRequest)
                .to(eventExchange)
                .with(Messaging.QUEUE_ENRICH_OSM_REQUEST)
                .noargs();
    }

}
