/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bebr.xdat.travel.products.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Optional;
import nl.bebr.xdat.travel.products.api.AccommodationDTO;
import nl.bebr.xdat.travel.products.api.AccommodationService;
import nl.bebr.xdat.travel.products.model.AccommodationEntity;
import nl.bebr.xdat.travel.products.model.AccommodationPropFacilityRepository;
import nl.bebr.xdat.travel.products.model.AccommodationPropRoomRepository;
import nl.bebr.xdat.travel.products.model.AccommodationPropSurroundingRepository;
import nl.bebr.xdat.travel.products.model.AccommodationRepository;
import static org.hamcrest.beans.SamePropertyValuesAs.samePropertyValuesAs;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import static org.mockito.ArgumentMatchers.any;
import org.mockito.Mockito;
import static org.mockito.Mockito.doNothing;
import org.modelmapper.ModelMapper;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author Timon Veenstra [tveenstra@bebr.nl]
 */
@RunWith(SpringRunner.class)
public class AccommodationServiceImplTest {

    public AccommodationServiceImplTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @TestConfiguration
    static class AccommodationServiceImplTestContextConfiguration {

        @Bean
        public AccommodationService accommodationService() {
            return new AccommodationServiceImpl(new ModelMapper());
        }

    }

    @MockBean
    AccommodationRepository mockRepository;
    @MockBean
    AccommodationPropFacilityRepository mockFacilityRepository;
    @MockBean
    AccommodationPropRoomRepository mockRoomRepository;
    @MockBean
    AccommodationPropSurroundingRepository mockSurroundingRepository;

    @MockBean
    private RabbitTemplate rabbitTemplate;
    @MockBean
    private Exchange exchange;    
    
    @MockBean
    ApplicationEventPublisher mockPublisher;

    @Autowired
    ApplicationContext context;
    @MockBean
    ObjectMapper mapper;

    @Autowired
    AccommodationService accommodationService;

    /**
     * Test of findById method, of class AccommodationServiceImpl.
     */
    @Test
    public void testFindById() {
        System.out.println("findById");
        try {

            Long id = 123456L;
            AccommodationEntity accommodationEntity = AccommodationEntity.builder().build();
            Mockito.when(mockRepository.findById(id)).thenReturn(Optional.of(accommodationEntity));
            doNothing().when(mockPublisher).publishEvent(any());
            doNothing().when(mockPublisher).publishEvent(any());

            Optional<AccommodationDTO> result = accommodationService.findById(id);
            assertTrue(result.isPresent());
            AccommodationDTO accommodationDTO = result.get();
            assertEquals(accommodationDTO.getId(), accommodationEntity.getId());
            assertThat(accommodationDTO, samePropertyValuesAs(accommodationDTO));
        } catch (Exception ex) {
            StringWriter swr = new StringWriter();
            PrintWriter out = new PrintWriter(swr);
            ex.printStackTrace(out);
            out.flush();
            fail(swr.getBuffer().toString());
        }
    }
//
//    /**
//     * Test of findByIds method, of class AccommodationServiceImpl.
//     */
//    @Test
//    public void testFindByIds() {
//        System.out.println("findByIds");
//        List<Long> ids = null;
//        AccommodationServiceImpl instance = new AccommodationServiceImpl();
//        List<AccommodationDTO> expResult = null;
//        List<AccommodationDTO> result = instance.findByIds(ids);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of findUpdatedAccommodations method, of class
//     * AccommodationServiceImpl.
//     */
//    @Test
//    public void testFindUpdatedAccommodations() {
//        System.out.println("findUpdatedAccommodations");
//        Date since = null;
//        Date until = null;
//        Integer size = null;
//        Integer page = null;
//        AccommodationServiceImpl instance = new AccommodationServiceImpl();
//        List<AccommodationDTO> expResult = null;
//        List<AccommodationDTO> result = instance.findUpdatedAccommodations(since, until, size, page);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of handlePytonUpdateEvent method, of class AccommodationServiceImpl.
//     */
//    @Test
//    public void testHandlePytonUpdateEvent() {
//        System.out.println("handlePytonUpdateEvent");
//        SourceUpdateAvailableEvent event = null;
//        AccommodationServiceImpl instance = new AccommodationServiceImpl();
//        instance.handlePytonUpdateEvent(event);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of findByExternalId method, of class AccommodationServiceImpl.
//     */
//    @Test
//    public void testFindByExternalId() {
//        System.out.println("findByExternalId");
//        Accommodation.Source source = null;
//        Long id = null;
//        AccommodationServiceImpl instance = new AccommodationServiceImpl();
//        List<AccommodationDTO> expResult = null;
//        List<AccommodationDTO> result = instance.findByExternalId(source, id);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

}
