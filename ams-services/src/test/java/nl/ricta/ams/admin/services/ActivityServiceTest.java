package nl.ricta.ams.admin.services;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.ArrayList;
import nl.ricta.ams.admin.services.interfaces.ActivityService;
import nl.ricta.ams.admin.repositories.*;
import java.util.Date;
import java.util.List;
import java.util.Optional;
//import java.util.List;
import nl.ricta.ams.api.entities.ActivityEntity;
import nl.ricta.ams.api.vo.ActivityDTO;
import nl.ricta.ams.api.vo.ActivityState;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

/**
 *
 * @author rr
 */



//@RunWith(SpringRunner.class)


@Configuration()
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
//@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MocksApplication.class)
public class ActivityServiceTest {


 

     
//    @Test
//    public void findNotOneByIdTest() 
//    {
//      
//        ActivityDTO actToFind = activityService.findOneByID(1);
//        Assert.isTrue(actToFind == null);
// 
//    }
//    @ConfigurationProperties(prefix="my")
//    @Bean (name = "ignoreFileNames")        
//        class Config {
//
//        private List<String> servers = new ArrayList<String>();
//
//        public List<String> getServers() {
//            return this.servers;
//        }
//    }   
    
    @Autowired
    ApplicationContext context;
    
    @Autowired
    ModelMapper modelmapper;
    
     @TestConfiguration
     //Mocked repos etc here
     static class AccommodationServiceImplTestContextConfiguration 
     { 
          
    
        @Autowired
        ModelMapper modelmapper;
         
        @Bean
        public ModelMapper modelMapper() 
        {
            
            return new ModelMapper();
        }

        @Bean
        @Primary
        public ActivityRepository activityRepository() 
        {
            ActivityRepository mockecActivityRepository = Mockito.mock(ActivityRepository.class);
             Long id = 1L;

           Optional<ActivityEntity> actEntity  = Optional.of(ActivityEntity.builder()
                .creationDate(new Date(2019, 1,1))
                .description("desc")
                .expirationDate(new Date(2019, 1,1))
                .id(1L)
                .prio("prio")
                .requestorName("rr")
                .type("type")
                .status("open")
                .title("title")
                .build());
                   
            
           Mockito.when(mockecActivityRepository.findById(id)).thenReturn(actEntity);
           Assert.isTrue(actEntity.get().getId() == 1L);
           return mockecActivityRepository;
        }
     }
         
     //Service impl to test
    @Autowired
    ActivityService activityService;
 
    @Test
    public void findOneByIdTest() 
    {
        try
        {
            ActivityDTO actToFind = activityService.findOneByID(1L);
            
            Assert.isTrue(actToFind != null);
            Assert.isTrue(actToFind.getId() == 1L);
        } 
        catch (Exception e) 
        {
            Assert.isTrue(false);
        }
    }
    
    @Test
    public void autowiredStateRepo() 
    {
        Assert.isTrue(activityService != null);
        Assert.isTrue(activityService.getClass().equals(ActivityServiceImpl.class));

        //test
        ActivityDTO actDto = activityService.findOneByID(1L);

        Assert.isTrue(actDto != null);
        Assert.isTrue(actDto.getId() == 1L);
        Assert.isTrue(actDto.getTitle()== "title");
    }
  
   
    
    @Before
    public void setup() 
    {
        //Too late:
        
//        Assert.isTrue(modelmapper != null);
//        ModelMapper m = context.getBean(ModelMapper.class);
//        Assert.isTrue(m != null);
//        
//        MockitoAnnotations.initMocks(this);
//        // Assert.isTrue(mockActivityRepository != null);
//         Assert.isTrue(this.activityService != null);
//        //ini service, inject with real modelmapper, but mocked repo
//       // this.activityService = new ActivityServiceImpl(new ModelMapper(), mockActivityRepository);
//        
//        Long id = 1L;
//        
//        ActivityEntity actEntity  = ActivityEntity.builder()
//            .creationDate(new Date(2019, 1,1))
//            .description("desc")
//            .expirationDate(new Date(2019, 1,1))
//            .id(1L)
//            .prio("prio")
//            .requestorName("rr")
//            .type("type")
//            .status("open")
//            .title("title")
//            .build();
//       //Mockito.when(mockActivityRepository.findOne(id)).thenReturn(actEntity);
//      // Assert.isTrue(actEntity.getId() == 1L);
    }
    
     
   
    
    //@Test
    public void updateByIdTest() 
    {
      //TODO enable teest, put in other testfile
        Long id = 1L;
        ActivityDTO act  = ActivityDTO.builder()
                .dateOfCreation("1-1-2019")
                .description("desc")
                .dateOfExpiration("2-1-2019")
                .id(id)
                .prio("prio")
                .requestorName("rr")
                .type("type")
                .status(ActivityState.Open.toString())
                .title("title")
                .build();
         ActivityEntity actEntity  = ActivityEntity.builder()
                .creationDate(new Date(2019, 1,1))
                .description("desc")
                .expirationDate(new Date(2019, 1,1))
                .id(1L)
                .prio("prio")
                .requestorName("rr")
                .type("type")
                .status("open")
                .title("title")
                .build();

    try
    {
         
         long updatedID = activityService.updateActivity(act);
        
        Assert.isTrue(updatedID == 1L);
    } 
    catch (Exception e) 
    {
        Assert.isTrue(false);
    }
  


    }


}
 //  Mockito.when(mockActivityRepository.findOne(id)).thenReturn(actEntity);
   //    ActivityDTO actToFind = activityService.findOneByID(1L);
   //     Assert.isTrue(actToFind != null);