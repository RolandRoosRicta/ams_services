/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.ricta.ams.admin.services;

import java.util.ArrayList;
import java.util.List;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 *
 * @author roosr
 */

@Configuration
    @ConfigurationProperties(prefix="servers")
       public class Servers {

           public List<String> ignoredFilenames = new ArrayList<String>();
          
           public List<String> getFilenames() {
               return this.ignoredFilenames;
           }
            public void setFilenames(List<String> files) {
               this.ignoredFilenames = files;
           }
       }
