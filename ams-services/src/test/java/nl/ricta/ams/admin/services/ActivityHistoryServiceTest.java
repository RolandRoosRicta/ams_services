package nl.ricta.ams.admin.services;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import nl.ricta.ams.admin.services.interfaces.ActivityService;
import nl.ricta.ams.admin.repositories.*;
import java.util.Date;
import nl.ricta.ams.admin.services.interfaces.ActivityHistoryService;
//import java.util.List;
import nl.ricta.ams.api.entities.ActivityEntity;
import nl.ricta.ams.api.entities.ActivityHistoryEntity;
import nl.ricta.ams.api.vo.ActivityDTO;
import nl.ricta.ams.api.vo.ActivityState;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

/**
 *
 * @author rr
 */

//@Configuration
@ActiveProfiles("test")

//@RunWith(SpringRunner.class)



//@RunWith(SpringRunner.class)
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MocksApplication.class)
public class ActivityHistoryServiceTest {

   
     @TestConfiguration
     static class AccommodationServiceImplTestContextConfiguration 
     { 

        @Bean
        @Primary
        public ActivityHistoryRepository activityHistoryRepository() 
        {
            ActivityHistoryRepository mockecActivityRepository = Mockito.mock(ActivityHistoryRepository.class);
            
           return mockecActivityRepository;
        }
  
     }
    @Autowired
    ActivityHistoryService activityHistoryService;
    
    @Test
    public void autowiredHistoryService() 
    {
        Assert.isTrue(activityHistoryService != null);
        
    }
}


