/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.ricta.ams.admin.services;

import java.util.Properties;
import nl.ricta.ams.admin.repositories.ActivityRepository;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

/**
 *
 * @author roosr
 */
@Profile("test")
@Configuration
public class RepoTestConfiguration 
{
 
    @Bean
    @Primary
    public ModelMapper modelMapper() 
    {

       return new ModelMapper();
    }
        
//    @Bean
//    @Primary
//    public ActivityRepository activityRepository() 
//    {
//        return Mockito.mock(ActivityRepository.class);
//    }
    
    @Bean
    @Primary
    public JavaMailSender getJavaMailSender() 
    {
       JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
       mailSender.setHost("smtp.gmail.com");
       mailSender.setPort(587);

       mailSender.setUsername("roos.ict.architectures@gmail.com");
       mailSender.setPassword("Roos5000");

       Properties props = mailSender.getJavaMailProperties();
       props.put("mail.transport.protocol", "smtp");
       props.put("mail.smtp.auth", "true");
       props.put("mail.smtp.starttls.enable", "true");
       props.put("mail.debug", "true");

       props.put("mail.smtp.host", "smtp.gmail.com");
       props.put("mail.smtp.port", "587");
       props.put("mail.smtp.ssl.trust", "smtp.gmail.com");

       return mailSender;
    }
}
