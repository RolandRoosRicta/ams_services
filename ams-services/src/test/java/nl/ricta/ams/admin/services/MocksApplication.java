/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.ricta.ams.admin.services;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 *
 * @author roosr
 */
@SpringBootApplication
//@EnableJpaRepositories("nl.ricta.ams*")
@EntityScan(basePackages = {"nl.ricta.ams*", "nl.ricta.ams.*"})
@ComponentScan(basePackages = {"nl.ricta.ams*", "nl.ricta.ams.*","nl.ricta.ams.admin.services.*", "nl.ricta.ams.admin.repositories.*", "nl.ricta.ams.admin.repositories*"})
public class MocksApplication {
    public static void main(String[] args) {
        SpringApplication.run(MocksApplication.class, args);
    }
}
