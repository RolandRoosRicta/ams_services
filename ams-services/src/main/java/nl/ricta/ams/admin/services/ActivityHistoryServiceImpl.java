/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.ricta.ams.admin.services;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;
import java.util.stream.Stream;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import nl.ricta.ams.admin.repositories.ActivityHistoryRepository;
import nl.ricta.ams.admin.services.interfaces.ActivityHistoryService;
import nl.ricta.ams.api.entities.ActivityHistoryEntity;
import nl.ricta.ams.api.vo.ActivityDTO;
import nl.ricta.ams.api.vo.ActivityHistoryDTO;
import org.apache.commons.collections.IteratorUtils;
import static org.modelmapper.convention.MatchingStrategies.STRICT;
import org.modelmapper.spi.MappingContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import static org.springframework.data.domain.Sort.Direction.ASC;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Component;
//import org.springframework.data.domain.Sort;
//import static org.springframework.data.domain.Sort.Direction.ASC;


/**
 * Activity service implementation
 *
 * The service layer is responsible for providing accommodation services like
 * find and storing them.
 *
 * The accommodation service only communicated in Data Transfer Objects, hiding
 * the entities from the persistence layer in the model project.
 *
 *
 *
 *
 * @author r.roos <rroos at ricta>
 */
@Component
public class ActivityHistoryServiceImpl implements ActivityHistoryService 
{
    private static DateFormat DATE_FORMAT = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss.SSS");
    private ModelMapper modelMapper;
          
    @Autowired(required = true)
    private ActivityHistoryRepository activityHistoryRepository;
  
    //ModelMapper modelMapper is injected by Spring. See the @Bean in AmsAdminApplication
    @Autowired(required = true)
    public ActivityHistoryServiceImpl(ModelMapper modelMapper) 
    {
        this.modelMapper = modelMapper;
        this.modelMapper.getConfiguration().setMatchingStrategy(STRICT);
        this.modelMapper.getConfiguration()
                .setPropertyCondition(Conditions.isNotNull());
        
        //map from ActivityDTO -> ActivityEntity
        this.modelMapper.createTypeMap(ActivityHistoryDTO.class, ActivityHistoryEntity.class);
        this.modelMapper.createTypeMap(ActivityHistoryEntity.class, ActivityHistoryDTO.class)
                .addMappings(mapper -> 
                {
                    mapper.using((MappingContext<Date, String> context) -> {
                           return DATE_FORMAT.format(context.getSource());
                       });
                });
//                 .addMappings(mapper -> 
//                {
//                    mapper.map(ActivityHistoryEntity::getId, ActivityHistoryDTO::setId);
//                   // mapper.map(ActivityEntity::getExpirationDate, ActivityDTO::setDateOfExpiration);
//                 });

        this.modelMapper.createTypeMap(ActivityDTO.class, ActivityHistoryEntity.class)
                .addMappings(mapper -> 
                {
                    mapper.map(ActivityDTO::getDateOfCreation, ActivityHistoryEntity::setCreationDate);
                    mapper.map(ActivityDTO::getDateOfExpiration, ActivityHistoryEntity::setExpirationDate);
                    mapper.map(ActivityDTO::getId, ActivityHistoryEntity::setActivityId);
                });
                
     }

    public List<ActivityHistoryDTO> findAll()
    {
        //Aggregation aggregation = newAggregation(
        //      sort
        //new Sort(Sort.Direction.DESC, new String[]{"price"}
        //List<String> orderList = Stream.of("activityId").collect(Collectors.toList());
        Sort order = Sort.by(Sort.Direction.ASC, "activityId");
        
      //  Sort s = new  Sort(  Sort.Direction.ASC);
//(Sort.Direction.ASC, orderList);
        List<ActivityHistoryEntity> entities = IteratorUtils.toList(activityHistoryRepository.findAll(order).iterator());
        List<ActivityHistoryDTO> dtos = 
                entities.stream().map(
                (a) -> 
                {
                    return this.modelMapper.map(a,ActivityHistoryDTO.class);
                })
                .collect(toList());
        return dtos;
    }
 
    /*
        saves the activity. 
        Note: required DB fields must be there!
        Note: History needs filling afterwards!
    */
    public void AddActivityVersion(ActivityDTO activity)
    {
        //firt, map DTO back to some enity
        var entity = this.modelMapper.map(activity, ActivityHistoryEntity.class);
        Calendar calendar = Calendar.getInstance();
        Date changed = calendar.getTime();
        entity.setChanged(changed);
        activityHistoryRepository.save(entity);
    }
    
    

    
}
