/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.ricta.ams.admin.services;

import nl.ricta.ams.admin.services.interfaces.ActivityService;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.util.stream.Collectors.toList;
import javax.mail.MessagingException;
import nl.ricta.ams.admin.repositories.ActivityRepository;
import nl.ricta.ams.api.entities.ActivityEntity;
import nl.ricta.ams.api.vo.ActivityDTO;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.apache.commons.collections.IteratorUtils;
import static org.modelmapper.convention.MatchingStrategies.STRICT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import javax.mail.internet.MimeMessage;
import org.modelmapper.spi.MappingContext;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

/**
 * Activity service implementation
 *
 * The service layer is responsible for providing accommodation services like
 * find and storing them.
 *
 * The accommodation service only communicated in Data Transfer Objects, hiding
 * the entities from the persistence layer in the model project.
 *
 *
 *
 *
 * @author r.roos <rroos at ricta>
 */
@Component
//@Service
public class ActivityServiceImpl implements ActivityService {

     private static DateFormat DATE_FORMAT = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss.SSS");
  //  private static final Logger log = LoggerFactory.getLogger(ActivityServiceImpl.class);
    @Autowired
    private ModelMapper modelMapper;
          
    @Autowired(required = true)
    private ActivityRepository activityRepository;
  
    //ModelMapper modelMapper is injected by Spring. See the @Bean in AmsAdminApplication
    @Autowired(required = true)
    public ActivityServiceImpl(ModelMapper modelMapper, ActivityRepository activityRepository) 
    {
        this.activityRepository = activityRepository;
        this.modelMapper = modelMapper;
        this.modelMapper.getConfiguration().setMatchingStrategy(STRICT);
        this.modelMapper.getConfiguration()
                .setPropertyCondition(Conditions.isNotNull());
        
        //map from ActivityDTO -> ActivityEntity
        this.modelMapper.createTypeMap(ActivityDTO.class, ActivityEntity.class)
                .addMappings(mapper -> 
                {
                    mapper.using((MappingContext<String, Date> context) -> 
                      {
                          try
                          {
                            return DATE_FORMAT.parse(context.getSource());
                          }
                          catch(Exception ex)
                          {
                              return new Date("1-1-1970");
                          }
                       });
                    mapper.map(ActivityDTO::getDateOfCreation, ActivityEntity::setCreationDate);
                    mapper.map(ActivityDTO::getDateOfExpiration, ActivityEntity::setExpirationDate);
                    mapper.map(ActivityDTO::getStatus, ActivityEntity::setStatus);
                     
//                    mapper.using((MappingContext<ActivityState, String> context) -> {
//                        String source = context.toString();
//                        return  source == null ? ActivityState.Closed.toString() : source;
//                    });
                    
                 });
//             this.modelMapper.addConverter(new Converter<ActivityState, String>()
//                {
//                   //@Override
//                   public ActivityState convert(MappingContext<ActivityState, String> context)
//                   {
//                      String source = context.toString();
//                      return source == null ? ActivityState.Closed.toString() : source;
//                   }
//                });
             
    
        //map from ActivityEntity -> ActivityDTO
        this.modelMapper.createTypeMap(ActivityEntity.class, ActivityDTO.class)
                 .addMappings(mapper -> 
                {
                     mapper.using((MappingContext<Date, String> context) -> {
                           return DATE_FORMAT.format(context.getSource());
                       });
                    mapper.map(ActivityEntity::getCreationDate, ActivityDTO::setDateOfCreation);
                    mapper.map(ActivityEntity::getExpirationDate, ActivityDTO::setDateOfExpiration);
                 });
     }

   
    public List<ActivityDTO> findAll()
    {
         //IteratorUtils.toList(entities.iterator());
        //StreamSupport.stream(entities.spliterator(), false)
        List<ActivityEntity> entities = IteratorUtils.toList(activityRepository.findAll().iterator());
        List<ActivityDTO> dtos = 
                entities.stream().map(
                (a) -> 
                {
                    return this.modelMapper.map(a,ActivityDTO.class);
                })
                .collect(toList());
        return dtos;
    }
    
    
    
    public ActivityDTO findOneByID(long Id)
    {

        Optional<ActivityEntity> entity = activityRepository.findById(Id);
        if(entity.isPresent())
        {
            return this.modelMapper.map(entity.get(),ActivityDTO.class);
        }
        else
        {
            return null;
        }
 
    }
    /*
        saves the activity. 
        Note: required DB fields must be there!
        Note: History needs filling afterwards!
    */
    public long updateActivity(ActivityDTO activity)
    {
        //firt, map DTO back to some enity
        ActivityEntity entity = this.modelMapper.map(activity, ActivityEntity.class);
        activityRepository.save(entity);
        activity.setId(entity.getId());
        return entity.getId();
    }
    
    @Value("${ams.admin.sentActivityExpirationMails}")
    private boolean sentActivityExpirationMails;
    
                
    @Autowired
    private JavaMailSender javaMailSender;
    
   // @Scheduled(fixedDelay = 60000)
    public void scheduleActivityCheckForExpiration() 
    {
        if (sentActivityExpirationMails)
        {
         Iterable<ActivityEntity> activities  = activityRepository.findAll();
         Calendar calendar = Calendar.getInstance();
         Date now = calendar.getTime();
         for(ActivityEntity act: activities)
         {
             if (act.getExpirationDate() != null && act.getExpirationDate().after( now))
             {
                 MimeMessage mimeMessage = javaMailSender.createMimeMessage();
                 MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "utf-8");

                 
                     //                   SimpleMailMessage message = new SimpleMailMessage();
//                    message.setTo("roos.ict.architectures@gmail.com");
//                    String subject = String.format("Id = %s, title = %s, description = %s",
//                            act.getId().toString(),
//                            act.getTitle(), 
//                            act.getDescription());
//                    
//                    message.setSubject(subject);//
//                    message.setText(act.getTitle() + ":" + act.getDescription());
                 try 
                 {
                    helper.setTo("roos.ict.architectures@gmail.com"); 
                 
                    String subjectText = String.format("<h3>title = %s</h3>"
                            + "<h2>Id</h2> = %s"
                             + "<h2>description </h2>= %s",
                            act.getId().toString(),
                            act.getTitle(), 
                            act.getDescription());
                    
                    helper.setSubject(act.getTitle() + ":" + act.getDescription());//
                    helper.setText(subjectText, true);//true = html
                    javaMailSender.send(mimeMessage);
                 } 
                 catch (MessagingException ex) 
                 {
                     Logger.getLogger(ActivityServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
                 }
                    
             }
         }
        }
    }
}
