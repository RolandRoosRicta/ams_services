package nl.ricta.codecoverageutil;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

@SpringBootApplication
@Configuration
public class BuildUtilApplication implements ApplicationRunner //CommandLineRunner
{
    public static void main(String[] args) 
    {
       SpringApplication.run(BuildUtilApplication.class, args);
      
        
    }

    @Override
    public void run(ApplicationArguments args) 
    {
         //try finding *index.html files in classpath
        Set<String> argsOption  = args.getOptionNames();
        for (String name : argsOption)
        {
            System.out.println("arg-" + name + "=" + args.getOptionValues(name));
        }

        boolean containsPathOption = args.containsOption("index.path");
        System.out.println("Contains index.path: " + containsPathOption);
        
         boolean containsTemplateOption = args.containsOption("index.templatepath");
        System.out.println("Contains index.templatepath: " + containsTemplateOption);
       try
       {
            String templatePath = containsPathOption? args.getOptionValues("index.templatepath").get(0) :  new File("./target/classes").getCanonicalPath() ;
             String path = containsPathOption? args.getOptionValues("index.path").get(0) :  new File("./target/classes").getCanonicalPath() ;
            File dir = new File(path);

             File[] matches = dir.listFiles(new FilenameFilter()
             {
               public boolean accept(File dir, String name)
               {
                  return name.endsWith("index.html") && !name.startsWith("template");
               }
             });
              DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
              //dbFactory.setNamespaceAware(false);
                dbFactory.setValidating(false);
                dbFactory.setFeature("http://xml.org/sax/features/namespaces", false);
                dbFactory.setFeature("http://xml.org/sax/features/validation", false);
                dbFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
                dbFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);

              DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
               Document template = dBuilder.parse(templatePath + "/template-index.html");
              Node body =  template.getElementsByTagName("body").item(0);
             for(File fXmlFile : matches)
             {
               
                Document doc = dBuilder.parse(fXmlFile);
                doc.getDocumentElement().normalize();

                System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

                NodeList nList = doc.getElementsByTagName("table");
                 Node newNode = template.importNode(nList.item(0), true);
                 body.appendChild(newNode);
             }
//             OutputFormat format = new OutputFormat(template); //document is an instance of org.w3c.dom.Document
//           // format.setLineWidth(165);
//            format.setIndenting(true);
//            format.setIndent(2);
//           //  template.setDocumentURI(path);
//             java.io.Writer writer = new java.io.FileWriter(path + "/code-coverage-total.html");
//                XMLSerializer xml = new XMLSerializer(writer, format);
//                xml.serialize(template);
//                writer.flush();
            // Create an "identity" transformer - copies input to output
            
            //HTML != XML
            //Valid XML, but invalid HTML:
            //<script src="jacoco-resources/sort.js" type="text/javascript"/>
             //Valid HTML:
             //<script src="jacoco-resources/sort.js" type="text/javascript"></script>
             
            //So, we need to generate the HTML format!
            TransformerFactory tf = TransformerFactory.newInstance();
            tf.setAttribute("indent-number", new Integer(2));
            Transformer t = tf.newTransformer();
          
//            // for "XHTML" serialization, use the output method "xml"
//            // and set publicId as shown
//            t.setOutputProperty(OutputKeys.METHOD, "xml");
//
//            t.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC,
//                                "-//W3C//DTD XHTML 1.0 Transitional//EN");
//
//            t.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM,
//                           "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd");

            // For "HTML" serialization, use
            t.setOutputProperty(OutputKeys.METHOD, "html");
            t.setOutputProperty(OutputKeys.INDENT, "yes");
            t.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC,"yes");
            t.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            
            java.io.Writer writer = new java.io.FileWriter(path + "/code-coverage-total.html");
            
            // Serialize DOM tree
            t.transform(new DOMSource(template), new StreamResult(writer));
        }
        catch(Exception ex)
        {
            String res = ex.getMessage();
        }
       
    }
}
